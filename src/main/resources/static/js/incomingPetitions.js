function PetitionsViewModel() {
	let self = this;
	self.petitions = ko.observableArray();
	self.petitionURI = '/petition';
	self.offerURI = '/offer';

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.ajax(self.petitionURI, 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			let broker = data[i].broker;
			let brokerDetails = broker.name + ' ' + broker.surname + ', ' + broker.company.name;
			self.petitions.push({
				id: ko.observable(data[i].id),
				name: ko.observable(data[i].name),
				type: ko.observable(data[i].type),
				policyHolderName: ko.observable(data[i].policyHolderName),
				coverStartDate: ko.observable(data[i].coverStartDate),
				coverEndDate: ko.observable(data[i].coverEndDate),
				brokerDetails: ko.observable(brokerDetails)
			});
		}
	});

	self.goToDetails = function (petition) {
		window.location.replace("/incomingPetitionDetails.html?id=" + petition.id())
	};

	self.createOffer = function (petition) {
		let sendRequest = {
			petitionId: petition.id()
		};

		self.ajax(self.offerURI + '/create', 'PUT', sendRequest).done(function (data) {
			window.location.replace('/offers.html');
		});
	};
}

let petitionsViewModel = new PetitionsViewModel();
ko.applyBindings(petitionsViewModel, $('#main')[0]);