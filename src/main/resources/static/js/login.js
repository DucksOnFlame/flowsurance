function LoginViewModel() {
	let self = this;
	self.loginURI = '/user/login';
	self.userURI = '/user';
	self.login = ko.observable();
	self.password = ko.observable();

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: 'application/json',
			accept: 'application/json',
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				if (jqXHR.status === 401) {
					alert("Podano błędny login lub hasło!");
					return;
				}

				console.log("ajax error " + jqXHR.status + ", message: " + jqXHR.message);
				alert('Coś poszło nietak!');
			}
		};
		return $.ajax(request);
	};

	self.sendLogin = function () {
		const loginData = {
			displayName: self.login(),
			password: self.password()
		};

		self.ajax(self.loginURI, 'POST', loginData).done(function (redirectResponse) {
			window.location.replace(redirectResponse.path);
			self.login("");
			self.password("");
		});
	};

	self.signUp = function () {
		window.location.replace('/signUp.html');
	};
}

const loginViewModel = new LoginViewModel();
ko.applyBindings(loginViewModel, $('#login')[0]);
