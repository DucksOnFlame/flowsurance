function MenuViewModel() {
	let self = this;
	self.userURI = '/user';
	self.isBroker = ko.observable();
	self.isInsurer = ko.observable();
	self.isAdmin = ko.observable();

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.ajax(self.userURI + '/config', 'GET').done(function (data) {
		self.isBroker(data.isBroker);
		self.isInsurer(data.isInsurer);
		self.isAdmin(data.isAdmin);
	});
}

let menuViewModel = new MenuViewModel();
ko.applyBindings(menuViewModel, $('#main')[0]);


redirectToLogin = function () {
	window.location = "/"
};

redirectToHome = function () {
	window.frames["content_frame"].location = "/greeting.html"
};

redirectToPetitions = function () {
	window.frames["content_frame"].location = "/petitions.html"
};

redirectToComparisons = function () {
	window.frames["content_frame"].location = "/comparisons.html"
};

redirectToIncomingPetitions = function () {
	window.frames["content_frame"].location = "/incomingPetitions.html"
};

redirectToOffers = function () {
	window.frames["content_frame"].location = "/offers.html"
};

redirectToClauses = function () {
	window.frames["content_frame"].location = "/clauses.html"
};

redirectToClients = function () {
	window.frames["content_frame"].location = "/clients.html"
};

redirectToExclusions = function () {
	window.frames["content_frame"].location = "/exclusions.html"
};
