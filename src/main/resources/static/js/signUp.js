function SignUpViewModel() {
	let self = this;
	self.userURI = '/user/create';
	self.name = ko.observable();
	self.surname = ko.observable();
	self.displayName = ko.observable();
	self.companyId = ko.observable();
	self.email = ko.observable();
	self.jobTitle = ko.observable();
	self.password = ko.observable();

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: 'application/json',
			accept: 'application/json',
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status + ", message: " + jqXHR.message);
				alert('Something went wrong!');
			}
		};
		return $.ajax(request);
	};

	self.createNewUser = function (createUserRequest) {
		self.ajax(self.userURI, 'PUT', createUserRequest).done(function () {
			$('#createUser').modal('hide');
			alert('Success!');
			window.location.replace('/');
		});
	};

	self.createUser = function () {
		signUpViewModel.createNewUser({
			name: self.name(),
			surname: self.surname(),
			displayName: self.displayName(),
			companyId: self.companyId(),
			email: self.email(),
			jobTitle: self.jobTitle(),
			password: self.password()
		});

		self.name('');
		self.surname('');
		self.displayName('');
		self.companyId('');
		self.email('');
		self.jobTitle('');
		self.password('');
	};
}

const signUpViewModel = new SignUpViewModel();
ko.applyBindings(signUpViewModel, $('#signup')[0]);
