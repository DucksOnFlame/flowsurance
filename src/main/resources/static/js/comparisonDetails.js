currentComparisonId = window.location.search.substring(4);

function ComparisonDetailsViewModel() {
	let self = this;
	self.comparisonURI = '/comparison';
	self.comparisons = ko.observableArray([]);
	self.columnNumber = 0;

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.maximizeFullSpanRows = function () {
		let rowsToMaximize = document.getElementsByClassName("maximized-row");
		for (var i = 0; i < rowsToMaximize.length; i++) {
			rowsToMaximize[i].colSpan = self.columnNumber;
		}
	};

	self.setBodyWidth = function () {
		document.body.style.width = (500 * self.columnNumber + 250).toString() + 'px';
	};

	self.mergeTableRowHeaders = function (elementClassName) {
		let elements = document.getElementsByClassName(elementClassName);
		let rowSpan = elements.length;
		for (var i = 1; i < elements.length; i++) {
			elements[i].style.display = "none";
		}

		elements[0].rowSpan = rowSpan;
	};

	self.ajax(self.comparisonURI + '/' + currentComparisonId, 'GET').done(function (comparison) {
		self.comparisons.push(ko.mapping.fromJS(comparison));
		// Number of offers + 1 petition
		self.columnNumber = self.comparisons()[0].insurers().length + 1;
		self.maximizeFullSpanRows();
		self.setBodyWidth();
		self.mergeTableRowHeaders('clause-table-header');
		self.mergeTableRowHeaders('exclusion-table-header');
		self.mergeTableRowHeaders('deductible-table-header');
	});

	self.isRejected = function (type) {
		return type === 'REJECTED';
	};

	self.isAccepted = function (type) {
		return type === 'ACCEPTED';
	};

	self.isChanged = function (type) {
		return type === 'CHANGED';
	};

	self.isNew = function (type) {
		return type === 'NEW';
	};
}

let comparisonDetailsViewModel = new ComparisonDetailsViewModel();
ko.applyBindings(comparisonDetailsViewModel, $('#main')[0]);