currentOfferId = window.location.search.substring(4);

function OfferDetailsViewModel() {
	let self = this;
	self.offerURI = '/offer';

	self.availableClauses = ko.observableArray();
	self.newClause = ko.observable();

	self.availableExclusions = ko.observableArray();
	self.newExclusion = ko.observable();

	self.newDeductibleDescription = ko.observable();

	self.brokerDetails = ko.observable();
	self.type = ko.observable();
	self.state = ko.observable();
	self.subjects = ko.observableArray();
	self.coverStartDate = ko.observable();
	self.coverEndDate = ko.observable();
	self.policyHolder = ko.observable();
	self.insureds = ko.observableArray();

	self.offeredClauses = ko.observableArray();
	self.offeredExclusions = ko.observableArray();
	self.offeredDeductibles = ko.observableArray();

	self.priceAmount = ko.observable();
	self.priceCurrency = ko.observable();
	self.isPriceEditMode = ko.observable(false);
	self.priceInput = ko.observable();
	self.selectedCurrency = ko.observable();

	self.availableCurrencies = ko.observableArray([
		ko.observable('PLN'),
		ko.observable('USD'),
		ko.observable('EUR')
	]);

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.ajax(self.offerURI + '/' + currentOfferId, 'GET').done(function (data) {
		self.setOffer(data);
	});

	self.setOffer = function (offer) {
		let petition = offer.petition;
		self.type(petition.type);
		self.state(offer.state);

		self.subjects([]);
		for (var i = 0; i < petition.subjects.length; i++) {
			self.subjects.push({
				item: ko.observable(petition.subjects[i].item)
			});
		}

		self.coverStartDate(petition.coverStartDate);
		self.coverEndDate(petition.coverEndDate);
		self.policyHolder(petition.policyHolder);

		let brokerDetails = petition.broker.name + ' ' + petition.broker.surname + ', ' + petition.broker.company.name;
		self.brokerDetails(brokerDetails);

		self.priceAmount(offer.amount);
		self.priceInput(offer.amount);
		self.priceCurrency(offer.currencyCode);
		self.selectedCurrency(offer.currencyCode);

		self.insureds([]);
		for (var i = 0; i < petition.insureds.length; i++) {
			self.insureds.push({
				id: ko.observable(petition.insureds[i].id),
				name: ko.observable(petition.insureds[i].name),
				type: ko.observable(petition.insureds[i].type),
				street: ko.observable(petition.insureds[i].street),
				zipCode: ko.observable(petition.insureds[i].zipCode),
				city: ko.observable(petition.insureds[i].city),
				state: ko.observable(petition.insureds[i].state),
				country: ko.observable(petition.insureds[i].country),
				taxPayerId: ko.observable(petition.insureds[i].taxPayerId)
			});
		}

		self.offeredClauses([]);
		for (var i = 0; i < offer.offeredClauses.length; i++) {
			let offeredClause = offer.offeredClauses[i];

			let originalClause = ko.observable(null);
			if (offeredClause.originalClause) {
				originalClause({
					id: ko.observable(offeredClause.originalClause.id),
					title: ko.observable(offeredClause.originalClause.title),
					content: ko.observable(offeredClause.originalClause.content)
				});
			}

			let changedClause = ko.observable(null);
			if (offeredClause.changedClause) {
				changedClause({
					id: ko.observable(offeredClause.changedClause.id),
					title: ko.observable(offeredClause.changedClause.title),
					content: ko.observable(offeredClause.changedClause.content)
				});
			}

			self.offeredClauses.push({
				originalClause: originalClause,
				type: ko.observable(offeredClause.type),
				changedClause: changedClause
			});
		}


		self.offeredExclusions([]);
		for (var i = 0; i < offer.offeredExclusions.length; i++) {
			let offeredExclusion = offer.offeredExclusions[i];

			let originalExclusion = ko.observable(null);
			if (offeredExclusion.originalExclusion) {
				originalExclusion({
					id: ko.observable(offeredExclusion.originalExclusion.id),
					content: ko.observable(offeredExclusion.originalExclusion.content)
				});
			}

			let changedExclusion = ko.observable(null);
			if (offeredExclusion.changedExclusion) {
				changedExclusion({
					id: ko.observable(offeredExclusion.changedExclusion.id),
					content: ko.observable(offeredExclusion.changedExclusion.content)
				});
			}

			self.offeredExclusions.push({
				originalExclusion: originalExclusion,
				type: ko.observable(offeredExclusion.type),
				changedExclusion: changedExclusion
			});
		}

		self.offeredDeductibles([]);
		for (var i = 0; i < offer.offeredDeductibles.length; i++) {
			let offeredDeductible = offer.offeredDeductibles[i];

			let originalDeductible = ko.observable(null);
			if (offeredDeductible.originalDeductible) {
				originalDeductible({
					description: ko.observable(offeredDeductible.originalDeductible.description)
				});
			}

			let changedDeductible = ko.observable(null);
			if (offeredDeductible.changedDeductible) {
				changedDeductible({
					description: ko.observable(offeredDeductible.changedDeductible.description)
				});
			}

			self.offeredDeductibles.push({
				originalDeductible: originalDeductible,
				type: ko.observable(offeredDeductible.type),
				changedDeductible: changedDeductible
			});
		}
	};

	self.isRejected = function (offeredClause) {
		return offeredClause.type() === 'REJECTED'
	};

	self.isAccepted = function (offeredClause) {
		return offeredClause.type() === 'ACCEPTED'
	};

	self.isChanged = function (offeredClause) {
		return offeredClause.type() === 'CHANGED'
	};

	self.isNew = function (offeredClause) {
		return offeredClause.type() === 'NEW'
	};

	// CLAUSE ACTIONS
	self.acceptClause = function (offeredClause) {
		self.clearChangedClauseIfPresent(offeredClause);
		offeredClause.type('ACCEPTED');
	};

	self.refuseClause = function (offeredClause) {
		self.clearChangedClauseIfPresent(offeredClause);
		offeredClause.type('REJECTED');
	};

	self.editClause = function (offeredClause) {
		self.clearChangedClauseIfPresent(offeredClause);
		offeredClause.clauseToBeChanged = ko.observable();
		offeredClause.type('CHANGED');
	};

	self.clearChangedClauseIfPresent = function (offeredClause) {
		let changedClause = offeredClause.changedClause();
		if (changedClause === null) {
			return;
		}

		self.availableClauses.remove(changedClause);
		self.availableClauses.push({
			id: ko.observable(changedClause.id()),
			title: ko.observable(changedClause.title()),
			content: ko.observable(changedClause.content())
		});
		offeredClause.changedClause(null);
	};

	self.addChangedClause = function (offeredClause) {
		offeredClause.changedClause({
			id: ko.observable(offeredClause.clauseToBeChanged().id()),
			title: ko.observable(offeredClause.clauseToBeChanged().title()),
			content: ko.observable(offeredClause.clauseToBeChanged().content())
		});
		self.availableClauses.remove(offeredClause.clauseToBeChanged());
		offeredClause.clauseToBeChanged(null);
	};

	self.addNewClause = function () {
		self.offeredClauses.push({
			originalClause: ko.observable(null),
			type: ko.observable('NEW'),
			changedClause: ko.observable({
				id: ko.observable(self.newClause().id()),
				title: ko.observable(self.newClause().title()),
				content: ko.observable(self.newClause().content())
			})
		});

		self.availableClauses.remove(self.newClause());
	};

	self.removeNewClause = function (offeredClause) {
		self.offeredClauses.remove(offeredClause);

		self.availableClauses.push({
			id: ko.observable(offeredClause.changedClause().id()),
			title: ko.observable(offeredClause.changedClause().title()),
			content: ko.observable(offeredClause.changedClause().content())
		});
	};

	// EXCLUSION ACTIONS
	self.acceptExclusion = function (offeredExclusion) {
		self.clearChangedExclusionIfPresent(offeredExclusion);
		offeredExclusion.type('ACCEPTED');
	};

	self.refuseExclusion = function (offeredExclusion) {
		self.clearChangedExclusionIfPresent(offeredExclusion);
		offeredExclusion.type('REJECTED');
	};

	self.editExclusion = function (offeredExclusion) {
		self.clearChangedExclusionIfPresent(offeredExclusion);
		offeredExclusion.exclusionToBeChanged = ko.observable();
		offeredExclusion.type('CHANGED');
	};

	self.clearChangedExclusionIfPresent = function (offeredExclusion) {
		let changedExclusion = offeredExclusion.changedExclusion();
		if (changedExclusion === null) {
			return;
		}

		self.availableExclusions.remove(changedExclusion);
		self.availableExclusions.push({
			id: ko.observable(changedExclusion.id()),
			content: ko.observable(changedExclusion.content())
		});
		offeredExclusion.changedExclusion(null);
	};

	self.addChangedExclusion = function (offeredExclusion) {
		offeredExclusion.changedExclusion({
			id: ko.observable(offeredExclusion.exclusionToBeChanged().id()),
			content: ko.observable(offeredExclusion.exclusionToBeChanged().content())
		});
		self.availableExclusions.remove(offeredExclusion.exclusionToBeChanged());
		offeredExclusion.exclusionToBeChanged(null);
	};

	self.addNewExclusion = function () {
		self.offeredExclusions.push({
			originalExclusion: ko.observable(null),
			type: ko.observable('NEW'),
			changedExclusion: ko.observable({
				id: ko.observable(self.newExclusion().id()),
				content: ko.observable(self.newExclusion().content())
			})
		});

		self.availableExclusions.remove(self.newExclusion());
	};

	self.removeNewExclusion = function (offeredExclusion) {
		self.offeredExclusions.remove(offeredExclusion);

		self.availableExclusions.push({
			id: ko.observable(offeredExclusion.changedExclusion().id()),
			content: ko.observable(offeredExclusion.changedExclusion().content())
		});
	};

	// DEDUCTIBLE ACTIONS
	self.acceptDeductible = function (offeredDeductible) {
		self.clearChangedDeductibleIfPresent(offeredDeductible);
		offeredDeductible.type('ACCEPTED');
	};

	self.refuseDeductible = function (offeredDeductible) {
		self.clearChangedDeductibleIfPresent(offeredDeductible);
		offeredDeductible.type('REJECTED');
	};

	self.editDeductible = function (offeredDeductible) {
		self.clearChangedDeductibleIfPresent(offeredDeductible);
		offeredDeductible.newDeductibleDescription = ko.observable();
		offeredDeductible.type('CHANGED');
	};

	self.clearChangedDeductibleIfPresent = function (offeredDeductible) {
		let changedDeductible = offeredDeductible.changedDeductible();
		if (changedDeductible === null) {
			return;
		}

		offeredDeductible.changedDeductible(null);
	};

	self.addChangedDeductible = function (offeredDeductible) {
		offeredDeductible.changedDeductible({
			description: ko.observable(offeredDeductible.newDeductibleDescription())
		});

		offeredDeductible.newDeductibleDescription(null);
	};

	self.addNewDeductible = function () {
		self.offeredDeductibles.push({
			originalDeductible: ko.observable(null),
			type: ko.observable('NEW'),
			changedDeductible: ko.observable({
				description: ko.observable(self.newDeductibleDescription())
			})
		});

		self.newDeductibleDescription("");
	};

	self.removeNewDeductible = function (offeredDeductible) {
		self.offeredDeductibles.remove(offeredDeductible);
	};

	self.editPrice = function () {
		self.isPriceEditMode(true);
	};

	self.cancelEditPrice = function () {
		self.isPriceEditMode(false);
		self.priceInput(self.priceAmount());
		self.selectedCurrency(self.priceCurrency());
	};

	self.confirmPrice = function () {
		self.priceAmount(self.priceInput());
		self.priceCurrency(self.selectedCurrency());
		self.isPriceEditMode(false);
	};

	self.save = function () {
		let offeredClauses = [];
		for (var i = 0; i < self.offeredClauses().length; i++) {
			let originalClause = self.offeredClauses()[i].originalClause();
			let originalClauseDTO = null;
			if (originalClause !== null) {
				originalClauseDTO = {
					id: originalClause.id(),
					title: originalClause.title(),
					content: originalClause.content()
				}
			}

			let changedClause = self.offeredClauses()[i].changedClause();
			let changedClauseDTO = null;
			if (changedClause !== null) {
				changedClauseDTO = {
					id: changedClause.id(),
					title: changedClause.title(),
					content: changedClause.content()
				}
			}

			offeredClauses.push({
				offerId: currentOfferId,
				originalClause: originalClauseDTO,
				type: self.offeredClauses()[i].type(),
				changedClause: changedClauseDTO
			});
		}

		let offeredExclusions = [];
		for (var i = 0; i < self.offeredExclusions().length; i++) {
			let originalExclusion = self.offeredExclusions()[i].originalExclusion();
			let originalExclusionDTO = null;
			if (originalExclusion !== null) {
				originalExclusionDTO = {
					id: originalExclusion.id(),
					content: originalExclusion.content()
				}
			}

			let changedExclusion = self.offeredExclusions()[i].changedExclusion();
			let changedExclusionDTO = null;
			if (changedExclusion !== null) {
				changedExclusionDTO = {
					id: changedExclusion.id(),
					content: changedExclusion.content()
				}
			}

			offeredExclusions.push({
				offerId: currentOfferId,
				originalExclusion: originalExclusionDTO,
				type: self.offeredExclusions()[i].type(),
				changedExclusion: changedExclusionDTO
			});
		}

		let offeredDeductibles = [];
		for (var i = 0; i < self.offeredDeductibles().length; i++) {
			let originalDeductible = self.offeredDeductibles()[i].originalDeductible();
			let originalDeductibleDTO = null;
			if (originalDeductible !== null) {
				originalDeductibleDTO = {
					description: originalDeductible.description()
				}
			}

			let changedDeductible = self.offeredDeductibles()[i].changedDeductible();
			let changedDeductibleDTO = null;
			if (changedDeductible !== null) {
				changedDeductibleDTO = {
					description: changedDeductible.description()
				}
			}

			offeredDeductibles.push({
				offerId: currentOfferId,
				originalDeductible: originalDeductibleDTO,
				type: self.offeredDeductibles()[i].type(),
				changedDeductible: changedDeductibleDTO
			});
		}

		let updatePetitionRequest = {
			id: currentOfferId,
			offeredClauses: offeredClauses,
			offeredExclusions: offeredExclusions,
			offeredDeductibles: offeredDeductibles,
			amount: self.priceAmount(),
			currencyCode: self.priceCurrency()
		};

		self.ajax(self.offerURI + '/update', 'POST', updatePetitionRequest).done(function (data) {
			self.setOffer(data);
		});
	};

	self.goBack = function () {
		window.location.replace('offers.html');
	};

	self.ajax(self.offerURI + '/' + currentOfferId + '/clause/available', 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			self.availableClauses.push({
				id: ko.observable(data[i].id),
				title: ko.observable(data[i].title),
				content: ko.observable(data[i].content)
			});
		}
	});

	self.ajax(self.offerURI + '/' + currentOfferId + '/exclusion/available', 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			self.availableExclusions.push({
				id: ko.observable(data[i].id),
				content: ko.observable(data[i].content)
			});
		}
	});
}

let offerDetailsViewModel = new OfferDetailsViewModel();
ko.applyBindings(offerDetailsViewModel, $('#main')[0]);