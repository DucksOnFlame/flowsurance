function ComparisonsViewModel() {
	let self = this;
	self.comparisonURI = '/comparison';
	self.comparisons = ko.observableArray();

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.ajax(self.comparisonURI, 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			let comparison = data[i];
			self.comparisons.push({
				id: ko.observable(comparison.id),
				name: ko.observable(comparison.petition.name),
				offerCount: ko.observable(comparison.offers.length),
				type: ko.observable(comparison.petition.type),
				policyHolderName: ko.observable(comparison.petition.policyHolder.name),
				coverStartDate: ko.observable(comparison.petition.coverStartDate),
				coverEndDate: ko.observable(comparison.petition.coverEndDate)
			});
		}
	});

	self.goToDetails = function (comparison) {
		window.open("/comparisonDetails.html?id=" + comparison.id())
	};
}

let comparisonsViewModel = new ComparisonsViewModel();
ko.applyBindings(comparisonsViewModel, $('#main')[0]);