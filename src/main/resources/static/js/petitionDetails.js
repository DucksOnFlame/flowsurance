currentPetitionId = window.location.search.substring(4);

function PetitionDetailsViewModel() {
	let self = this;
	self.petitionURI = '/petition';

	self.subjectItem = ko.observable();

	self.availableClauses = ko.observableArray();
	self.selectedClause = ko.observable();

	self.availableExclusions = ko.observableArray();
	self.selectedExclusion = ko.observable();

	self.deductibleDescription = ko.observable();

	self.name = ko.observable();
	self.type = ko.observable();
	self.state = ko.observable();
	self.subjects = ko.observableArray();
	self.coverStartDate = ko.observable();
	self.coverEndDate = ko.observable();
	self.policyHolder = ko.observable();
	self.insureds = ko.observableArray();
	self.clauses = ko.observableArray();
	self.exclusions = ko.observableArray();
	self.deductibles = ko.observableArray();
	self.brokerDetails = ko.observable();

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.ajax(self.petitionURI + '/' + currentPetitionId, 'GET').done(function (data) {
		self.setPetition(data);
	});

	self.setPetition = function (petition) {
		self.name(petition.name);
		self.type(petition.type);
		self.state(petition.state);

		self.subjects([]);
		for (var i = 0; i < petition.subjects.length; i++) {
			self.subjects.push({
				item: ko.observable(petition.subjects[i].item)
			});
		}

		self.coverStartDate(petition.coverStartDate);
		self.coverEndDate(petition.coverEndDate);
		self.policyHolder(petition.policyHolder);

		self.insureds([]);
		for (var i = 0; i < petition.insureds.length; i++) {
			self.insureds.push({
				id: ko.observable(petition.insureds[i].id),
				name: ko.observable(petition.insureds[i].name),
				type: ko.observable(petition.insureds[i].type),
				street: ko.observable(petition.insureds[i].street),
				zipCode: ko.observable(petition.insureds[i].zipCode),
				city: ko.observable(petition.insureds[i].city),
				state: ko.observable(petition.insureds[i].state),
				country: ko.observable(petition.insureds[i].country),
				taxPayerId: ko.observable(petition.insureds[i].taxPayerId)
			});
		}

		self.clauses([]);
		for (var i = 0; i < petition.clauses.length; i++) {
			self.clauses.push({
				id: ko.observable(petition.clauses[i].id),
				title: ko.observable(petition.clauses[i].title),
				content: ko.observable(petition.clauses[i].content)
			});
		}

		self.exclusions([]);
		for (var i = 0; i < petition.exclusions.length; i++) {
			self.exclusions.push({
				id: ko.observable(petition.exclusions[i].id),
				content: ko.observable(petition.exclusions[i].content)
			});
		}

		self.deductibles([]);
		for (var i = 0; i < petition.deductibles.length; i++) {
			self.deductibles.push({
				description: ko.observable(petition.deductibles[i].description)
			});
		}

		let brokerDetails = petition.broker.name + ' ' + petition.broker.surname + ', ' + petition.broker.company.name;
		self.brokerDetails(brokerDetails);
	};

	self.addSubject = function () {
		if (!self.subjectItem()) {
			alert("Wpisz poprawny przedmiot ubezpieczenia!");
			return;
		}

		self.subjects.push({
			item: ko.observable(self.subjectItem())
		});

		self.subjectItem("");
	};

	self.removeSubject = function (subject) {
		self.subjects.remove(subject);
	};

	self.addClause = function () {
		if (!self.selectedClause()) {
			alert("Wybierz klauzulę!");
			return;
		}

		self.clauses.push(self.selectedClause());
		self.availableClauses.remove(self.selectedClause());
		self.selectedClause("");
	};

	self.removeClause = function (clause) {
		self.clauses.remove(clause);
		self.availableClauses.push({
			id: ko.observable(clause.id()),
			title: ko.observable(clause.title()),
			content: ko.observable(clause.content())
		});
	};

	self.addExclusion = function () {
		if (!self.selectedExclusion()) {
			alert("Wybierz wyłączenie!");
			return;
		}

		self.exclusions.push(self.selectedExclusion());
		self.availableExclusions.remove(self.selectedExclusion());
		self.selectedExclusion("");
	};

	self.removeExclusion = function (exclusion) {
		self.exclusions.remove(exclusion);
		self.availableExclusions.push({
			id: ko.observable(exclusion.id()),
			content: ko.observable(exclusion.content())
		});
	};

	self.addDeductible = function () {
		if (!self.deductibleDescription()) {
			alert("Wpisz poprawną franszyzę!");
			return;
		}

		self.deductibles.push({
			description: ko.observable(self.deductibleDescription())
		});

		self.deductibleDescription("");
	};

	self.removeDeductible = function (deductible) {
		self.deductibles.remove(deductible);
	};

	self.save = function () {
		let insuredIds = [];
		for (var i = 0; i < self.insureds().length; i++) {
			insuredIds.push(self.insureds()[i].id());
		}

		let clauseIds = [];
		for (var i = 0; i < self.clauses().length; i++) {
			clauseIds.push(self.clauses()[i].id());
		}

		let exclusionIds = [];
		for (var i = 0; i < self.exclusions().length; i++) {
			exclusionIds.push(self.exclusions()[i].id());
		}

		let deductibleDTOs = [];
		for (var i = 0; i < self.deductibles().length; i++) {
			deductibleDTOs.push({
				description: self.deductibles()[i].description()
			});
		}

		let subjectDTOs = [];
		for (var i = 0; i < self.subjects().length; i++) {
			subjectDTOs.push({
				item: self.subjects()[i].item()
			});
		}

		let updatePetitionRequest = {
			id: currentPetitionId,
			name: self.name(),
			type: self.type(),
			state: self.state(),
			subjects: subjectDTOs,
			coverStartDate: self.coverStartDate(),
			coverEndDate: self.coverEndDate(),
			policyHolderId: self.policyHolder().id,
			insuredIds: insuredIds,
			clauseIds: clauseIds,
			exclusionIds: exclusionIds,
			deductibles: deductibleDTOs
		};

		self.ajax(self.petitionURI + '/update', 'POST', updatePetitionRequest).done(function (data) {
			self.setPetition(data);
		});
	};

	self.ajax(self.petitionURI + '/' + currentPetitionId + '/clause/available', 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			self.availableClauses.push({
				id: ko.observable(data[i].id),
				title: ko.observable(data[i].title),
				content: ko.observable(data[i].content)
			});
		}
	});

	self.ajax(self.petitionURI + '/' + currentPetitionId + '/exclusion/available', 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			self.availableExclusions.push({
				id: ko.observable(data[i].id),
				content: ko.observable(data[i].content)
			});
		}
	});

	self.goBack = function () {
		window.location.replace('petitions.html');
	};
}

let petitionDetailsViewModel = new PetitionDetailsViewModel();
ko.applyBindings(petitionDetailsViewModel, $('#main')[0]);