function ClausesViewModel() {
	let self = this;
	self.clauses = ko.observableArray();
	self.clauseURI = '/clause';

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.beginAdd = function () {
		$('#add').modal('show');
	};

	self.remove = function (clause) {
		self.ajax(self.clauseURI + '/' + clause.id(), 'DELETE').done(function () {
			self.clauses.remove(clause);
		});
	};

	self.ajax(self.clauseURI, 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			self.clauses.push({
				id: ko.observable(data[i].id),
				title: ko.observable(data[i].title),
				content: ko.observable(data[i].content)
			});
		}
	});

	self.add = function (clause) {
		self.ajax(self.clauseURI + '/create', 'PUT', clause).done(function (data) {
			self.clauses.push({
				id: ko.observable(data.id),
				title: ko.observable(data.title),
				content: ko.observable(data.content)
			});
		});
	}
}

function AddClauseViewModel() {
	let self = this;
	self.title = ko.observable();
	self.content = ko.observable();

	self.addClause = function () {
		$('#add').modal('hide');
		clausesViewModel.add({
			title: self.title(),
			content: self.content()
		});
		self.title("");
		self.content("");
	}
}

let clausesViewModel = new ClausesViewModel();
let addClauseViewModel = new AddClauseViewModel();
ko.applyBindings(clausesViewModel, $('#main')[0]);
ko.applyBindings(addClauseViewModel, $('#add')[0]);