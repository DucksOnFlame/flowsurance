function ExclusionsViewModel() {
	let self = this;
	self.exclusions = ko.observableArray();
	self.exclusionURI = '/exclusion';

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.beginAdd = function () {
		$('#add').modal('show');
	};

	self.remove = function (exclusion) {
		self.ajax(self.exclusionURI + '/' + exclusion.id(), 'DELETE').done(function () {
			self.exclusions.remove(exclusion);
		});
	};

	self.ajax(self.exclusionURI, 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			self.exclusions.push({
				id: ko.observable(data[i].id),
				content: ko.observable(data[i].content)
			});
		}
	});

	self.add = function (exclusion) {
		self.ajax(self.exclusionURI + '/create', 'PUT', exclusion).done(function (data) {
			self.exclusions.push({
				id: ko.observable(data.id),
				content: ko.observable(data.content)
			});
		});
	}
}

function AddExclusionViewModel() {
	let self = this;
	self.content = ko.observable();

	self.addExclusion = function () {
		$('#add').modal('hide');
		exclusionsViewModel.add({
			content: self.content(),
		});
		self.content("");
	}
}

let exclusionsViewModel = new ExclusionsViewModel();
let addExclusionViewModel = new AddExclusionViewModel();
ko.applyBindings(exclusionsViewModel, $('#main')[0]);
ko.applyBindings(addExclusionViewModel, $('#add')[0]);