function PetitionsViewModel() {
	let self = this;
	self.petitions = ko.observableArray();
	self.petitionURI = '/petition';
	self.userURI = '/user';
	self.clientsURI = '/company';

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.beginAdd = function () {
		$('#add').modal('show');
	};

	self.remove = function (petition) {
		self.ajax(self.petitionURI + '/' + petition.id(), 'DELETE').done(function () {
			self.petitions.remove(petition);
		});
	};

	self.loadAllPetitions = function () {
		self.ajax(self.petitionURI, 'GET').done(function (data) {
			self.petitions([]);
			for (var i = 0; i < data.length; i++) {
				self.petitions.push({
					id: ko.observable(data[i].id),
					state: ko.observable(data[i].state),
					name: ko.observable(data[i].name),
					type: ko.observable(data[i].type),
					policyHolderName: ko.observable(data[i].policyHolderName),
					coverStartDate: ko.observable(data[i].coverStartDate),
					coverEndDate: ko.observable(data[i].coverEndDate)
				});
			}
		});
	};

	self.loadAllPetitions();

	self.add = function (petition) {
		self.ajax(self.petitionURI + '/create', 'PUT', petition).done(function (data) {
			self.petitions.push({
				id: ko.observable(data.id),
				state: ko.observable(data.state),
				name: ko.observable(data.name),
				type: ko.observable(data.type),
				policyHolderName: ko.observable(data.policyHolderName),
				coverStartDate: ko.observable(data.coverStartDate),
				coverEndDate: ko.observable(data.coverEndDate)
			});
		});
	};

	self.loadAllClients = function () {
		self.ajax(self.clientsURI, 'GET').done(function (data) {
			for (var i = 0; i < data.length; i++) {
				const address = data[i].street + ', ' + data[i].zipCode + ', ' + data[i].city + ', ' + data[i].state + ', ' + data[i].country;
				addPetitionViewModel.clients.push({
					id: ko.observable(data[i].id),
					name: ko.observable(data[i].name),
					type: ko.observable(data[i].type),
					address: ko.observable(address),
					taxPayerId: ko.observable(data[i].taxPayerId)
				});
			}
		});
	};

	self.loadAllAvailableTypes = function () {
		self.ajax(self.petitionURI + '/type', 'GET').done(function (data) {
			for (var i = 0; i < data.length; i++) {
				addPetitionViewModel.types.push(ko.observable(data[i]));
			}
		});
	};

	self.goToDetails = function (petition) {
		window.location.replace("/petitionDetails.html?id=" + petition.id())
	};

	self.beginSend = function (petition) {
		sendPetitionViewModel.petition = petition;
		$('#send').modal('show');

		self.ajax(self.userURI + '/insurer', 'GET').done(function (data) {
			for (var i = 0; i < data.length; i++) {
				sendPetitionViewModel.availableInsurers.push({
					id: ko.observable(data[i].id),
					name: ko.observable(data[i].name),
					surname: ko.observable(data[i].surname),
					companyName: ko.observable(data[i].company.name)
				});
			}
		});
	};

	self.send = function (petitionId, insurerIds) {
		let sendRequest = {
			petitionId: petitionId,
			insurerIds: insurerIds
		};

		self.ajax(self.petitionURI + '/send', 'POST', sendRequest).done(function (data) {
			self.loadAllPetitions();
		});
	};
}

function AddPetitionViewModel() {
	let self = this;
	self.clients = ko.observableArray();
	self.types = ko.observableArray();
	petitionsViewModel.loadAllClients();
	petitionsViewModel.loadAllAvailableTypes();

	self.name = ko.observable();
	self.selectedType = ko.observable();
	self.selectedClient = ko.observable();
	self.coverStartDate = ko.observable();
	self.coverEndDate = ko.observable();

	self.addPetition = function () {
		$('#add').modal('hide');
		petitionsViewModel.add({
			name: self.name(),
			type: self.selectedType(),
			policyHolderId: self.selectedClient().id(),
			coverStartDate: self.coverStartDate(),
			coverEndDate: self.coverEndDate(),
			insuredIds: [self.selectedClient().id()]
		});
		self.name("");
		self.selectedType("");
		self.selectedClient("");
		self.coverStartDate("");
		self.coverEndDate("");
	}
}

function SendPetitionViewModel() {
	let self = this;
	self.petition = {};
	self.insurerSendList = ko.observableArray();
	self.availableInsurers = ko.observableArray();
	self.selectedInsurer = ko.observable();

	self.sendPetition = function () {
		let insurerIds = [];
		for (var i = 0; i < self.insurerSendList().length; i++) {
			insurerIds.push(self.insurerSendList()[i].id());
		}

		$('#send').modal('hide');
		petitionsViewModel.send(
			self.petition.id(),
			insurerIds
		);

		self.insurerSendList([]);
		self.availableInsurers([]);
		self.petition = {};
		self.insurers = [];
	};

	self.addToSendList = function () {
		if (!self.selectedInsurer()) {
			alert("Wybierz adresata!");
			return;
		}

		self.insurerSendList.push(self.selectedInsurer());
		self.availableInsurers.remove(self.selectedInsurer());
		self.selectedInsurer({});
	};

	self.removeInsurerFromList = function (insurer) {
		self.insurerSendList.remove(insurer);
		self.availableInsurers.push({
			id: ko.observable(insurer.id()),
			name: ko.observable(insurer.name()),
			surname: ko.observable(insurer.surname()),
			companyName: ko.observable(insurer.companyName())
		});
	};
}

let petitionsViewModel = new PetitionsViewModel();
let addPetitionViewModel = new AddPetitionViewModel();
let sendPetitionViewModel = new SendPetitionViewModel();
ko.applyBindings(petitionsViewModel, $('#main')[0]);
ko.applyBindings(addPetitionViewModel, $('#add')[0]);
ko.applyBindings(sendPetitionViewModel, $('#send')[0]);