function OffersViewModel() {
	let self = this;
	self.offers = ko.observableArray();
	self.offerURI = '/offer';

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.loadAllOffers = function () {
		self.ajax(self.offerURI, 'GET').done(function (data) {
			self.offers([]);
			for (var i = 0; i < data.length; i++) {
				let petition = data[i].petition;
				let broker = petition.broker;
				let brokerDetails = broker.name + ' ' + broker.surname + ', ' + broker.company.name;
				self.offers.push({
					id: ko.observable(data[i].id),
					state: ko.observable(data[i].state),
					type: ko.observable(petition.type),
					policyHolderName: ko.observable(petition.policyHolder.name),
					coverStartDate: ko.observable(petition.coverStartDate),
					coverEndDate: ko.observable(petition.coverEndDate),
					brokerDetails: ko.observable(brokerDetails)
				});
			}
		});
	};

	self.loadAllOffers();

	self.edit = function (offer) {
		window.location.replace("/offerDetails.html?id=" + offer.id())
	};

	self.send = function (offer) {
		let sendRequest = {
			offerId: offer.id(),
		};

		self.ajax(self.offerURI + '/send', 'POST', sendRequest).done(function (data) {
			self.loadAllOffers();
		});
	};
}

let offersViewModel = new OffersViewModel();
ko.applyBindings(offersViewModel, $('#main')[0]);