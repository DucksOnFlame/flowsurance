function ClientsViewModel() {
	let self = this;
	self.clients = ko.observableArray();
	self.companyURI = '/company';

	self.ajax = function (uri, method, data) {
		let request = {
			url: uri,
			type: method,
			contentType: "application/json",
			accept: "application/json",
			cache: false,
			dataType: 'json',
			data: JSON.stringify(data),
			error: function (jqXHR) {
				console.log("ajax error " + jqXHR.status);
			}
		};
		return $.ajax(request);
	};

	self.beginAdd = function () {
		$('#add').modal('show');
	};

	self.remove = function (client) {
		self.ajax(self.companyURI + '/' + client.id(), 'DELETE').done(function () {
			self.clients.remove(client);
		});
	};

	self.ajax(self.companyURI, 'GET').done(function (data) {
		for (var i = 0; i < data.length; i++) {
			const address = data[i].street + ', ' + data[i].zipCode + ', ' + data[i].city + ', ' + data[i].state + ', ' + data[i].country;
			self.clients.push({
				id: ko.observable(data[i].id),
				name: ko.observable(data[i].name),
				type: ko.observable(data[i].type),
				address: ko.observable(address),
				taxPayerId: ko.observable(data[i].taxPayerId)
			});
		}
	});

	self.add = function (client) {
		self.ajax(self.companyURI + '/create', 'PUT', client).done(function (data) {
			const address = data.street + ', ' + data.zipCode + ', ' + data.city + ', ' + data.state + ', ' + data.country;
			self.clients.push({
				id: ko.observable(data.id),
				name: ko.observable(data.name),
				type: ko.observable(data.type),
				address: ko.observable(address),
				taxPayerId: ko.observable(data.taxPayerId)
			});
		});
	}
}

function AddClientViewModel() {
	let self = this;
	self.name = ko.observable();
	self.type = ko.observable("CLIENT");
	self.street = ko.observable();
	self.zipCode = ko.observable();
	self.city = ko.observable();
	self.state = ko.observable();
	self.country = ko.observable();
	self.taxPayerId = ko.observable();

	self.addClient = function () {
		$('#add').modal('hide');
		clientsViewModel.add({
			name: self.name(),
			type: self.type(),
			street: self.street(),
			zipCode: self.zipCode(),
			city: self.city(),
			state: self.state(),
			country: self.country(),
			taxPayerId: self.taxPayerId()
		});
		self.name("");
		self.type("");
		self.street("");
		self.zipCode("");
		self.city("");
		self.state("");
		self.country("");
		self.taxPayerId("");
	}
}

let clientsViewModel = new ClientsViewModel();
let addClientViewModel = new AddClientViewModel();
ko.applyBindings(clientsViewModel, $('#main')[0]);
ko.applyBindings(addClientViewModel, $('#add')[0]);