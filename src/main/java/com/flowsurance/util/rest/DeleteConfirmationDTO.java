package com.flowsurance.util.rest;

public class DeleteConfirmationDTO {

	private final static DeleteConfirmationDTO instance = new DeleteConfirmationDTO();

	private final String message = "Record deleted";

	private DeleteConfirmationDTO() {
	}

	public static DeleteConfirmationDTO get() {
		return instance;
	}
}
