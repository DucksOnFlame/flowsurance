package com.flowsurance.util.rest;

public class AuthorizedDTO {

	private final static AuthorizedDTO instance = new AuthorizedDTO();

	private final String message = "Authorized!";

	private AuthorizedDTO() {
	}

	public static AuthorizedDTO get() {
		return instance;
	}
}
