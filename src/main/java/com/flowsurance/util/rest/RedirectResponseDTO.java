package com.flowsurance.util.rest;

public class RedirectResponseDTO {

	private String path;

	public RedirectResponseDTO(String path) {
		this.path = path;
	}
}
