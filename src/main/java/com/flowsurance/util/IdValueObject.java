package com.flowsurance.util;

import java.io.Serializable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import static com.google.common.base.Preconditions.checkNotNull;

public class IdValueObject implements Serializable, Comparable<IdValueObject> {

	private String id;

	public IdValueObject(String id) {
		this.id = checkNotNull(id);
	}

	public String get() {
		return id;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder(17, 37);
		builder.append(id);
		return builder.toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		IdValueObject that = (IdValueObject) o;

		return new EqualsBuilder()
				.append(id, that.id)
				.isEquals();
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public int compareTo(IdValueObject otherObject) {
		return id.compareTo(otherObject.id);
	}
}
