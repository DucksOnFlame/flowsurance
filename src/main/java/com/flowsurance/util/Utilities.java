package com.flowsurance.util;

import com.google.common.base.Preconditions;

public class Utilities {

	public static void checkNoneNull(Object... arguments) {
		Preconditions.checkNotNull(arguments);
		for (Object o : arguments) {
			Preconditions.checkNotNull(o);
		}
	}

	public static void checkAtLeastOneNotNull(Object... arguments) {
		Preconditions.checkNotNull(arguments);

		int counter = 0;
		for (Object o : arguments) {
			if (o == null) {
				counter++;
			}
		}

		if (counter >= arguments.length) {
			throw new NullPointerException("All arguments are null!");
		}
	}
}
