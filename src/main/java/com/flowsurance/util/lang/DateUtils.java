package com.flowsurance.util.lang;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {

	public static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd");

	public static DateTime fromString(String dateTime) {
		return new DateTime(dateTime);
	}

	public static String dateTimeToString(DateTime dateTime) {
		return dateTime.toString(DATE_FORMAT);
	}
}
