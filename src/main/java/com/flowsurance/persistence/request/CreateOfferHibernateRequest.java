package com.flowsurance.persistence.request;

import com.flowsurance.persistence.hibernate.UserHibernate;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;

public class CreateOfferHibernateRequest {

	private final PetitionHibernate petition;

	private final UserHibernate insurer;

	public CreateOfferHibernateRequest(PetitionHibernate petition,
			UserHibernate insurer) {
		this.petition = petition;
		this.insurer = insurer;
	}

	public PetitionHibernate getPetition() {
		return petition;
	}

	public UserHibernate getInsurer() {
		return insurer;
	}
}
