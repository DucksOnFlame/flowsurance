package com.flowsurance.persistence.request;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedDeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedExclusionHibernate;
import com.google.common.collect.Lists;
import java.util.List;
import org.joda.money.BigMoney;

public class UpdateOfferHibernateRequest {

	private final OfferId offerId;

	private final List<OfferedClauseHibernate> offeredClauses;

	private final List<OfferedExclusionHibernate> offeredExclusions;

	private final List<OfferedDeductibleHibernate> offeredDeductibles;

	private final BigMoney price;

	public UpdateOfferHibernateRequest(OfferId offerId,
			List<OfferedClauseHibernate> offeredClauses,
			List<OfferedExclusionHibernate> offeredExclusions,
			List<OfferedDeductibleHibernate> offeredDeductibles,
			BigMoney price) {
		this.offerId = offerId;
		this.offeredClauses = offeredClauses;
		this.offeredExclusions = offeredExclusions;
		this.offeredDeductibles = offeredDeductibles;
		this.price = price;
	}

	public OfferId getOfferId() {
		return offerId;
	}

	public List<OfferedClauseHibernate> getOfferedClauses() {
		return Lists.newArrayList(offeredClauses);
	}

	public List<OfferedExclusionHibernate> getOfferedExclusions() {
		return Lists.newArrayList(offeredExclusions);
	}

	public List<OfferedDeductibleHibernate> getOfferedDeductibles() {
		return Lists.newArrayList(offeredDeductibles);
	}

	public BigMoney getPrice() {
		return price;
	}
}
