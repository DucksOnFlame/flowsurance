package com.flowsurance.persistence.request;

import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.conditions.petition.PetitionName;
import com.flowsurance.domain.model.conditions.petition.PetitionState;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import com.flowsurance.persistence.hibernate.conditions.ClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.ExclusionHibernate;
import com.flowsurance.persistence.hibernate.conditions.SubjectHibernate;
import com.google.common.collect.Lists;
import java.util.List;
import org.joda.time.DateTime;

public class UpdatePetitionHibernateRequest {

	private final PetitionId id;

	private final PetitionName name;

	private final InsuranceConditionsType type;

	private final List<SubjectHibernate> subjects;

	private final PetitionState state;

	private final DateTime coverStartDate;

	private final DateTime coverEndDate;

	private final CompanyHibernate policyHolder;

	private final List<CompanyHibernate> insureds;

	private final List<ClauseHibernate> clauses;

	private final List<ExclusionHibernate> exclusions;

	private final List<DeductibleHibernate> deductibles;

	public UpdatePetitionHibernateRequest(PetitionId id,
			PetitionName name,
			InsuranceConditionsType type,
			List<SubjectHibernate> subjects,
			PetitionState state,
			DateTime coverStartDate,
			DateTime coverEndDate,
			CompanyHibernate policyHolder,
			List<CompanyHibernate> insureds,
			List<ClauseHibernate> clauses,
			List<ExclusionHibernate> exclusions, List<DeductibleHibernate> deductibles) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.subjects = subjects;
		this.state = state;
		this.coverStartDate = coverStartDate;
		this.coverEndDate = coverEndDate;
		this.policyHolder = policyHolder;
		this.insureds = insureds;
		this.clauses = clauses;
		this.exclusions = exclusions;
		this.deductibles = deductibles;
	}

	public PetitionId getId() {
		return id;
	}

	public PetitionName getName() {
		return name;
	}

	public InsuranceConditionsType getType() {
		return type;
	}

	public PetitionState getState() {
		return state;
	}

	public List<SubjectHibernate> getSubjects() {
		return Lists.newArrayList(subjects);
	}

	public DateTime getCoverStartDate() {
		return coverStartDate;
	}

	public DateTime getCoverEndDate() {
		return coverEndDate;
	}

	public CompanyHibernate getPolicyHolder() {
		return policyHolder;
	}

	public List<CompanyHibernate> getInsureds() {
		return insureds;
	}

	public List<ClauseHibernate> getClauses() {
		return clauses;
	}

	public List<ExclusionHibernate> getExclusions() {
		return exclusions;
	}

	public List<DeductibleHibernate> getDeductibles() {
		return deductibles;
	}
}
