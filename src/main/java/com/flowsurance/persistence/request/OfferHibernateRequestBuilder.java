package com.flowsurance.persistence.request;

import com.flowsurance.domain.model.conditions.offer.CreateOfferRequest;
import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.domain.model.conditions.offer.UpdateOfferRequest;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.persistence.dao.UserDao;
import com.flowsurance.persistence.dao.conditions.PetitionDao;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedDeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedExclusionHibernate;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferHibernateRequestBuilder {

	private final PetitionDao petitionDao;

	private final UserDao userDao;

	@Autowired
	public OfferHibernateRequestBuilder(PetitionDao petitionDao, UserDao userDao) {
		this.petitionDao = petitionDao;
		this.userDao = userDao;
	}

	public CreateOfferHibernateRequest buildCreateRequest(CreateOfferRequest request) {
		return new CreateOfferHibernateRequest(
				petitionDao.get(request.getPetitionId()).get(),
				userDao.get(request.getInsurerUserId()).get()
		);
	}

	public UpdateOfferHibernateRequest buildUpdateRequest(UpdateOfferRequest request) {
		OfferId offerId = request.getId();

		List<OfferedClauseHibernate> offeredClauses = request.getOfferedClauses().stream()
				.map(offeredClauseDTO -> new OfferedClauseHibernate(offerId,
						offeredClauseDTO.getOriginalClause().transform(ClauseDTO::getId),
						offeredClauseDTO.getType(),
						offeredClauseDTO.getChangedClause().transform(ClauseDTO::getId)))
				.collect(Collectors.toList());

		List<OfferedExclusionHibernate> offeredExclusions = request.getOfferedExclusions().stream()
				.map(offeredExclusionDTO -> new OfferedExclusionHibernate(offerId,
						offeredExclusionDTO.getOriginalExclusion().transform(ExclusionDTO::getId),
						offeredExclusionDTO.getType(),
						offeredExclusionDTO.getChangedExclusion().transform(ExclusionDTO::getId)))
				.collect(Collectors.toList());

		List<OfferedDeductibleHibernate> offeredDeductibles = request.getOfferedDeductibles().stream()
				.map(offeredDeductibleDTO -> new OfferedDeductibleHibernate(offerId,
						offeredDeductibleDTO.getOriginalDeductible().transform(dto -> new DeductibleHibernate(dto.getDescription())),
						offeredDeductibleDTO.getType(),
						offeredDeductibleDTO.getChangedDeductible().transform(changedExclusion -> new DeductibleHibernate(changedExclusion.getDescription()))))
				.collect(Collectors.toList());

		return new UpdateOfferHibernateRequest(
				offerId,
				offeredClauses,
				offeredExclusions,
				offeredDeductibles,
				request.getPrice()
		);
	}

	public UpdateOfferHibernateRequest buildCreateOfferedEntitiesRequest(Offer offer) {
		Petition petition = offer.getPetition();
		OfferId offerId = offer.getId();

		List<OfferedClauseHibernate> offeredClauses = petition.getClauses().stream()
				.map(clause -> new OfferedClauseHibernate(offerId,
						Optional.of(clause.getId()),
						OfferedConditionType.TBD,
						Optional.absent()))
				.collect(Collectors.toList());

		List<OfferedExclusionHibernate> offeredExclusions = petition.getExclusions().stream()
				.map(exclusion -> new OfferedExclusionHibernate(offerId,
						Optional.of(exclusion.getId()),
						OfferedConditionType.TBD,
						Optional.absent()))
				.collect(Collectors.toList());

		List<OfferedDeductibleHibernate> offeredDeductibles = petition.getDeductibles().stream()
				.map(deductible -> new OfferedDeductibleHibernate(offerId,
						Optional.of(new DeductibleHibernate(deductible)),
						OfferedConditionType.TBD,
						Optional.absent()))
				.collect(Collectors.toList());

		return new UpdateOfferHibernateRequest(
				offerId,
				offeredClauses,
				offeredExclusions,
				offeredDeductibles,
				offer.getPrice()
		);
	}
}
