package com.flowsurance.persistence.request;

import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.petition.PetitionName;
import com.flowsurance.domain.model.conditions.petition.PetitionState;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.flowsurance.persistence.hibernate.conditions.StakesHibernate;
import com.google.common.collect.Lists;
import java.util.List;
import org.joda.time.DateTime;

public class CreatePetitionHibernateRequest {

	private final PetitionName name;

	private final InsuranceConditionsType type;

	private final PetitionState state;

	private final DateTime coverStart;

	private final DateTime coverEnd;

	private final CompanyHibernate policyHolder;

	private final List<CompanyHibernate> insureds;

	private final List<StakesHibernate> brokerStakes;

	private final UserHibernate broker;

	public CreatePetitionHibernateRequest(PetitionName name,
			InsuranceConditionsType type,
			PetitionState state,
			DateTime coverStart,
			DateTime coverEnd,
			CompanyHibernate policyHolder,
			List<CompanyHibernate> insureds,
			List<StakesHibernate> brokerStakes, UserHibernate broker) {
		this.name = name;
		this.type = type;
		this.state = state;
		this.coverStart = coverStart;
		this.coverEnd = coverEnd;
		this.policyHolder = policyHolder;
		this.insureds = insureds;
		this.brokerStakes = brokerStakes;
		this.broker = broker;
	}

	public PetitionName getName() {
		return name;
	}

	public InsuranceConditionsType getType() {
		return type;
	}

	public PetitionState getState() {
		return state;
	}

	public DateTime getCoverStart() {
		return coverStart;
	}

	public DateTime getCoverEnd() {
		return coverEnd;
	}

	public CompanyHibernate getPolicyHolder() {
		return policyHolder;
	}

	public List<CompanyHibernate> getInsureds() {
		return Lists.newArrayList(insureds);
	}

	public List<StakesHibernate> getBrokerStakes() {
		return Lists.newArrayList(brokerStakes);
	}

	public UserHibernate getBroker() {
		return broker;
	}
}
