package com.flowsurance.persistence.request;

import com.flowsurance.domain.model.conditions.petition.CreatePetitionRequest;
import com.flowsurance.domain.model.conditions.petition.UpdatePetitionRequest;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.persistence.dao.CompanyDao;
import com.flowsurance.persistence.dao.UserDao;
import com.flowsurance.persistence.dao.conditions.ClauseDao;
import com.flowsurance.persistence.dao.conditions.ExclusionDao;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.flowsurance.persistence.hibernate.conditions.ClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.ExclusionHibernate;
import com.flowsurance.persistence.hibernate.conditions.StakesHibernate;
import com.flowsurance.persistence.hibernate.conditions.SubjectHibernate;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetitionHibernateRequestBuilder {

	private final CompanyDao companyDao;

	private final ClauseDao clauseDao;

	private final ExclusionDao exclusionDao;

	private final UserDao userDao;

	@Autowired
	public PetitionHibernateRequestBuilder(CompanyDao companyDao,
			ClauseDao clauseDao,
			ExclusionDao exclusionDao,
			UserDao userDao) {
		this.companyDao = companyDao;
		this.clauseDao = clauseDao;
		this.exclusionDao = exclusionDao;
		this.userDao = userDao;
	}

	public CreatePetitionHibernateRequest buildCreateRequest(CreatePetitionRequest request) {
		CompanyHibernate policyHolder = companyDao.get(request.getPolicyHolderId()).orNull();
		if (policyHolder == null) {
			throw new IllegalArgumentException("Could not find PolicyHolder by id! Id: " + request.getPolicyHolderId());
		}

		List<CompanyHibernate> insureds = request.getInsuredIds().stream()
				.map(insuredId -> companyDao.get(insuredId).orNull())
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		Map<CompanyId, Float> brokerStakes = request.getBrokerStakes().getAllStakesByIds();
		List<CompanyHibernate> stakesBrokers = companyDao.getByIds(brokerStakes.keySet());

		List<StakesHibernate> brokerStakesHibernate = Lists.newArrayList();
		for (CompanyHibernate broker : stakesBrokers) {
			brokerStakesHibernate.add(new StakesHibernate(broker, brokerStakes.get(broker.getId())));
		}

		UserHibernate broker = userDao.get(request.getBroker().getId()).get();

		return new CreatePetitionHibernateRequest(
				request.getName(),
				request.getType(),
				request.getState(),
				request.getCoverStartDate(),
				request.getCoverEndDate(),
				policyHolder,
				insureds,
				brokerStakesHibernate,
				broker);
	}

	public UpdatePetitionHibernateRequest buildUpdateRequest(UpdatePetitionRequest request) {
		CompanyHibernate policyHolder = companyDao.get(request.getPolicyHolderId()).orNull();
		if (policyHolder == null) {
			throw new IllegalArgumentException("Could not find PolicyHolder by id! Id: " + request.getPolicyHolderId());
		}

		List<SubjectHibernate> subjects = request.getSubjects().stream()
				.map(dto -> new SubjectHibernate(dto.getItem()))
				.collect(Collectors.toList());

		List<CompanyHibernate> insureds = request.getInsuredIds().stream()
				.map(insuredId -> companyDao.get(insuredId).orNull())
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		List<ClauseHibernate> clauses = request.getClauseIds().stream()
				.map(clauseDao::get)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toList());

		List<ExclusionHibernate> exclusions = request.getExclusionIds().stream()
				.map(exclusionDao::get)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toList());

		List<DeductibleHibernate> deductibles = request.getDeductibles().stream()
				.map(DeductibleHibernate::new)
				.collect(Collectors.toList());

		return new UpdatePetitionHibernateRequest(
				request.getId(),
				request.getName(),
				request.getType(),
				subjects,
				request.getState(),
				request.getCoverStartDate(),
				request.getCoverEndDate(),
				policyHolder,
				insureds,
				clauses,
				exclusions,
				deductibles
		);
	}
}
