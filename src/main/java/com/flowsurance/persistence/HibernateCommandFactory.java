package com.flowsurance.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.springframework.stereotype.Service;

@Service
public class HibernateCommandFactory {

	private final EntityManagerFactory entityManagerFactory;

	public HibernateCommandFactory() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("flowsuranceMain");
	}

	public <T> HibernateCommand<T> initializeHibernateCommand(HibernateCommand<T> command) {
		if (command == null) {
			throw new IllegalArgumentException("Command cannot be null!");
		}
		command.setEntityManager(entityManagerFactory.createEntityManager());
		return command;
	}
}
