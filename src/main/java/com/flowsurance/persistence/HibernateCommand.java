package com.flowsurance.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class HibernateCommand<T> {

	private EntityManager entityManager;

	public abstract T perform();

	public EntityManager getEntityManager() {
		return entityManager;
	}

	void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public T run() {
		EntityTransaction transaction = entityManager.getTransaction();
		transaction.begin();
		T result;
		try {
			result = perform();
			transaction.commit();
		} catch (RuntimeException e) {
			entityManager.getTransaction().rollback();
			throw e;
		} finally {
			entityManager.close();
		}

		return result;
	}
}
