package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.user.UserId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "company_user_mapping")
public class CompanyUserMappingHibernate extends StringIdModelHibernate {

	private String userId;

	private String companyId;

	public CompanyUserMappingHibernate() {
	}

	public CompanyUserMappingHibernate(UserId userId, CompanyId companyId) {
		this.userId = userId.get();
		this.companyId = companyId.get();
	}

	public UserId getUserId() {
		return new UserId(userId);
	}

	public CompanyId getCompanyId() {
		return new CompanyId(companyId);
	}
}
