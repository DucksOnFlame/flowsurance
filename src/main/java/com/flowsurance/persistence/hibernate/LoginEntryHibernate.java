package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.user.LoginId;
import com.flowsurance.domain.model.user.UserId;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "login_entry",
		indexes = {@Index(name = "user_id_index", columnList = "user_id", unique = true)})
public class LoginEntryHibernate extends StringIdModelHibernate {

	@Column(name = "user_id", nullable = false, unique = true)
	private String userId;

	@Column(nullable = false)
	private String password;

	protected LoginEntryHibernate() {
	}

	public LoginEntryHibernate(UserId userId, String hashedPassword) {
		this.userId = userId.get();
		this.password = hashedPassword;
	}

	public LoginId getId() {
		return new LoginId(id);
	}

	public UserId getUserId() {
		return new UserId(userId);
	}

	public String getHashedPassword() {
		return password;
	}

	public void setHashedPassword(String psw) {
		this.password = psw;
	}
}
