package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.user.Email;
import com.flowsurance.domain.model.user.JobTitle;
import com.flowsurance.domain.model.user.UserDisplayName;
import com.flowsurance.domain.model.user.UserId;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class UserHibernate extends StringIdModelHibernate {

	@Embedded
	private UserNameHibernate userName;

	@Column(unique = true, nullable = false)
	private String displayName;

	@OneToOne
	@JoinColumn(nullable = false)
	private CompanyHibernate company;

	@Column(nullable = false)
	private String email;

	@Column(name = "job_title")
	private String jobTitle;

	public UserHibernate() {
	}

	public UserHibernate(UserNameHibernate userName,
			UserDisplayName displayName,
			CompanyHibernate company,
			Email email,
			JobTitle jobTitle) {
		this.userName = userName;
		this.displayName = displayName.get();
		this.company = company;
		this.email = email.get();
		this.jobTitle = jobTitle.get();
	}

	public UserId getId() {
		return new UserId(id);
	}

	public UserNameHibernate getUserName() {
		return userName;
	}

	public UserDisplayName getDisplayName() {
		return new UserDisplayName(displayName);
	}

	public CompanyHibernate getCompany() {
		return company;
	}

	public Email getEmail() {
		return new Email(email);
	}

	public JobTitle getJobTitle() {
		return new JobTitle(jobTitle);
	}
}
