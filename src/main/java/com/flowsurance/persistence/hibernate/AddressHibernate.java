package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.address.Address;
import com.flowsurance.domain.model.address.AddressId;
import com.flowsurance.domain.model.address.City;
import com.flowsurance.domain.model.address.Country;
import com.flowsurance.domain.model.address.State;
import com.flowsurance.domain.model.address.StreetAddress;
import com.flowsurance.domain.model.address.ZipCode;
import com.neovisionaries.i18n.CountryCode;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class AddressHibernate extends StringIdModelHibernate {

	@Column(nullable = false)
	private String streetAddress;

	@Column
	private String zipCode;

	@Column(nullable = false)
	private String city;

	@Column
	private String state;

	@Column(nullable = false)
	private String countryCode;

	public AddressHibernate() {
	}

	public AddressHibernate(Address address) {
		this.streetAddress = address.getStreetAddress().get();
		this.zipCode = address.getZipCode().get();
		this.city = address.getCity().get();
		this.state = address.getState().get();
		this.countryCode = address.getCountry().getCode().toString();
	}

	public AddressHibernate(StreetAddress streetAddress,
			ZipCode zipCode,
			City city,
			State state,
			Country country) {
		this.streetAddress = streetAddress.get();
		this.zipCode = zipCode.get();
		this.city = city.get();
		this.state = state.get();
		this.countryCode = country.getCode().toString();
	}

	public AddressId getId() {
		return new AddressId(id);
	}

	public StreetAddress getStreetAddress() {
		return new StreetAddress(streetAddress);
	}

	public void setStreetAddress(StreetAddress streetAddress) {
		this.streetAddress = streetAddress.get();
	}

	public ZipCode getZipCode() {
		return new ZipCode(zipCode);
	}

	public void setZipCode(ZipCode zipCode) {
		this.zipCode = zipCode.get();
	}

	public City getCity() {
		return new City(city);
	}

	public void setCity(City city) {
		this.city = city.get();
	}

	public State getState() {
		return new State(state);
	}

	public void setState(State state) {
		this.state = state.get();
	}

	public Country getCountry() {
		return Country.valueOf(CountryCode.valueOf(countryCode));
	}

	public void setCountryCode(Country country) {
		this.countryCode = country.getCode().toString();
	}
}
