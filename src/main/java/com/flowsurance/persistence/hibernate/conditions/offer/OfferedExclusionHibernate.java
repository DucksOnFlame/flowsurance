package com.flowsurance.persistence.hibernate.conditions.offer;

import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.google.common.base.Optional;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OfferedExclusionHibernate {

	@Column(nullable = false)
	private String offerId;

	@Column(nullable = false)
	private String originalExclusionId;

	@Column(nullable = false)
	private OfferedConditionType type;

	@Column(nullable = true)
	private String changedExclusionId;

	public OfferedExclusionHibernate() {
	}

	public OfferedExclusionHibernate(OfferId offerId,
			Optional<ExclusionId> originalExclusionId,
			OfferedConditionType type,
			Optional<ExclusionId> changedExclusionId) {
		this.offerId = offerId.get();
		this.originalExclusionId = originalExclusionId.transform(ExclusionId::get).orNull();
		this.type = type;
		this.changedExclusionId = changedExclusionId.transform(ExclusionId::get).orNull();
	}

	public OfferId getOfferId() {
		return new OfferId(offerId);
	}

	public void setOfferId(OfferId offerId) {
		this.offerId = offerId.get();
	}

	public Optional<ExclusionId> getOriginalExclusionId() {
		return Optional.fromNullable(originalExclusionId).transform(ExclusionId::new);
	}

	public void setoriginalExclusionId(Optional<ExclusionId> originalExclusionId) {
		this.originalExclusionId = originalExclusionId.transform(ExclusionId::get).orNull();
	}

	public OfferedConditionType getType() {
		return type;
	}

	public void setType(OfferedConditionType type) {
		this.type = type;
	}

	public Optional<ExclusionId> getChangedExclusionId() {
		return Optional.fromNullable(changedExclusionId).transform(ExclusionId::new);
	}

	public void setchangedExclusionId(Optional<ExclusionId> changedExclusionId) {
		this.changedExclusionId = changedExclusionId.transform(ExclusionId::get).orNull();
	}
}
