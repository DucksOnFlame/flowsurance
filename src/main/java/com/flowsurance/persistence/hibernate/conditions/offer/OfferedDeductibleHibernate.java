package com.flowsurance.persistence.hibernate.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import com.google.common.base.Optional;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class OfferedDeductibleHibernate {

	@Column(nullable = false)
	private String offerId;

	@Embedded
	@Column(nullable = false)
	private DeductibleHibernate originalDeductible;

	@Column(nullable = false)
	private OfferedConditionType type;

	@Embedded
	@Column(nullable = true)
	private DeductibleHibernate changedDeductible;

	public OfferedDeductibleHibernate() {
	}

	public OfferedDeductibleHibernate(OfferId offerId,
			Optional<DeductibleHibernate> originalDeductible,
			OfferedConditionType type,
			Optional<DeductibleHibernate> changedDeductible) {
		this.offerId = offerId.get();
		this.originalDeductible = originalDeductible.orNull();
		this.type = type;
		this.changedDeductible = changedDeductible.orNull();
	}

	public OfferId getOfferId() {
		return new OfferId(offerId);
	}

	public void setOfferId(OfferId offerId) {
		this.offerId = offerId.get();
	}

	public Optional<DeductibleHibernate> getOriginalDeductible() {
		return Optional.fromNullable(originalDeductible);
	}

	public void setOriginalDeductible(Optional<DeductibleHibernate> originalDeductible) {
		this.originalDeductible = originalDeductible.orNull();
	}

	public OfferedConditionType getType() {
		return type;
	}

	public void setType(OfferedConditionType type) {
		this.type = type;
	}

	public Optional<DeductibleHibernate> getChangedDeductible() {
		return Optional.fromNullable(changedDeductible);
	}

	public void setChangedDeductible(Optional<DeductibleHibernate> changedDeductible) {
		this.changedDeductible = changedDeductible.orNull();
	}
}
