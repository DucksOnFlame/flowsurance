package com.flowsurance.persistence.hibernate.conditions;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SubjectHibernate {

	@Column(nullable = false)
	private String item;

	public SubjectHibernate() {
	}

	public SubjectHibernate(String item) {
		this.item = item;
	}

	public String getItem() {
		return item;
	}
}
