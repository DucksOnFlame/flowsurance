package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "petition_user_mapping")
public class PetitionUserMappingHibernate extends StringIdModelHibernate {

	@Column(name = "user_id")
	private String userId;

	@Column(name = "policy_id")
	private String conditionsId;

	public PetitionUserMappingHibernate() {
	}

	public PetitionUserMappingHibernate(UserId userId, PetitionId conditionsId) {
		this.userId = userId.get();
		this.conditionsId = conditionsId.get();
	}

	public UserId getUserId() {
		return new UserId(userId);
	}

	public PetitionId getConditionsId() {
		return new PetitionId(conditionsId);
	}
}
