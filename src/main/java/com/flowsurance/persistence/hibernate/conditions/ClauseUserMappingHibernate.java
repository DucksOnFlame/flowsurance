package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "clause_user_mapping")
public class ClauseUserMappingHibernate extends StringIdModelHibernate {

	private String userId;

	private String clauseId;

	public ClauseUserMappingHibernate() {
	}

	public ClauseUserMappingHibernate(UserId userId, ClauseId clauseId) {
		this.userId = userId.get();
		this.clauseId = clauseId.get();
	}

	public UserId getUserId() {
		return new UserId(userId);
	}

	public ClauseId getClauseId() {
		return new ClauseId(clauseId);
	}
}
