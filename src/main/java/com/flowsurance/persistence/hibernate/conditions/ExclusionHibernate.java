package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "exclusion")
public class ExclusionHibernate extends StringIdModelHibernate {

	@Column
	private String content;

	public ExclusionHibernate() {
	}

	public ExclusionHibernate(String content) {
		this.content = content;
	}

	public ExclusionId getId() {
		return new ExclusionId(id);
	}

	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(id)
				.append(content)
				.build();
	}
}
