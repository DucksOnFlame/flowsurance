package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.Limit;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;

@Embeddable
public class LimitHibernate implements Serializable {

	@Column(nullable = false)
	private String description;

	@Column
	private BigDecimal valuePerOccurrence;

	@Column
	private String occurrenceCurrency;

	@Column
	private BigDecimal valueInAggregate;

	@Column
	private String aggregateCurrency;

	public LimitHibernate() {
	}

	public LimitHibernate(Limit limit) {
		this.description = limit.getDescription();
		BigMoney perOccurrence = limit.getValuePerOccurrence().orNull();
		if (perOccurrence != null) {
			this.valuePerOccurrence = perOccurrence.getAmount();
			this.occurrenceCurrency = perOccurrence.getCurrencyUnit().getCode();
		}

		BigMoney inAggregate = limit.getValueInAggregate().orNull();
		if (inAggregate != null) {
			this.valueInAggregate = inAggregate.getAmount();
			this.aggregateCurrency = inAggregate.getCurrencyUnit().getCode();
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigMoney getValuePerOccurrence() {
		return BigMoney.of(CurrencyUnit.of(occurrenceCurrency), valuePerOccurrence);
	}

	public void setValuePerOccurrence(BigMoney valuePerOccurrence) {
		this.valuePerOccurrence = valuePerOccurrence.getAmount();
		this.occurrenceCurrency = valuePerOccurrence.getCurrencyUnit().getCode();
	}

	public BigMoney getValueInAggregate() {
		return BigMoney.of(CurrencyUnit.of(aggregateCurrency), valueInAggregate);
	}

	public void setValueInAggregate(BigMoney valueInAggregate) {
		this.valuePerOccurrence = valueInAggregate.getAmount();
		this.occurrenceCurrency = valueInAggregate.getCurrencyUnit().getCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(description)
				.append(valuePerOccurrence)
				.append(occurrenceCurrency)
				.append(valueInAggregate)
				.append(aggregateCurrency)
				.build();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		LimitHibernate that = (LimitHibernate) o;
		return Objects.equals(description, that.description);
	}

	@Override
	public int hashCode() {
		return Objects.hash(description);
	}
}
