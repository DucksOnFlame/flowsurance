package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.persistence.hibernate.CompanyHibernate;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class StakesHibernate {

	@ManyToOne
	@JoinColumn(nullable = false)
	private CompanyHibernate company;

	@Column
	private Float value;

	public StakesHibernate() {
	}

	public StakesHibernate(CompanyHibernate company, Float value) {
		this.company = company;
		this.value = value;
	}

	public CompanyHibernate getCompany() {
		return company;
	}

	public Float getValue() {
		return value;
	}
}
