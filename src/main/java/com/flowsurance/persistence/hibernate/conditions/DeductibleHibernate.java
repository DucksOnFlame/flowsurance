package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.Deductible;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DeductibleHibernate {

	@Column(nullable = false)
	private String description;

	public DeductibleHibernate() {
	}

	public DeductibleHibernate(Deductible deductible) {
		this.description = deductible.getDescription();
	}

	public DeductibleHibernate(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
