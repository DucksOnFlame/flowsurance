package com.flowsurance.persistence.hibernate.conditions.offer;

import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.google.common.base.Optional;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OfferedClauseHibernate {

	@Column(nullable = false)
	private String offerId;

	@Column
	private String originalClause;

	@Column(nullable = false)
	private OfferedConditionType type;

	@Column
	private String changedClause;

	public OfferedClauseHibernate() {
	}

	public OfferedClauseHibernate(OfferId offerId,
			Optional<ClauseId> originalClause,
			OfferedConditionType type,
			Optional<ClauseId> changedClause) {
		this.offerId = offerId.get();
		this.originalClause = originalClause.transform(ClauseId::get).orNull();
		this.type = type;
		this.changedClause = changedClause.transform(ClauseId::get).orNull();
	}

	public OfferId getOfferId() {
		return new OfferId(offerId);
	}

	public void setOfferId(OfferId offerId) {
		this.offerId = offerId.get();
	}

	public Optional<ClauseId> getOriginalClause() {
		return Optional.fromNullable(originalClause).transform(ClauseId::new);
	}

	public void setOriginalClause(Optional<ClauseId> originalClause) {
		this.originalClause = originalClause.transform(ClauseId::get).orNull();
	}

	public OfferedConditionType getType() {
		return type;
	}

	public void setType(OfferedConditionType type) {
		this.type = type;
	}

	public Optional<ClauseId> getChangedClause() {
		return Optional.fromNullable(changedClause).transform(ClauseId::new);
	}

	public void setChangedClause(Optional<ClauseId> changedClause) {
		this.changedClause = changedClause.transform(ClauseId::get).orNull();
	}
}
