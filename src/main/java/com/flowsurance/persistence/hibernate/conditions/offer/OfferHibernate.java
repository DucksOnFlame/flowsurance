package com.flowsurance.persistence.hibernate.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferState;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;
import com.google.common.collect.Lists;
import com.neovisionaries.i18n.CurrencyCode;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;

@Entity
@Table(name = "offer")
public class OfferHibernate extends StringIdModelHibernate {

	@ManyToOne
	@JoinColumn
	private PetitionHibernate petition;

	@Column
	private OfferState state;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<OfferedClauseHibernate> offeredClauses = Lists.newArrayList();

	@ElementCollection(fetch = FetchType.EAGER)
	private List<OfferedExclusionHibernate> offeredExclusions = Lists.newArrayList();

	@ElementCollection(fetch = FetchType.EAGER)
	private List<OfferedDeductibleHibernate> offeredDeductibles = Lists.newArrayList();

	@Column
	private double amount;

	@Column
	private String currencyCode;

	@ManyToOne
	@JoinColumn
	private UserHibernate insurer;

	public OfferHibernate() {
	}

	public OfferHibernate(PetitionHibernate petition,
			OfferState state,
			UserHibernate insurer) {
		this.petition = petition;
		this.state = state;
		this.amount = 0;
		this.currencyCode = CurrencyCode.PLN.name();
		this.insurer = insurer;
	}

	public OfferId getId() {
		return new OfferId(id);
	}

	public PetitionHibernate getPetition() {
		return petition;
	}

	public void setPetition(PetitionHibernate petition) {
		this.petition = petition;
	}

	public OfferState getState() {
		return state;
	}

	public void setState(OfferState state) {
		this.state = state;
	}

	public List<OfferedClauseHibernate> getOfferedClauses() {
		return Lists.newArrayList(offeredClauses);
	}

	public void setOfferedClauses(List<OfferedClauseHibernate> offeredClauses) {
		this.offeredClauses = offeredClauses;
	}

	public List<OfferedExclusionHibernate> getOfferedExclusions() {
		return Lists.newArrayList(offeredExclusions);
	}

	public void setOfferedExclusions(List<OfferedExclusionHibernate> offeredExclusions) {
		this.offeredExclusions = offeredExclusions;
	}

	public List<OfferedDeductibleHibernate> getOfferedDeductibles() {
		return Lists.newArrayList(offeredDeductibles);
	}

	public void setOfferedDeductibles(List<OfferedDeductibleHibernate> offeredDeductibles) {
		this.offeredDeductibles = offeredDeductibles;
	}

	public BigMoney getPrice() {
		return BigMoney.of(CurrencyUnit.of(currencyCode), amount);
	}

	public void setPrice(BigMoney price) {
		this.amount = price.getAmount().doubleValue();
		this.currencyCode = price.getCurrencyUnit().getCode();
	}

	public UserHibernate getInsurer() {
		return insurer;
	}

	public void setInsurer(UserHibernate insurer) {
		this.insurer = insurer;
	}
}
