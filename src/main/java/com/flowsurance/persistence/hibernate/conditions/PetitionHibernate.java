package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.petition.PetitionName;
import com.flowsurance.domain.model.conditions.petition.PetitionState;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import com.flowsurance.persistence.hibernate.UserHibernate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@Table(name = "petition")
public class PetitionHibernate extends AbstractConditionsHibernate {

	@Column
	@Enumerated(EnumType.STRING)
	private PetitionState state;

	public PetitionHibernate() {
	}

	public PetitionHibernate(PetitionName name,
			InsuranceConditionsType type,
			PetitionState state,
			DateTime coverStartDate,
			DateTime coverEndDate,
			CompanyHibernate policyHolder,
			List<CompanyHibernate> insureds,
			List<StakesHibernate> brokerStakes,
			UserHibernate broker) {
		super(name, type, coverStartDate, coverEndDate, policyHolder, insureds, brokerStakes, broker);
		this.state = state;
	}

	@Override
	public PetitionName getName() {
		return new PetitionName(name);
	}

	public PetitionState getState() {
		return state;
	}

	public void setState(PetitionState state) {
		this.state = state;
	}
}
