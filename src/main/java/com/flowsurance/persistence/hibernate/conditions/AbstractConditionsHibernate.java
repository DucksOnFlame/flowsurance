package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.ConditionsName;
import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.joda.time.DateTime;

@MappedSuperclass
public abstract class AbstractConditionsHibernate extends StringIdModelHibernate {

	@Column
	protected String name;

	@Column
	@Enumerated(EnumType.STRING)
	private InsuranceConditionsType type;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<SubjectHibernate> subjects = Lists.newArrayList();

	@Column(name = "cover_start_date")
	private Date coverStartDate;

	@Column(name = "cover_end_date")
	private Date coverEndDate;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "policy_holder")
	private CompanyHibernate policyHolder;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(nullable = false)
	private List<CompanyHibernate> insureds;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<StakesHibernate> brokerStakes;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn
	private List<ClauseHibernate> clauses = Lists.newArrayList();

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn
	private List<ExclusionHibernate> exclusions = Lists.newArrayList();

	@ElementCollection(fetch = FetchType.EAGER)
	private List<DeductibleHibernate> deductibles = Lists.newArrayList();

	@ManyToOne
	@JoinColumn
	private UserHibernate broker;

	public AbstractConditionsHibernate() {
	}

	public AbstractConditionsHibernate(ConditionsName name,
			InsuranceConditionsType type,
			DateTime coverStartDate,
			DateTime coverEndDate,
			CompanyHibernate policyHolder,
			List<CompanyHibernate> insureds,
			List<StakesHibernate> brokerStakes,
			UserHibernate broker) {
		this.name = name.get();
		this.type = type;
		this.coverStartDate = coverStartDate.toDate();
		this.coverEndDate = coverEndDate.toDate();
		this.policyHolder = policyHolder;
		this.insureds = insureds;
		this.brokerStakes = brokerStakes;
		this.broker = broker;
	}

	public PetitionId getId() {
		return new PetitionId(id);
	}

	public abstract ConditionsName getName();

	public void setName(ConditionsName name) {
		this.name = name.get();
	}

	public InsuranceConditionsType getType() {
		return type;
	}

	public void setType(InsuranceConditionsType type) {
		this.type = type;
	}

	public List<SubjectHibernate> getSubjects() {
		return Lists.newArrayList(subjects);
	}

	public void setSubjects(List<SubjectHibernate> subjects) {
		this.subjects = Preconditions.checkNotNull(subjects);
	}

	public Date getCoverStartDate() {
		return coverStartDate;
	}

	public void setCoverStartDate(Date coverStartDate) {
		this.coverStartDate = coverStartDate;
	}

	public Date getCoverEndDate() {
		return coverEndDate;
	}

	public void setCoverEndDate(Date coverEndDate) {
		this.coverEndDate = coverEndDate;
	}

	public CompanyHibernate getPolicyHolder() {
		return policyHolder;
	}

	public void setPolicyHolder(CompanyHibernate policyHolder) {
		this.policyHolder = policyHolder;
	}

	public List<CompanyHibernate> getInsureds() {
		return Lists.newArrayList(insureds);
	}

	public void setInsureds(List<CompanyHibernate> insureds) {
		this.insureds = Preconditions.checkNotNull(insureds);
	}

	public List<StakesHibernate> getBrokerStakes() {
		return Lists.newArrayList(brokerStakes);
	}

	public void setBrokerStakes(List<StakesHibernate> brokerStakes) {
		this.brokerStakes = Preconditions.checkNotNull(brokerStakes);
	}

	public List<ClauseHibernate> getClauses() {
		return Lists.newArrayList(clauses);
	}

	public void setClauses(List<ClauseHibernate> clauses) {
		this.clauses = Preconditions.checkNotNull(clauses);
	}

	public List<ExclusionHibernate> getExclusions() {
		return Lists.newArrayList(exclusions);
	}

	public void setExclusions(List<ExclusionHibernate> exclusions) {
		this.exclusions = Preconditions.checkNotNull(exclusions);
	}

	public List<DeductibleHibernate> getDeductibles() {
		return Lists.newArrayList(deductibles);
	}

	public void setDeductibles(List<DeductibleHibernate> deductibles) {
		this.deductibles = Preconditions.checkNotNull(deductibles);
	}

	public UserHibernate getBroker() {
		return broker;
	}

	public void setBroker(UserHibernate broker) {
		this.broker = broker;
	}
}