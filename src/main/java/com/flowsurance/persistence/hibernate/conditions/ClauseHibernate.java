package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.clause.ClauseTitle;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "clause")
public class ClauseHibernate extends StringIdModelHibernate {

	@Column(nullable = false)
	private String title;

	@Column(nullable = false)
	private String content;

	@ElementCollection(fetch = FetchType.EAGER)
	private List<LimitHibernate> limits = new ArrayList<>();

	@ElementCollection(fetch = FetchType.EAGER)
	private List<DeductibleHibernate> deductibles = new ArrayList<>();

	public ClauseHibernate() {
	}

	public ClauseHibernate(ClauseTitle title,
			String content,
			List<LimitHibernate> limits,
			List<DeductibleHibernate> deductibles) {
		this.title = title.get();
		this.content = content;
		this.limits = limits;
		this.deductibles = deductibles;
	}

	public ClauseId getId() {
		return new ClauseId(id);
	}

	public ClauseTitle getTitle() {
		return new ClauseTitle(title);
	}

	public void setTitle(ClauseTitle title) {
		this.title = title.get();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<LimitHibernate> getLimits() {
		return Lists.newArrayList(limits);
	}

	public void setLimits(List<LimitHibernate> limits) {
		this.limits = limits;
	}

	public List<DeductibleHibernate> getDeductibles() {
		return Lists.newArrayList(deductibles);
	}

	public void setDeductibles(List<DeductibleHibernate> deductibles) {
		this.deductibles = deductibles;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(id)
				.append(title)
				.append(content)
				.append(limits)
				.build();
	}
}
