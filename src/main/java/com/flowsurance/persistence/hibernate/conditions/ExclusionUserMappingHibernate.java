package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "exclusion_user_mapping")
public class ExclusionUserMappingHibernate extends StringIdModelHibernate {

	private String userId;

	private String exclusionId;

	public ExclusionUserMappingHibernate() {
	}

	public ExclusionUserMappingHibernate(UserId userId, ExclusionId exclusionId) {
		this.userId = userId.get();
		this.exclusionId = exclusionId.get();
	}

	public UserId getUserId() {
		return new UserId(userId);
	}

	public ExclusionId getExclusionId() {
		return new ExclusionId(exclusionId);
	}
}
