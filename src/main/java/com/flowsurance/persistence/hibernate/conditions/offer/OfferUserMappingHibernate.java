package com.flowsurance.persistence.hibernate.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "offer_user_mapping")
public class OfferUserMappingHibernate extends StringIdModelHibernate {

	@Column(name = "user_id")
	private String userId;

	@Column(name = "offer_id")
	private String offerId;

	public OfferUserMappingHibernate() {
	}

	public OfferUserMappingHibernate(UserId userId, OfferId offerId) {
		this.userId = userId.get();
		this.offerId = offerId.get();
	}

	public UserId getUserId() {
		return new UserId(userId);
	}

	public OfferId getOfferId() {
		return new OfferId(offerId);
	}
}
