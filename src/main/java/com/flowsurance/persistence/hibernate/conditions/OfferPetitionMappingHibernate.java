package com.flowsurance.persistence.hibernate.conditions;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "offer_petition_mapping")
public class OfferPetitionMappingHibernate extends StringIdModelHibernate {

	@Column(name = "offer_id")
	private String offerId;

	@Column(name = "petition_id")
	private String petitionId;

	public OfferPetitionMappingHibernate() {
	}

	public OfferPetitionMappingHibernate(PetitionId petitionId, OfferId offerId) {
		this.petitionId = petitionId.get();
		this.offerId = offerId.get();
	}

	public OfferId getOfferId() {
		return new OfferId(offerId);
	}

	public PetitionId getPetitionId() {
		return new PetitionId(petitionId);
	}
}
