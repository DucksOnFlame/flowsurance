package com.flowsurance.persistence.hibernate.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ComparisonId;
import com.flowsurance.persistence.hibernate.StringIdModelHibernate;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferHibernate;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comparison")
public class ComparisonHibernate extends StringIdModelHibernate {

	@OneToOne
	private PetitionHibernate petition;

	@OneToMany(fetch = FetchType.EAGER)
	private List<OfferHibernate> offers = Lists.newArrayList();

	public ComparisonHibernate(PetitionHibernate petition, List<OfferHibernate> offers) {
		this.petition = petition;
		this.offers = offers;
	}

	public ComparisonHibernate() {
	}

	public ComparisonId getId() {
		return new ComparisonId(id);
	}

	public PetitionHibernate getPetition() {
		return petition;
	}

	public List<OfferHibernate> getOffers() {
		return Lists.newArrayList(offers);
	}

	public void setOffers(List<OfferHibernate> offers) {
		this.offers = Preconditions.checkNotNull(offers);
	}
}
