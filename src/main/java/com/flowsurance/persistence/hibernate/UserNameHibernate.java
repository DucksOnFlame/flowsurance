package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.user.UserName;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserNameHibernate implements Serializable {

	@Column
	private String name;

	@Column
	private String surname;

	public UserNameHibernate() {
	}

	public UserNameHibernate(UserName userName) {
		this.name = userName.getName();
		this.surname = userName.getSurname();
	}

	public UserName getUserName() {
		return new UserName(name, surname);
	}
}
