package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.suite.InsuranceSuiteId;
import com.flowsurance.domain.model.suite.InsuranceSuiteType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "insurance_suite")
public class InsuranceSuiteHibernate extends StringIdModelHibernate {

	@Column
	private String name;

	@Column
	@Enumerated(EnumType.STRING)
	private InsuranceSuiteType type;

	public InsuranceSuiteHibernate() {
	}

	public InsuranceSuiteHibernate(InsuranceSuiteId id, String name, InsuranceSuiteType type) {
		this.id = id.get();
		this.name = name;
		this.type = type;
	}

	public InsuranceSuiteId getId() {
		return new InsuranceSuiteId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InsuranceSuiteType getType() {
		return type;
	}

	public void setType(InsuranceSuiteType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(id)
				.append(name)
				.append(type)
				.build();
	}
}
