package com.flowsurance.persistence.hibernate;

import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.entity.CompanyName;
import com.flowsurance.domain.model.entity.CompanyType;
import com.flowsurance.domain.model.entity.TaxPayerId;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "entity")
public class CompanyHibernate extends StringIdModelHibernate {

	@Column(nullable = false, unique = true)
	private String name;

	@OneToOne
	@JoinColumn(nullable = false)
	private AddressHibernate address;

	@Column
	@Enumerated(EnumType.STRING)
	private CompanyType type;

	@Column(nullable = false)
	private String taxPayerId;

	public CompanyHibernate() {
	}

	public CompanyHibernate(Company company) {
		this.name = company.getName().get();
		this.address = new AddressHibernate(company.getAddress());
		this.type = company.getType();
		this.taxPayerId = company.getTaxPayerId().get();
	}

	public CompanyHibernate(CompanyName name, AddressHibernate address, CompanyType type, TaxPayerId taxPayerId) {
		this.name = name.get();
		this.address = address;
		this.type = type;
		this.taxPayerId = taxPayerId.get();
	}

	public CompanyId getId() {
		return new CompanyId(id);
	}

	public CompanyName getName() {
		return new CompanyName(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public AddressHibernate getAddress() {
		return address;
	}

	public CompanyType getType() {
		return type;
	}

	public TaxPayerId getTaxPayerId() {
		return new TaxPayerId(taxPayerId);
	}
}