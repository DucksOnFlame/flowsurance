package com.flowsurance.persistence.dao;

import com.flowsurance.domain.model.user.CreateUserRequest;
import com.flowsurance.domain.model.user.UserDisplayName;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.flowsurance.persistence.hibernate.UserNameHibernate;
import com.google.common.base.Optional;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDao extends GenericHibernateDao<UserHibernate, UserId> {

	private final CompanyDao companyDao;

	@Autowired
	public UserDao(HibernateCommandFactory hibernateCommandFactory, CompanyDao companyDao) {
		super(hibernateCommandFactory);
		this.companyDao = companyDao;
	}

	@Override
	protected Class<UserHibernate> getEntityClass() {
		return UserHibernate.class;
	}

	public UserHibernate createUser(CreateUserRequest request) {
		UserHibernate hibernate = new UserHibernate(
				new UserNameHibernate(request.getUserName()),
				request.getDisplayName(),
				companyDao.get(request.getCompanyId()).orNull(),
				request.getEmail(),
				request.getJobTitle()
		);

		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}

	public Optional<UserHibernate> findByDisplayName(UserDisplayName userDisplayName) {
		return performDbOperation(
				new HibernateCommand<Optional<UserHibernate>>() {
					@Override
					public Optional<UserHibernate> perform() {
						Query query = getEntityManager().createQuery("from UserHibernate where displayName = :displayName")
								.setParameter("displayName", userDisplayName.get());
						try {
							return Optional.of((UserHibernate) query.getSingleResult());
						} catch (NoResultException e) {
							return Optional.absent();
						}
					}
				}
		);
	}
}
