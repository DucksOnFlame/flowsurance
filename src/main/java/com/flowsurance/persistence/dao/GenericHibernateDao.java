package com.flowsurance.persistence.dao;

import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.util.IdValueObject;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.hql.ParsingException;

public abstract class GenericHibernateDao<HIBERNATE, ID extends IdValueObject> {

	private final HibernateCommandFactory hibernateCommandFactory;

	public GenericHibernateDao(HibernateCommandFactory hibernateCommandFactory) {
		this.hibernateCommandFactory = hibernateCommandFactory;
	}

	protected <T> T performDbOperation(HibernateCommand<T> command) {
		return hibernateCommandFactory.initializeHibernateCommand(command).run();
	}

	protected abstract Class<HIBERNATE> getEntityClass();

	public Optional<HIBERNATE> get(final ID id) {
		return Optional.fromNullable(performDbOperation(new HibernateCommand<HIBERNATE>() {
			@Override
			public HIBERNATE perform() {
				return getEntityManager().find(getEntityClass(), id.get());
			}
		}));
	}

	public List<HIBERNATE> getAll() {
		return performDbOperation(new HibernateCommand<List<HIBERNATE>>() {
			@Override
			public List<HIBERNATE> perform() {
				return getEntityManager()
						.createQuery("from " + getEntityClass().getSimpleName() + " i")
						.getResultList();
			}
		});
	}

	public void delete(ID id) {
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				HIBERNATE hibernate = getEntityManager().find(getEntityClass(), id.get());
				getEntityManager().remove(hibernate);
				return null;
			}
		});
	}

	public List<HIBERNATE> getByIds(final Collection<ID> ids) {
		String query = String.format("FROM " + getEntityClass().getSimpleName() + " WHERE id IN (%s)", joinIds(ids));
		return performDbOperation(new HibernateCommand<List<HIBERNATE>>() {
			@Override
			public List<HIBERNATE> perform() {
				try {
					return getEntityManager().createQuery(query)
							.getResultList();
				} catch (ParsingException e) {
					return Lists.newArrayList();
				}
			}
		});
	}

	public String joinIds(Collection<ID> ids) {
		return ids.stream()
				.map(IdValueObject::get)
				.map(id -> "'" + id + "'")
				.collect(Collectors.joining(", "));
	}
}
