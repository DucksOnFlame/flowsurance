package com.flowsurance.persistence.dao;

import com.flowsurance.domain.core.binding.CompanyUserMappingId;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.hibernate.CompanyUserMappingHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyUserMappingDao extends GenericHibernateDao<CompanyUserMappingHibernate, CompanyUserMappingId> {

	@Autowired
	public CompanyUserMappingDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<CompanyUserMappingHibernate> getEntityClass() {
		return CompanyUserMappingHibernate.class;
	}

	public List<CompanyUserMappingHibernate> getAllMappingsForUser(UserId userId) {
		return performDbOperation(new HibernateCommand<List<CompanyUserMappingHibernate>>() {
			@Override
			public List<CompanyUserMappingHibernate> perform() {
				return getEntityManager().createQuery("from CompanyUserMappingHibernate where userId = :userId")
						.setParameter("userId", userId.get())
						.getResultList();
			}
		});
	}

	public CompanyUserMappingHibernate createMapping(UserId userId, CompanyId companyId) {
		CompanyUserMappingHibernate hibernate = new CompanyUserMappingHibernate(userId, companyId);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}

	public void deleteAllCompanyMappings(CompanyId companyId) {
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				List<CompanyUserMappingHibernate> companyMappings = getEntityManager()
						.createQuery("from CompanyUserMappingHibernate where companyId = :companyId")
						.setParameter("companyId", companyId.get())
						.getResultList();
				for (CompanyUserMappingHibernate hibernate : companyMappings) {
					getEntityManager().remove(hibernate);
				}
				return null;
			}
		});
	}
}
