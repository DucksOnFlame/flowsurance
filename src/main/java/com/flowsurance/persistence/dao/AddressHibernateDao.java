package com.flowsurance.persistence.dao;

import com.flowsurance.domain.model.address.AddressId;
import com.flowsurance.domain.model.address.CreateAddressRequest;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.hibernate.AddressHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressHibernateDao extends GenericHibernateDao<AddressHibernate, AddressId> {

	@Autowired
	public AddressHibernateDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<AddressHibernate> getEntityClass() {
		return AddressHibernate.class;
	}

	@Override
	public List<AddressHibernate> getAll() {
		throw new UnsupportedOperationException();
	}

	public AddressHibernate createAddress(CreateAddressRequest request) {
		AddressHibernate hibernate = new AddressHibernate(
				request.getStreetAddress(),
				request.getZipCode(),
				request.getCity(),
				request.getState(),
				request.getCountry()
		);

		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
