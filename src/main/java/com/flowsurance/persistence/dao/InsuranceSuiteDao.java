package com.flowsurance.persistence.dao;

import com.flowsurance.domain.model.suite.CreateInsuranceSuiteRequest;
import com.flowsurance.domain.model.suite.InsuranceSuiteId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.hibernate.InsuranceSuiteHibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InsuranceSuiteDao extends GenericHibernateDao<InsuranceSuiteHibernate, InsuranceSuiteId> {

	@Autowired
	public InsuranceSuiteDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<InsuranceSuiteHibernate> getEntityClass() {
		return InsuranceSuiteHibernate.class;
	}

	public InsuranceSuiteHibernate createSuite(CreateInsuranceSuiteRequest request) {
		InsuranceSuiteHibernate hibernate = new InsuranceSuiteHibernate();
		hibernate.setName(request.getName());
		hibernate.setType(request.getType());
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
