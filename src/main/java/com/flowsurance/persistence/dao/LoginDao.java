package com.flowsurance.persistence.dao;

import com.flowsurance.domain.model.user.CreateLoginRequest;
import com.flowsurance.domain.model.user.IsAuthorizedRequest;
import com.flowsurance.domain.model.user.LoginId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.hibernate.LoginEntryHibernate;
import com.google.common.base.Optional;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class LoginDao extends GenericHibernateDao<LoginEntryHibernate, LoginId> {

	@Autowired
	public LoginDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<LoginEntryHibernate> getEntityClass() {
		return LoginEntryHibernate.class;
	}

	public LoginEntryHibernate createEntry(CreateLoginRequest request) {
		String encryptedPassword = BCrypt.hashpw(request.getPassword().get(), BCrypt.gensalt());
		LoginEntryHibernate hibernate = new LoginEntryHibernate(request.getUserId(), encryptedPassword);
		performDbOperation(new HibernateCommand<LoginEntryHibernate>() {
			@Override
			public LoginEntryHibernate perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}

	public boolean isAuthorized(IsAuthorizedRequest request) {
		LoginEntryHibernate loginEntry = performDbOperation(
				new HibernateCommand<Optional<LoginEntryHibernate>>() {
					@Override
					public Optional<LoginEntryHibernate> perform() {
						Query query = getEntityManager().createQuery("from LoginEntryHibernate where userId = :userId")
								.setParameter("userId", request.getUserId().get());
						try {
							return Optional.of((LoginEntryHibernate) query.getSingleResult());
						} catch (NoResultException e) {
							return Optional.absent();
						}
					}
				}
		).orNull();

		if (loginEntry == null) {
			return false;
		}

		return BCrypt.checkpw(request.getPassword().get(), loginEntry.getHashedPassword());
	}
}
