package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.core.binding.PetitionUserMappingId;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.PetitionUserMappingHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetitionUserMappingDao extends GenericHibernateDao<PetitionUserMappingHibernate, PetitionUserMappingId> {

	@Autowired
	public PetitionUserMappingDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<PetitionUserMappingHibernate> getEntityClass() {
		return PetitionUserMappingHibernate.class;
	}

	public List<PetitionUserMappingHibernate> getAllMappingsForUser(UserId userId) {
		return performDbOperation(new HibernateCommand<List<PetitionUserMappingHibernate>>() {
			@Override
			public List<PetitionUserMappingHibernate> perform() {
				return getEntityManager().createQuery("from PetitionUserMappingHibernate where userId = :userId")
						.setParameter("userId", userId.get())
						.getResultList();
			}
		});
	}

	public PetitionUserMappingHibernate createMapping(UserId userId, PetitionId conditionsId) {
		PetitionUserMappingHibernate hibernate = new PetitionUserMappingHibernate(userId, conditionsId);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}

	public void deleteAllConditionsMappings(PetitionId id) {
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				List<PetitionUserMappingHibernate> conditionsMappings = getEntityManager()
						.createQuery("from PetitionUserMappingHibernate where conditionsId = :conditionsId")
						.setParameter("conditionsId", id.get())
						.getResultList();
				for (PetitionUserMappingHibernate hibernate : conditionsMappings) {
					getEntityManager().remove(hibernate);
				}
				return null;
			}
		});
	}

	public void delete(PetitionId petitionId, UserId userId) {
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				PetitionUserMappingHibernate conditionsMapping = (PetitionUserMappingHibernate) getEntityManager()
						.createQuery("from PetitionUserMappingHibernate where conditionsId = :conditionsId and userId = :userId")
						.setParameter("conditionsId", petitionId.get())
						.setParameter("userId", userId.get())
						.getSingleResult();

				getEntityManager().remove(conditionsMapping);
				return null;
			}
		});
	}
}
