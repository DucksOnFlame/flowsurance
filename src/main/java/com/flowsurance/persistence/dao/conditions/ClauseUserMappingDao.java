package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.core.binding.ClauseUserMappingId;
import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.ClauseUserMappingHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClauseUserMappingDao extends GenericHibernateDao<ClauseUserMappingHibernate, ClauseUserMappingId> {

	@Autowired
	public ClauseUserMappingDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<ClauseUserMappingHibernate> getEntityClass() {
		return ClauseUserMappingHibernate.class;
	}

	public List<ClauseUserMappingHibernate> getAllMappingsForUser(UserId userId) {
		return performDbOperation(new HibernateCommand<List<ClauseUserMappingHibernate>>() {
			@Override
			public List<ClauseUserMappingHibernate> perform() {
				return getEntityManager().createQuery("from ClauseUserMappingHibernate where userId = :userId")
						.setParameter("userId", userId.get())
						.getResultList();
			}
		});
	}

	public ClauseUserMappingHibernate createMapping(UserId userId, ClauseId clauseId) {
		ClauseUserMappingHibernate hibernate = new ClauseUserMappingHibernate(userId, clauseId);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}

	public void deleteAllClauseMappings(ClauseId clauseId) {
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				List<ClauseUserMappingHibernate> clauseMappings = getEntityManager()
						.createQuery("from ClauseUserMappingHibernate where clauseId = :clauseId")
						.setParameter("clauseId", clauseId.get())
						.getResultList();

				for (ClauseUserMappingHibernate hibernate : clauseMappings) {
					getEntityManager().remove(hibernate);
				}
				return null;
			}
		});
	}
}
