package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.core.binding.OfferPetitionMappingId;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.OfferPetitionMappingHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferPetitionMappingDao extends GenericHibernateDao<OfferPetitionMappingHibernate, OfferPetitionMappingId> {

	@Autowired
	public OfferPetitionMappingDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<OfferPetitionMappingHibernate> getEntityClass() {
		return OfferPetitionMappingHibernate.class;
	}

	public List<OfferPetitionMappingHibernate> getAllMappingsForPetition(PetitionId petitionId) {
		return performDbOperation(new HibernateCommand<List<OfferPetitionMappingHibernate>>() {
			@Override
			public List<OfferPetitionMappingHibernate> perform() {
				return getEntityManager().createQuery("from OfferPetitionMappingHibernate where petitionId = :petitionId")
						.setParameter("petitionId", petitionId.get())
						.getResultList();
			}
		});
	}

	public OfferPetitionMappingHibernate createMapping(OfferId offerId, PetitionId petitionId) {
		OfferPetitionMappingHibernate hibernate = new OfferPetitionMappingHibernate(petitionId, offerId);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
