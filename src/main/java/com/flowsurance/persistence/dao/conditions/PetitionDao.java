package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.conditions.petition.PetitionState;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;
import com.flowsurance.persistence.request.CreatePetitionHibernateRequest;
import com.flowsurance.persistence.request.UpdatePetitionHibernateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetitionDao extends GenericHibernateDao<PetitionHibernate, PetitionId> {

	@Autowired
	public PetitionDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<PetitionHibernate> getEntityClass() {
		return PetitionHibernate.class;
	}

	public PetitionHibernate create(CreatePetitionHibernateRequest request) {
		return performDbOperation(new HibernateCommand<PetitionHibernate>() {
			@Override
			public PetitionHibernate perform() {
				PetitionHibernate hibernate = new PetitionHibernate(
						request.getName(),
						request.getType(),
						request.getState(),
						request.getCoverStart(),
						request.getCoverEnd(),
						request.getPolicyHolder(),
						request.getInsureds(),
						request.getBrokerStakes(),
						request.getBroker()
				);

				return getEntityManager().merge(hibernate);
			}
		});
	}

	public void update(UpdatePetitionHibernateRequest request) {
		PetitionHibernate hibernate = get(request.getId()).orNull();
		if (hibernate == null) {
			throw new IllegalArgumentException("Could not find Petition by id: " + request.getId());
		}

		hibernate.setName(request.getName());
		hibernate.setType(request.getType());
		hibernate.setState(request.getState());
		hibernate.setSubjects(request.getSubjects());
		hibernate.setCoverStartDate(request.getCoverStartDate().toDate());
		hibernate.setCoverEndDate(request.getCoverEndDate().toDate());
		hibernate.setPolicyHolder(request.getPolicyHolder());
		hibernate.setInsureds(request.getInsureds());
		hibernate.setClauses(request.getClauses());
		hibernate.setExclusions(request.getExclusions());
		hibernate.setDeductibles(request.getDeductibles());

		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().merge(hibernate);
				return null;
			}
		});
	}

	public void updateState(PetitionId id, PetitionState newState) {
		PetitionHibernate hibernate = get(id).orNull();
		if (hibernate == null) {
			throw new IllegalArgumentException("Could not find Petition by id: " + id);
		}

		hibernate.setState(newState);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().merge(hibernate);
				return null;
			}
		});
	}

	public void markInComparison(PetitionId petitionId) {
		PetitionHibernate hibernate = get(petitionId).orNull();
		if (hibernate == null) {
			throw new IllegalArgumentException("Could not find Petition by id: " + petitionId);
		}

		if (hibernate.getState().equals(PetitionState.SENT)) {
			hibernate.setState(PetitionState.COMPARISON);
		}

		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().merge(hibernate);
				return null;
			}
		});
	}
}
