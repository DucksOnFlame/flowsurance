package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.clause.CreateClauseRequest;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.ClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.LimitHibernate;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClauseDao extends GenericHibernateDao<ClauseHibernate, ClauseId> {

	@Autowired
	public ClauseDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<ClauseHibernate> getEntityClass() {
		return ClauseHibernate.class;
	}

	public ClauseHibernate createClause(CreateClauseRequest request) {
		List<LimitHibernate> limits = request.getLimits().stream()
				.map(LimitHibernate::new)
				.collect(Collectors.toList());

		List<DeductibleHibernate> deductibles = request.getDeductibles().stream()
				.map(DeductibleHibernate::new)
				.collect(Collectors.toList());

		ClauseHibernate hibernate = new ClauseHibernate(
				request.getTitle(),
				request.getContent(),
				limits,
				deductibles
		);

		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
