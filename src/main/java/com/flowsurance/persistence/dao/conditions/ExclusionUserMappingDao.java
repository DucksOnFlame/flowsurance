package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.core.binding.ExclusionUserMappingId;
import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.ExclusionUserMappingHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExclusionUserMappingDao extends GenericHibernateDao<ExclusionUserMappingHibernate, ExclusionUserMappingId> {

	@Autowired
	public ExclusionUserMappingDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<ExclusionUserMappingHibernate> getEntityClass() {
		return ExclusionUserMappingHibernate.class;
	}

	public List<ExclusionUserMappingHibernate> getAllMappingsForUser(UserId userId) {
		return performDbOperation(new HibernateCommand<List<ExclusionUserMappingHibernate>>() {
			@Override
			public List<ExclusionUserMappingHibernate> perform() {
				return getEntityManager().createQuery("from ExclusionUserMappingHibernate where userId = :userId")
						.setParameter("userId", userId.get())
						.getResultList();
			}
		});
	}

	public ExclusionUserMappingHibernate createMapping(UserId userId, ExclusionId exclusionId) {
		ExclusionUserMappingHibernate hibernate = new ExclusionUserMappingHibernate(userId, exclusionId);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}

	public void deleteAllExclusionMappings(ExclusionId exclusionId) {
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				List<ExclusionUserMappingHibernate> exclusionMappings = getEntityManager()
						.createQuery("from ExclusionUserMappingHibernate where exclusionId = :exclusionId")
						.setParameter("exclusionId", exclusionId.get())
						.getResultList();
				for (ExclusionUserMappingHibernate hibernate : exclusionMappings) {
					getEntityManager().remove(hibernate);
				}
				return null;
			}
		});
	}
}
