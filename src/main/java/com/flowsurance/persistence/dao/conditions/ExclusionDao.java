package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.model.conditions.CreateExclusionRequest;
import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.ExclusionHibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExclusionDao extends GenericHibernateDao<ExclusionHibernate, ExclusionId> {

	@Autowired
	public ExclusionDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<ExclusionHibernate> getEntityClass() {
		return ExclusionHibernate.class;
	}

	public ExclusionHibernate createClause(CreateExclusionRequest request) {
		ExclusionHibernate hibernate = new ExclusionHibernate(request.getContent());
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
