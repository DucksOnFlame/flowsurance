package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.core.binding.OfferUserMappingId;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferUserMappingHibernate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferUserMappingDao extends GenericHibernateDao<OfferUserMappingHibernate, OfferUserMappingId> {

	@Autowired
	public OfferUserMappingDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<OfferUserMappingHibernate> getEntityClass() {
		return OfferUserMappingHibernate.class;
	}

	public List<OfferUserMappingHibernate> getAllMappingsForUser(UserId userId) {
		return performDbOperation(new HibernateCommand<List<OfferUserMappingHibernate>>() {
			@Override
			public List<OfferUserMappingHibernate> perform() {
				return getEntityManager().createQuery("from OfferUserMappingHibernate where userId = :userId")
						.setParameter("userId", userId.get())
						.getResultList();
			}
		});
	}

	public OfferUserMappingHibernate createMapping(OfferId offerId, UserId userId) {
		OfferUserMappingHibernate hibernate = new OfferUserMappingHibernate(userId, offerId);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
