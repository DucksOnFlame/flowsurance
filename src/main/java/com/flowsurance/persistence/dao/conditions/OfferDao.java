package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferState;
import com.flowsurance.domain.model.conditions.offer.SendOfferRequest;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferHibernate;
import com.flowsurance.persistence.request.CreateOfferHibernateRequest;
import com.flowsurance.persistence.request.UpdateOfferHibernateRequest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferDao extends GenericHibernateDao<OfferHibernate, OfferId> {

	@Autowired
	public OfferDao(HibernateCommandFactory hibernateCommandFactory) {
		super(hibernateCommandFactory);
	}

	@Override
	protected Class<OfferHibernate> getEntityClass() {
		return OfferHibernate.class;
	}

	public List<OfferHibernate> getAllOffersForPetition(PetitionHibernate petitionHibernate) {
		return performDbOperation(new HibernateCommand<List<OfferHibernate>>() {
			@Override
			public List<OfferHibernate> perform() {
				return getEntityManager().createQuery("from OfferHibernate where petition = :petition")
						.setParameter("petition", petitionHibernate)
						.getResultList();
			}
		});
	}

	public OfferHibernate create(CreateOfferHibernateRequest request) {
		OfferHibernate hibernate = new OfferHibernate(
				request.getPetition(),
				OfferState.NEW,
				request.getInsurer()
		);

		return performDbOperation(new HibernateCommand<OfferHibernate>() {
			@Override
			public OfferHibernate perform() {
				return getEntityManager().merge(hibernate);
			}
		});
	}

	public OfferHibernate update(UpdateOfferHibernateRequest request) {
		OfferHibernate hibernate = get(request.getOfferId()).orNull();
		if (hibernate == null) {
			throw new IllegalArgumentException("Could not find Offer by id: " + request.getOfferId());
		}

		hibernate.setOfferedClauses(request.getOfferedClauses());
		hibernate.setOfferedExclusions(request.getOfferedExclusions());
		hibernate.setOfferedDeductibles(request.getOfferedDeductibles());
		hibernate.setPrice(request.getPrice());

		return performDbOperation(new HibernateCommand<OfferHibernate>() {
			@Override
			public OfferHibernate perform() {
				return getEntityManager().merge(hibernate);
			}
		});
	}

	public OfferHibernate markSent(SendOfferRequest request) {
		OfferHibernate hibernate = get(request.getOfferId()).orNull();
		if (hibernate == null) {
			throw new IllegalArgumentException("Could not find Offer by id: " + request.getOfferId());
		}

		hibernate.setState(OfferState.SENT);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().merge(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
