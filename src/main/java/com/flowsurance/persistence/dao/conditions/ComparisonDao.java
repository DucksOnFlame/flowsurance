package com.flowsurance.persistence.dao.conditions;

import com.flowsurance.domain.model.conditions.comparison.ComparisonId;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.dao.GenericHibernateDao;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;
import com.flowsurance.persistence.hibernate.conditions.comparison.ComparisonHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferHibernate;
import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComparisonDao extends GenericHibernateDao<ComparisonHibernate, ComparisonId> {

	private final PetitionDao petitionDao;

	private final OfferDao offerDao;

	@Autowired
	public ComparisonDao(HibernateCommandFactory hibernateCommandFactory, PetitionDao petitionDao, OfferDao offerDao) {
		super(hibernateCommandFactory);
		this.petitionDao = petitionDao;
		this.offerDao = offerDao;
	}

	@Override
	protected Class<ComparisonHibernate> getEntityClass() {
		return ComparisonHibernate.class;
	}

	public List<ComparisonHibernate> getAllComparisonsByPetition(List<PetitionId> petitionIds) {
		List<PetitionHibernate> petitions = petitionDao.getByIds(petitionIds);

		return performDbOperation(new HibernateCommand<List<ComparisonHibernate>>() {
			@Override
			public List<ComparisonHibernate> perform() {
				return getEntityManager().createQuery("from ComparisonHibernate where petition IN (:petitions)")
						.setParameter("petitions", petitions)
						.getResultList();
			}
		});
	}

	public void updateOffers(ComparisonId comparisonId) {
		ComparisonHibernate comparison = get(comparisonId).orNull();
		if (comparison == null) {
			throw new IllegalArgumentException("Could not find Comparison by id: " + comparisonId);
		}

		List<OfferHibernate> availableOffers = offerDao.getAllOffersForPetition(comparison.getPetition());

		Set<OfferHibernate> offersInDB = Sets.newHashSet(availableOffers);
		Set<OfferHibernate> offersInComparison = Sets.newHashSet(comparison.getOffers());

		if (offersInDB.equals(offersInComparison)) {
			return;
		}

		comparison.setOffers(availableOffers);
		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().merge(comparison);
				return null;
			}
		});
	}

	public ComparisonHibernate createForPetition(PetitionId petitionId) {
		PetitionHibernate petitionHibernate = petitionDao.get(petitionId).orNull();
		if (petitionHibernate == null) {
			throw new IllegalArgumentException("Could not find Petition by id: " + petitionId);
		}

		List<OfferHibernate> offers = offerDao.getAllOffersForPetition(petitionHibernate);
		ComparisonHibernate comparisonHibernate = new ComparisonHibernate(petitionHibernate, offers);

		performDbOperation(new HibernateCommand<List<Void>>() {
			@Override
			public List<Void> perform() {
				getEntityManager().persist(comparisonHibernate);
				return null;
			}
		});

		return comparisonHibernate;
	}
}
