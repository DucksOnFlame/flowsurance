package com.flowsurance.persistence.dao;

import com.flowsurance.domain.model.address.AddressId;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.entity.CreateCompanyRequest;
import com.flowsurance.persistence.HibernateCommand;
import com.flowsurance.persistence.HibernateCommandFactory;
import com.flowsurance.persistence.hibernate.AddressHibernate;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyDao extends GenericHibernateDao<CompanyHibernate, CompanyId> {

	private final AddressHibernateDao addressDao;

	@Autowired
	public CompanyDao(HibernateCommandFactory hibernateCommandFactory, AddressHibernateDao addressDao) {
		super(hibernateCommandFactory);
		this.addressDao = addressDao;
	}

	@Override
	protected Class<CompanyHibernate> getEntityClass() {
		return CompanyHibernate.class;
	}

	public CompanyHibernate createCompany(CreateCompanyRequest request, AddressId addressId) {
		AddressHibernate addressHibernate = addressDao.get(addressId).orNull();
		if (addressHibernate == null) {
			throw new RuntimeException("Could not find Address by id: " + addressId);
		}

		CompanyHibernate hibernate = new CompanyHibernate(
				request.getName(),
				addressHibernate,
				request.getType(),
				request.getTaxPayerId()
		);

		performDbOperation(new HibernateCommand<Void>() {
			@Override
			public Void perform() {
				getEntityManager().persist(hibernate);
				return null;
			}
		});

		return hibernate;
	}
}
