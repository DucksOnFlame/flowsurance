package com.flowsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlowsuranceApp {

	public static void main(String[] args) {
		SpringApplication.run(FlowsuranceApp.class, args);
	}
}
