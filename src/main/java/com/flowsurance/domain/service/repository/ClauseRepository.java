package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.clause.CreateClauseRequest;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.ClauseBuilder;
import com.flowsurance.persistence.dao.conditions.ClauseDao;
import com.flowsurance.persistence.dao.conditions.ClauseUserMappingDao;
import com.flowsurance.persistence.hibernate.conditions.ClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.ClauseUserMappingHibernate;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;

@Repository
public class ClauseRepository {

	private final ClauseDao clauseDao;

	private final ClauseUserMappingDao clauseUserMappingDao;

	private final ClauseBuilder clauseBuilder;

	private final UserContextService userContextService;

	public ClauseRepository(ClauseDao clauseDao,
			ClauseUserMappingDao clauseUserMappingDao,
			ClauseBuilder clauseBuilder,
			UserContextService userContextService) {
		this.clauseDao = clauseDao;
		this.clauseUserMappingDao = clauseUserMappingDao;
		this.clauseBuilder = clauseBuilder;
		this.userContextService = userContextService;
	}

	public List<Clause> getAllClausesForCurrentUser() {
		UserId currentUserId = userContextService.get().getId();
		return getAllClausesForUser(currentUserId);
	}

	public List<Clause> getAllClausesForUser(UserId userId) {
		List<ClauseId> clauseIds = clauseUserMappingDao.getAllMappingsForUser(userId).stream()
				.map(ClauseUserMappingHibernate::getClauseId)
				.collect(Collectors.toList());

		return clauseDao.getByIds(clauseIds).stream()
				.map(clauseBuilder::buildClause)
				.collect(Collectors.toList());
	}

	public Optional<Clause> getClause(ClauseId id) {
		ClauseHibernate hibernate = clauseDao.get(id).orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(clauseBuilder.buildClause(hibernate));
	}

	public Clause createClause(CreateClauseRequest request) {
		Clause clause = clauseBuilder.buildClause(clauseDao.createClause(request));
		clauseUserMappingDao.createMapping(userContextService.get().getId(), clause.getId());
		return clause;
	}

	public void deleteClause(ClauseId id) {
		clauseDao.delete(id);
		clauseUserMappingDao.deleteAllClauseMappings(id);
	}
}
