package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.suite.CreateInsuranceSuiteRequest;
import com.flowsurance.domain.model.suite.InsuranceSuite;
import com.flowsurance.domain.model.suite.InsuranceSuiteId;
import com.flowsurance.domain.service.builder.InsuranceSuiteBuilder;
import com.flowsurance.persistence.dao.InsuranceSuiteDao;
import com.flowsurance.persistence.hibernate.InsuranceSuiteHibernate;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InsuranceSuiteRepository {

	private final InsuranceSuiteDao insuranceSuiteDao;

	private final InsuranceSuiteBuilder insuranceSuiteBuilder;

	@Autowired
	public InsuranceSuiteRepository(InsuranceSuiteDao insuranceSuiteDao, InsuranceSuiteBuilder insuranceSuiteBuilder) {
		this.insuranceSuiteDao = insuranceSuiteDao;
		this.insuranceSuiteBuilder = insuranceSuiteBuilder;
	}

	public Optional<InsuranceSuite> getSuite(InsuranceSuiteId id) {
		InsuranceSuiteHibernate hibernate = insuranceSuiteDao.get(id).orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(insuranceSuiteBuilder.buildInsuranceSuite(hibernate));
	}

	public List<InsuranceSuite> getAllSuites() {
		return insuranceSuiteDao.getAll().parallelStream()
				.map(insuranceSuiteBuilder::buildInsuranceSuite)
				.collect(Collectors.toList());
	}

	public InsuranceSuite createSuite(CreateInsuranceSuiteRequest request) {
		return insuranceSuiteBuilder.buildInsuranceSuite(insuranceSuiteDao.createSuite(request));
	}

	public void deleteSuite(InsuranceSuiteId id) {
		insuranceSuiteDao.delete(id);
	}
}
