package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.CompanyType;
import com.flowsurance.domain.model.user.CreateUserRequest;
import com.flowsurance.domain.model.user.IsAuthorizedRequest;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.model.user.UserDisplayName;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.model.user.UserPassword;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.UserBuilder;
import com.flowsurance.persistence.dao.UserDao;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.flowsurance.rest.dto.user.UserConfigDTO;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

	private final UserDao userDao;

	private final UserBuilder userBuilder;

	private final LoginRepository loginRepository;

	private final UserContextService userContextService;

	@Autowired
	public UserRepository(UserDao userDao,
			UserBuilder userBuilder,
			LoginRepository loginRepository,
			UserContextService userContextService) {
		this.userDao = userDao;
		this.userBuilder = userBuilder;
		this.loginRepository = loginRepository;
		this.userContextService = userContextService;
	}

	public Optional<User> getUser(UserId id) {
		if (id == null) {
			return Optional.absent();
		}

		UserHibernate hibernate = userDao.get(id).orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(userBuilder.buildUser(hibernate));
	}

	public UserConfigDTO getCurrentUserConfig() {
		User user = userContextService.get();
		Company company = user.getCompany();

		return new UserConfigDTO(
				company.getType().equals(CompanyType.BROKER),
				company.getType().equals(CompanyType.INSURER),
				false
		);
	}

	public boolean authorize(UserDisplayName displayName, UserPassword userPassword) {
		User user = userBuilder.buildUser(userDao.findByDisplayName(displayName)).orNull();
		if (user == null) {
			return false;
		}

		boolean isAuthorized = loginRepository.isAuthorized(new IsAuthorizedRequest(user.getId(), userPassword));
		if (isAuthorized) {
			userContextService.set(user);
		}

		return isAuthorized;
	}

	public List<User> getAllUsers() {
		return userDao.getAll().parallelStream()
				.map(userBuilder::buildUser)
				.collect(Collectors.toList());
	}

	public List<User> getAllInsurers() {
		return getAllUsers().stream()
				.filter(user -> user.getCompany().getType().equals(CompanyType.INSURER))
				.collect(Collectors.toList());
	}

	public User createUser(CreateUserRequest request) {
		return userBuilder.buildUser(userDao.createUser(request));
	}

	public void deleteUser(UserId id) {
		userDao.delete(id);
	}
}
