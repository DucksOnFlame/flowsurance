package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.conditions.BrokerStakes;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.petition.CreatePetitionRequest;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.conditions.petition.PetitionState;
import com.flowsurance.domain.model.conditions.petition.SendPetitionRequest;
import com.flowsurance.domain.model.conditions.petition.UpdatePetitionRequest;
import com.flowsurance.domain.model.entity.BrokerCompany;
import com.flowsurance.domain.model.entity.CompanyType;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.PetitionBuilder;
import com.flowsurance.persistence.dao.conditions.PetitionDao;
import com.flowsurance.persistence.dao.conditions.PetitionUserMappingDao;
import com.flowsurance.persistence.hibernate.conditions.PetitionUserMappingHibernate;
import com.flowsurance.persistence.request.CreatePetitionHibernateRequest;
import com.flowsurance.persistence.request.PetitionHibernateRequestBuilder;
import com.flowsurance.persistence.request.UpdatePetitionHibernateRequest;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetitionRepository {

	private final PetitionDao petitionDao;

	private final PetitionUserMappingDao petitionUserMappingDao;

	private final ClauseRepository clauseRepository;

	private final ExclusionRepository exclusionRepository;

	private final PetitionBuilder petitionBuilder;

	private final UserContextService userContextService;

	private final PetitionHibernateRequestBuilder hibernateRequestBuilder;

	@Autowired
	public PetitionRepository(PetitionDao petitionDao,
			PetitionUserMappingDao petitionUserMappingDao,
			ClauseRepository clauseRepository,
			ExclusionRepository exclusionRepository,
			PetitionBuilder petitionBuilder,
			UserContextService userContextService,
			PetitionHibernateRequestBuilder hibernateRequestBuilder) {
		this.petitionDao = petitionDao;
		this.petitionUserMappingDao = petitionUserMappingDao;
		this.clauseRepository = clauseRepository;
		this.exclusionRepository = exclusionRepository;
		this.petitionBuilder = petitionBuilder;
		this.userContextService = userContextService;
		this.hibernateRequestBuilder = hibernateRequestBuilder;
	}

	public List<Petition> getPetitionsForCurrentUser() {
		return getUserMappedPetitions(userContextService.get().getId());
	}

	public List<Petition> getUserMappedPetitions(UserId userId) {
		List<PetitionId> conditionsIds = petitionUserMappingDao.getAllMappingsForUser(userId).stream()
				.map(PetitionUserMappingHibernate::getConditionsId)
				.collect(Collectors.toList());

		return petitionDao.getByIds(conditionsIds).stream()
				.map(petitionBuilder::buildPetition)
				.collect(Collectors.toList());
	}

	public Optional<Petition> getPetition(PetitionId id) {
		return petitionDao.get(id).transform(petitionBuilder::buildPetition);
	}

	public Petition createPetition(CreatePetitionRequest request) {
		User currentUser = userContextService.get();
		if (!currentUser.getCompany().getType().equals(CompanyType.BROKER)) {
			throw new IllegalStateException("User is not a broker!");
		}

		request.setBroker(currentUser);
		request.setBrokerStakes(BrokerStakes.singleStakes(BrokerCompany.valueOf(currentUser.getCompany())));
		CreatePetitionHibernateRequest hibernateRequest = hibernateRequestBuilder.buildCreateRequest(request);
		Petition petition = petitionBuilder.buildPetition(petitionDao.create(hibernateRequest));
		petitionUserMappingDao.createMapping(currentUser.getId(), petition.getId());
		return petition;
	}

	public void delete(PetitionId id) {
		petitionDao.delete(id);
		petitionUserMappingDao.deleteAllConditionsMappings(id);
	}

	public Petition update(UpdatePetitionRequest request) {
		UpdatePetitionHibernateRequest hibernateRequest = hibernateRequestBuilder.buildUpdateRequest(request);
		petitionDao.update(hibernateRequest);
		return getPetition(request.getId()).get();
	}

	public Petition send(SendPetitionRequest request) {
		PetitionId petitionId = request.getPetitionId();
		for (UserId insurerId : request.getInsurerIds()) {
			petitionUserMappingDao.createMapping(insurerId, petitionId);
		}

		petitionDao.updateState(petitionId, PetitionState.SENT);
		return getPetition(petitionId).get();
	}

	public List<Clause> getAllAvailableClauses(PetitionId id) {
		List<Clause> clauses = getPetition(id).transform(Petition::getClauses).orNull();
		if (clauses == null) {
			throw new IllegalArgumentException("Could not find Petition by id: " + id);
		}

		return clauseRepository.getAllClausesForCurrentUser().stream()
				.filter(clause -> !clauses.contains(clause))
				.collect(Collectors.toList());
	}

	public List<Exclusion> getAllAvailableExclusions(PetitionId id) {
		List<Exclusion> exclusions = getPetition(id).transform(Petition::getExclusions).orNull();
		if (exclusions == null) {
			throw new IllegalArgumentException("Could not find Petition by id: " + id);
		}

		return exclusionRepository.getAllExclusionsForCurrentUser().stream()
				.filter(exclusion -> !exclusions.contains(exclusion))
				.collect(Collectors.toList());
	}

	public void unbind(PetitionId petitionId, UserId userId) {
		petitionUserMappingDao.delete(petitionId, userId);
	}

	public void markInComparison(PetitionId petitionId) {
		petitionDao.markInComparison(petitionId);
	}
}
