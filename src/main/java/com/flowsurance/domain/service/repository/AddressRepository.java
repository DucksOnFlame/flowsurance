package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.address.Address;
import com.flowsurance.domain.model.address.AddressId;
import com.flowsurance.domain.model.address.CreateAddressRequest;
import com.flowsurance.domain.service.builder.AddressBuilder;
import com.flowsurance.persistence.dao.AddressHibernateDao;
import com.flowsurance.persistence.hibernate.AddressHibernate;
import com.google.common.base.Optional;
import org.springframework.stereotype.Repository;

@Repository
public class AddressRepository {

	private final AddressHibernateDao dao;

	public AddressRepository(AddressHibernateDao dao) {
		this.dao = dao;
	}

	public Optional<Address> getAddress(AddressId id) {
		AddressHibernate hibernate = dao.get(id).orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(AddressBuilder.buildAddress(hibernate));
	}

	public Address createAddress(CreateAddressRequest request) {
		return AddressBuilder.buildAddress(dao.createAddress(request));
	}

	public void deleteAddress(AddressId id) {
		dao.delete(id);
	}
}
