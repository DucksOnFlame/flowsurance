package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.offer.CreateOfferRequest;
import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferNotReadyException;
import com.flowsurance.domain.model.conditions.offer.OfferedClause;
import com.flowsurance.domain.model.conditions.offer.OfferedCondition;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.domain.model.conditions.offer.OfferedExclusion;
import com.flowsurance.domain.model.conditions.offer.SendOfferRequest;
import com.flowsurance.domain.model.conditions.offer.UpdateOfferRequest;
import com.flowsurance.domain.model.entity.CompanyType;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.OfferBuilder;
import com.flowsurance.persistence.dao.conditions.OfferDao;
import com.flowsurance.persistence.dao.conditions.OfferPetitionMappingDao;
import com.flowsurance.persistence.dao.conditions.OfferUserMappingDao;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferUserMappingHibernate;
import com.flowsurance.persistence.request.OfferHibernateRequestBuilder;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;

@Repository
public class OfferRepository {

	private final UserContextService userContextService;

	private final OfferDao offerDao;

	private final OfferHibernateRequestBuilder hibernateRequestBuilder;

	private final OfferBuilder offerBuilder;

	private final OfferUserMappingDao offerUserMappingDao;

	private final PetitionRepository petitionRepository;

	private final ClauseRepository clauseRepository;

	private final ExclusionRepository exclusionRepository;

	private final OfferPetitionMappingDao offerPetitionMappingDao;

	public OfferRepository(UserContextService userContextService,
			OfferDao offerDao,
			OfferHibernateRequestBuilder hibernateRequestBuilder,
			OfferBuilder offerBuilder,
			OfferUserMappingDao offerUserMappingDao,
			PetitionRepository petitionRepository,
			ClauseRepository clauseRepository,
			ExclusionRepository exclusionRepository,
			OfferPetitionMappingDao offerPetitionMappingDao) {
		this.userContextService = userContextService;
		this.offerDao = offerDao;
		this.hibernateRequestBuilder = hibernateRequestBuilder;
		this.offerBuilder = offerBuilder;
		this.offerUserMappingDao = offerUserMappingDao;
		this.petitionRepository = petitionRepository;
		this.clauseRepository = clauseRepository;
		this.exclusionRepository = exclusionRepository;
		this.offerPetitionMappingDao = offerPetitionMappingDao;
	}

	public List<Offer> getOffersForCurrentUser() {
		return getOffersForUser(userContextService.get().getId());
	}

	public List<Offer> getOffersForUser(UserId userId) {
		List<OfferId> offerIds = offerUserMappingDao.getAllMappingsForUser(userId).stream()
				.map(OfferUserMappingHibernate::getOfferId)
				.collect(Collectors.toList());

		return offerDao.getByIds(offerIds).stream()
				.map(offerBuilder::build)
				.collect(Collectors.toList());
	}

	public Optional<Offer> get(OfferId offerId) {
		return offerDao.get(offerId).transform(offerBuilder::build);
	}

	public Offer create(CreateOfferRequest request) {
		User user = userContextService.get();
		UserId userId = user.getId();
		if (!user.getCompany().getType().equals(CompanyType.INSURER)) {
			throw new IllegalStateException("User is not an Insurer!" + userId);
		}

		request.setInsurerUserId(userId);
		Offer offer = offerBuilder.build(offerDao.create(hibernateRequestBuilder.buildCreateRequest(request)));
		offer = createOfferedEntities(offer);

		offerUserMappingDao.createMapping(offer.getId(), userId);
		petitionRepository.unbind(request.getPetitionId(), userId);
		return offer;
	}

	private Offer createOfferedEntities(Offer offer) {
		return offerBuilder.build(offerDao.update(hibernateRequestBuilder.buildCreateOfferedEntitiesRequest(offer)));
	}

	public Offer update(UpdateOfferRequest request) {
		return offerBuilder.build(offerDao.update(hibernateRequestBuilder.buildUpdateRequest(request)));
	}

	public List<Clause> getAllAvailableClauses(OfferId offerId) {
		List<OfferedClause> offeredClauses = get(offerId).transform(Offer::getOfferedClauses).orNull();
		if (offeredClauses == null) {
			throw new IllegalArgumentException("Could not find Offer by id: " + offerId);
		}

		List<Clause> includedClauses = offeredClauses.stream()
				.filter(offeredClause -> offeredClause.getChangedClause().isPresent())
				.map(offeredClause -> offeredClause.getChangedClause().get())
				.collect(Collectors.toList());

		return clauseRepository.getAllClausesForCurrentUser().stream()
				.filter(clause -> !includedClauses.contains(clause))
				.collect(Collectors.toList());
	}

	public List<Exclusion> getAllAvailableExclusions(OfferId offerId) {
		List<OfferedExclusion> offeredExclusions = get(offerId).transform(Offer::getOfferedExclusions).orNull();
		if (offeredExclusions == null) {
			throw new IllegalArgumentException("Could not find Offer by id: " + offerId);
		}

		List<Exclusion> includedExclusions = offeredExclusions.stream()
				.filter(offeredExclusion -> offeredExclusion.getChangedExclusion().isPresent())
				.map(offeredExclusion -> offeredExclusion.getChangedExclusion().get())
				.collect(Collectors.toList());

		return exclusionRepository.getAllExclusionsForCurrentUser().stream()
				.filter(exclusion -> !includedExclusions.contains(exclusion))
				.collect(Collectors.toList());
	}

	public Offer send(SendOfferRequest request) throws OfferNotReadyException {
		OfferId offerId = request.getOfferId();
		Offer offer = get(offerId).orNull();
		if (offer == null) {
			throw new IllegalArgumentException("Could not find Offer by id: " + offerId);
		}

		for (OfferedCondition offeredCondition : offer.getAllOfferedConditions()) {
			if (offeredCondition.getType().equals(OfferedConditionType.TBD)) {
				throw new OfferNotReadyException();
			}
		}

		offerDao.markSent(request);
		petitionRepository.markInComparison(offer.getPetition().getId());
		offerPetitionMappingDao.createMapping(offerId, offer.getPetition().getId());
		return get(offerId).get();
	}
}
