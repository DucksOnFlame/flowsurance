package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.conditions.CreateExclusionRequest;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.ExclusionBuilder;
import com.flowsurance.persistence.dao.conditions.ExclusionDao;
import com.flowsurance.persistence.dao.conditions.ExclusionUserMappingDao;
import com.flowsurance.persistence.hibernate.conditions.ExclusionHibernate;
import com.flowsurance.persistence.hibernate.conditions.ExclusionUserMappingHibernate;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExclusionRepository {

	private final ExclusionDao exclusionDao;

	private final ExclusionBuilder exclusionBuilder;

	private final ExclusionUserMappingDao exclusionUserMappingDao;

	private final UserContextService userContextService;

	@Autowired
	public ExclusionRepository(ExclusionDao exclusionDao,
			ExclusionBuilder exclusionBuilder,
			ExclusionUserMappingDao exclusionUserMappingDao,
			UserContextService userContextService) {
		this.exclusionDao = exclusionDao;
		this.exclusionBuilder = exclusionBuilder;
		this.exclusionUserMappingDao = exclusionUserMappingDao;
		this.userContextService = userContextService;
	}

	public List<Exclusion> getAllExclusionsForCurrentUser() {
		return getAllExclusionsForUser(userContextService.get().getId());
	}

	public List<Exclusion> getAllExclusionsForUser(UserId userId) {
		List<ExclusionId> exclusionIds = exclusionUserMappingDao.getAllMappingsForUser(userId).stream()
				.map(ExclusionUserMappingHibernate::getExclusionId)
				.collect(Collectors.toList());

		return exclusionDao.getByIds(exclusionIds).stream()
				.map(exclusionBuilder::buildExclusion)
				.collect(Collectors.toList());
	}

	public Optional<Exclusion> getExclusion(ExclusionId id) {
		ExclusionHibernate hibernate = exclusionDao.get(id).orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(exclusionBuilder.buildExclusion(hibernate));
	}

	public Exclusion createExclusion(CreateExclusionRequest request) {
		Exclusion exclusion = exclusionBuilder.buildExclusion(exclusionDao.createClause(request));
		exclusionUserMappingDao.createMapping(userContextService.get().getId(), exclusion.getId());
		return exclusion;
	}

	public void deleteExclusion(ExclusionId id) {
		exclusionDao.delete(id);
		exclusionUserMappingDao.deleteAllExclusionMappings(id);
	}
}
