package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.address.Address;
import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.entity.CreateCompanyRequest;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.CompanyBuilder;
import com.flowsurance.persistence.dao.CompanyDao;
import com.flowsurance.persistence.dao.CompanyUserMappingDao;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import com.flowsurance.persistence.hibernate.CompanyUserMappingHibernate;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyRepository {

	private final CompanyDao dao;

	private final CompanyBuilder companyBuilder;

	private final AddressRepository addressRepository;

	private final UserContextService userContextService;

	private final CompanyUserMappingDao companyUserMappingDao;

	@Autowired
	public CompanyRepository(CompanyDao dao,
			CompanyBuilder companyBuilder,
			AddressRepository addressRepository,
			UserContextService userContextService,
			CompanyUserMappingDao companyUserMappingDao) {
		this.dao = dao;
		this.companyBuilder = companyBuilder;
		this.addressRepository = addressRepository;
		this.userContextService = userContextService;
		this.companyUserMappingDao = companyUserMappingDao;
	}

	public List<Company> getAllCompaniesForCurrentUser() {
		return getAllCompaniesForUser(userContextService.get().getId());
	}

	public List<Company> getAllCompaniesForUser(UserId userId) {
		List<CompanyId> companyIds = companyUserMappingDao.getAllMappingsForUser(userId).stream()
				.map(CompanyUserMappingHibernate::getCompanyId)
				.collect(Collectors.toList());

		return dao.getByIds(companyIds).stream()
				.map(companyBuilder::buildCompany)
				.collect(Collectors.toList());
	}

	public Optional<Company> getCompany(CompanyId id) {
		CompanyHibernate hibernate = dao.get(id).orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(companyBuilder.buildCompany(hibernate));
	}

	public Company createCompany(CreateCompanyRequest request) {
		Address address = addressRepository.createAddress(request.getCreateAddressRequest());
		Company company = companyBuilder.buildCompany(dao.createCompany(request, address.getId()));

		User currentUser = userContextService.get();
		companyUserMappingDao.createMapping(currentUser.getId(), company.getId());

		return company;
	}

	public void deleteCompany(CompanyId id) {
		Company company = getCompany(id).orNull();
		if (company == null) {
			return;
		}

		dao.delete(id);
		companyUserMappingDao.deleteAllCompanyMappings(id);
	}
}