package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.conditions.comparison.AnemicComparison;
import com.flowsurance.domain.model.conditions.comparison.Comparison;
import com.flowsurance.domain.model.conditions.comparison.ComparisonId;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.builder.ComparisonBuilder;
import com.flowsurance.persistence.dao.conditions.ComparisonDao;
import com.flowsurance.persistence.dao.conditions.OfferDao;
import com.flowsurance.persistence.hibernate.conditions.comparison.ComparisonHibernate;
import com.google.common.base.Optional;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComparisonRepository {

	private final UserContextService userContextService;

	private final PetitionRepository petitionRepository;

	private final ComparisonDao comparisonDao;

	private final ComparisonBuilder comparisonBuilder;

	private final OfferDao offerDao;

	@Autowired
	public ComparisonRepository(UserContextService userContextService,
			PetitionRepository petitionRepository,
			ComparisonDao comparisonDao,
			ComparisonBuilder comparisonBuilder,
			OfferDao offerDao) {
		this.userContextService = userContextService;
		this.petitionRepository = petitionRepository;
		this.comparisonDao = comparisonDao;
		this.comparisonBuilder = comparisonBuilder;
		this.offerDao = offerDao;
	}

	public Optional<Comparison> get(ComparisonId comparisonId) {
		return comparisonDao.get(comparisonId).transform(comparisonBuilder::build);
	}

	public List<AnemicComparison> getAnemicComparisonsForCurrentUser() {
		return getAnemicComparisonsForUser(userContextService.get().getId());
	}

	public List<AnemicComparison> getAnemicComparisonsForUser(UserId userId) {
		List<Petition> petitions = petitionRepository.getUserMappedPetitions(userId);

		getByPetitions(petitions).forEach(comparison -> comparisonDao.updateOffers(comparison.getId()));

		List<AnemicComparison> comparisons = getByPetitions(petitions).stream()
				.map(comparisonBuilder::buildAnemicComparison)
				.collect(Collectors.toList());

		List<Petition> comparedPetitions = comparisons.stream()
				.map(comparison -> comparison.getPetition().getId())
				.map(petitionRepository::getPetition)
				.map(Optional::get)
				.collect(Collectors.toList());

		for (Petition petition : petitions) {
			if (!comparedPetitions.contains(petition)) {
				comparisons.add(comparisonBuilder.buildAnemicComparison(comparisonDao.createForPetition(petition.getId())));
			}
		}

		return comparisons;
	}

	private List<ComparisonHibernate> getByPetitions(List<Petition> petitions) {
		return comparisonDao.getAllComparisonsByPetition(petitions.stream()
				.map(Petition::getId)
				.collect(Collectors.toList()));
	}
}
