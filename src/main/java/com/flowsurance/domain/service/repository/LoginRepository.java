package com.flowsurance.domain.service.repository;

import com.flowsurance.domain.model.user.CreateLoginRequest;
import com.flowsurance.domain.model.user.IsAuthorizedRequest;
import com.flowsurance.persistence.dao.LoginDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginRepository {

	private final LoginDao loginDao;

	@Autowired
	public LoginRepository(LoginDao loginDao) {
		this.loginDao = loginDao;
	}

	public void create(CreateLoginRequest request) {
		loginDao.createEntry(request);
	}

	public boolean isAuthorized(IsAuthorizedRequest request) {
		return loginDao.isAuthorized(request);
	}
}
