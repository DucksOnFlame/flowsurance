package com.flowsurance.domain.service;

import com.flowsurance.domain.model.user.User;
import org.springframework.stereotype.Service;

@Service
public class UserContextService {

	private final ThreadLocal<User> userThreadLocal = new ThreadLocal<>();

	public User get() {
		return userThreadLocal.get();
	}

	public void set(User user) {
		userThreadLocal.set(user);
	}

	public void clear() {
		userThreadLocal.remove();
	}
}
