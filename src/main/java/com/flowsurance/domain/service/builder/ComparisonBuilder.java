package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.comparison.AdditionalClauses;
import com.flowsurance.domain.model.conditions.comparison.AdditionalDeductibles;
import com.flowsurance.domain.model.conditions.comparison.AdditionalExclusions;
import com.flowsurance.domain.model.conditions.comparison.AnemicComparison;
import com.flowsurance.domain.model.conditions.comparison.ClauseComparisonRow;
import com.flowsurance.domain.model.conditions.comparison.ComparedClause;
import com.flowsurance.domain.model.conditions.comparison.ComparedDeductible;
import com.flowsurance.domain.model.conditions.comparison.ComparedExclusion;
import com.flowsurance.domain.model.conditions.comparison.Comparison;
import com.flowsurance.domain.model.conditions.comparison.DeductibleComparisonRow;
import com.flowsurance.domain.model.conditions.comparison.ExclusionComparisonRow;
import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.persistence.hibernate.conditions.comparison.ComparisonHibernate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.joda.money.BigMoney;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComparisonBuilder {

	private final PetitionBuilder petitionBuilder;

	private final OfferBuilder offerBuilder;

	@Autowired
	public ComparisonBuilder(PetitionBuilder petitionBuilder, OfferBuilder offerBuilder) {
		this.petitionBuilder = petitionBuilder;
		this.offerBuilder = offerBuilder;
	}

	public AnemicComparison buildAnemicComparison(ComparisonHibernate hibernate) {
		List<Offer> offers = hibernate.getOffers().stream()
				.map(offerBuilder::build)
				.collect(Collectors.toList());

		Petition petition = petitionBuilder.buildPetition(hibernate.getPetition());
		return new AnemicComparison(hibernate.getId(), petition, offers);
	}

	public Comparison build(ComparisonHibernate hibernate) {
		List<Offer> offers = hibernate.getOffers().stream()
				.map(offerBuilder::build)
				.collect(Collectors.toList());

		Petition petition = petitionBuilder.buildPetition(hibernate.getPetition());
		List<User> underwritters = offers.stream()
				.map(Offer::getInsurer)
				.collect(Collectors.toList());

		return new Comparison(
				hibernate.getId(),
				petition,
				underwritters,
				buildClauseComparisonRows(petition, offers),
				buildExclusionsComparisonRows(petition, offers),
				buildDeductibleComparisonRows(petition, offers),
				buildAdditionalClauses(offers),
				buildAdditionalExclusions(offers),
				buildAdditionalDeductibles(offers),
				buildPriceMap(offers)
		);
	}

	private List<ClauseComparisonRow> buildClauseComparisonRows(Petition petition, List<Offer> offers) {
		List<ClauseComparisonRow> comparisonRows = Lists.newArrayList();
		List<Clause> queriedClauses = petition.getClauses();

		for (Clause clause : queriedClauses) {
			Map<InsurerCompany, ComparedClause> comparedClauseMap = Maps.newHashMap();
			for (Offer offer : offers) {
				InsurerCompany insurer = InsurerCompany.valueOf(offer.getInsurer().getCompany());
				ComparedClause comparedClause = offer.getOfferedClauses().stream()
						.filter(offeredClause -> !offeredClause.getType().equals(OfferedConditionType.NEW))
						.filter(offeredClause -> offeredClause.getOriginalClause().get().equals(clause))
						.findFirst()
						.map(offeredClause -> new ComparedClause(offeredClause.getType(), offeredClause.getChangedClause()))
						.orElse(null);

				if (comparedClause == null) {
					throw new IllegalStateException("Could not find OfferedClause for Clause: " + clause + ", Insurer: " + insurer);
				}

				comparedClauseMap.put(insurer, comparedClause);
			}

			comparisonRows.add(new ClauseComparisonRow(clause, comparedClauseMap));
		}

		return comparisonRows;
	}

	private List<ExclusionComparisonRow> buildExclusionsComparisonRows(Petition petition, List<Offer> offers) {
		List<ExclusionComparisonRow> comparisonRows = Lists.newArrayList();
		List<Exclusion> queriedExclusions = petition.getExclusions();

		for (Exclusion exclusion : queriedExclusions) {
			Map<InsurerCompany, ComparedExclusion> comparedExclusionMap = Maps.newHashMap();
			for (Offer offer : offers) {
				InsurerCompany insurer = InsurerCompany.valueOf(offer.getInsurer().getCompany());
				ComparedExclusion comparedExclusion = offer.getOfferedExclusions().stream()
						.filter(offeredExclusion -> !offeredExclusion.getType().equals(OfferedConditionType.NEW))
						.filter(offeredExclusion -> offeredExclusion.getOriginalExclusion().get().equals(exclusion))
						.findFirst()
						.map(offeredExclusion -> new ComparedExclusion(offeredExclusion.getType(), offeredExclusion.getChangedExclusion()))
						.orElse(null);

				if (comparedExclusion == null) {
					throw new IllegalStateException("Could not find OfferedExclusion for Exclusion: " + exclusion + ", Insurer: " + insurer);
				}

				comparedExclusionMap.put(insurer, comparedExclusion);
			}

			comparisonRows.add(new ExclusionComparisonRow(exclusion, comparedExclusionMap));
		}

		return comparisonRows;
	}

	private List<DeductibleComparisonRow> buildDeductibleComparisonRows(Petition petition, List<Offer> offers) {
		List<DeductibleComparisonRow> comparisonRows = Lists.newArrayList();
		List<Deductible> queriedDeductibles = petition.getDeductibles();

		for (Deductible deductible : queriedDeductibles) {
			Map<InsurerCompany, ComparedDeductible> comparedDeductibleMap = Maps.newHashMap();
			for (Offer offer : offers) {
				InsurerCompany insurer = InsurerCompany.valueOf(offer.getInsurer().getCompany());
				ComparedDeductible comparedDeductible = offer.getOfferedDeductibles().stream()
						.filter(offeredDeductible -> !offeredDeductible.getType().equals(OfferedConditionType.NEW))
						.filter(offeredDeductible -> offeredDeductible.getOriginalDeductible().get().equals(deductible))
						.findFirst()
						.map(offeredDeductible -> new ComparedDeductible(offeredDeductible.getType(), offeredDeductible.getChangedDeductible()))
						.orElse(null);

				if (comparedDeductible == null) {
					throw new IllegalStateException("Could not find OfferedDeductible for Deductible: " + deductible + ", Insurer: " + insurer);
				}

				comparedDeductibleMap.put(insurer, comparedDeductible);
			}

			comparisonRows.add(new DeductibleComparisonRow(deductible, comparedDeductibleMap));
		}

		return comparisonRows;
	}

	private AdditionalClauses buildAdditionalClauses(List<Offer> offers) {
		Map<InsurerCompany, List<Clause>> additionalClauses = Maps.newHashMap();

		for (Offer offer : offers) {
			additionalClauses.put(
					InsurerCompany.valueOf(offer.getInsurer().getCompany()),
					offer.getOfferedClauses().stream()
							.filter(offeredClause -> offeredClause.getType().equals(OfferedConditionType.NEW))
							.map(offeredClause -> offeredClause.getChangedClause().get())
							.collect(Collectors.toList()));
		}

		return new AdditionalClauses(additionalClauses);
	}

	private AdditionalExclusions buildAdditionalExclusions(List<Offer> offers) {
		Map<InsurerCompany, List<Exclusion>> additionalExclusions = Maps.newHashMap();

		for (Offer offer : offers) {
			additionalExclusions.put(
					InsurerCompany.valueOf(offer.getInsurer().getCompany()),
					offer.getOfferedExclusions().stream()
							.filter(offeredExclusion -> offeredExclusion.getType().equals(OfferedConditionType.NEW))
							.map(offeredExclusion -> offeredExclusion.getChangedExclusion().get())
							.collect(Collectors.toList()));
		}

		return new AdditionalExclusions(additionalExclusions);
	}

	private AdditionalDeductibles buildAdditionalDeductibles(List<Offer> offers) {
		Map<InsurerCompany, List<Deductible>> additionalDeductibles = Maps.newHashMap();

		for (Offer offer : offers) {
			additionalDeductibles.put(
					InsurerCompany.valueOf(offer.getInsurer().getCompany()),
					offer.getOfferedDeductibles().stream()
							.filter(offeredDeductible -> offeredDeductible.getType().equals(OfferedConditionType.NEW))
							.map(offeredDeductible -> offeredDeductible.getChangedDeductible().get())
							.collect(Collectors.toList()));
		}

		return new AdditionalDeductibles(additionalDeductibles);
	}

	private Map<InsurerCompany, BigMoney> buildPriceMap(List<Offer> offers) {
		Map<InsurerCompany, BigMoney> priceMap = Maps.newHashMap();

		for (Offer offer : offers) {
			priceMap.put(InsurerCompany.valueOf(offer.getInsurer().getCompany()), offer.getPrice());
		}

		return priceMap;
	}
}
