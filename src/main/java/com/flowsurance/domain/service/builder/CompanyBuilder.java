package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.entity.BrokerCompany;
import com.flowsurance.domain.model.entity.ClientCompany;
import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.persistence.hibernate.CompanyHibernate;
import org.springframework.stereotype.Service;

@Service
public class CompanyBuilder {

	public Company buildCompany(CompanyHibernate hibernate) {
		return new Company(
				hibernate.getId(),
				hibernate.getName(),
				AddressBuilder.buildAddress(hibernate.getAddress()),
				hibernate.getType(),
				hibernate.getTaxPayerId()
		);
	}

	public InsurerCompany buildInsurerCompany(CompanyHibernate hibernate) {
		return InsurerCompany.valueOf(buildCompany(hibernate));
	}

	public BrokerCompany buildBrokerCompany(CompanyHibernate hibernate) {
		return BrokerCompany.valueOf(buildCompany(hibernate));
	}

	public ClientCompany buildClientCompany(CompanyHibernate hibernate) {
		return ClientCompany.valueOf(buildCompany(hibernate));
	}
}
