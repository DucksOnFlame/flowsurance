package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.BrokerStakes;
import com.flowsurance.domain.model.conditions.InsurerStakes;
import com.flowsurance.domain.model.entity.BrokerCompany;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.persistence.hibernate.conditions.StakesHibernate;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StakesBuilder {

	private final CompanyBuilder companyBuilder;

	@Autowired
	public StakesBuilder(CompanyBuilder companyBuilder) {
		this.companyBuilder = companyBuilder;
	}

	public InsurerStakes buildInsurerStakes(List<StakesHibernate> stakesHibernateList) {
		Map<InsurerCompany, Float> stakesMap = Maps.newHashMap();
		for (StakesHibernate hibernate : stakesHibernateList) {
			stakesMap.put(companyBuilder.buildInsurerCompany(hibernate.getCompany()), hibernate.getValue());
		}

		return new InsurerStakes(stakesMap);
	}

	public BrokerStakes buildBrokerStakes(List<StakesHibernate> stakesHibernateList) {
		Map<BrokerCompany, Float> stakesMap = Maps.newHashMap();
		for (StakesHibernate hibernate : stakesHibernateList) {
			stakesMap.put(companyBuilder.buildBrokerCompany(hibernate.getCompany()), hibernate.getValue());
		}

		return new BrokerStakes(stakesMap);
	}
}
