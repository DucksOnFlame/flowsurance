package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.Subject;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.entity.ClientCompany;
import com.flowsurance.persistence.hibernate.conditions.PetitionHibernate;
import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetitionBuilder {

	private final StakesBuilder stakesBuilder;

	private final CompanyBuilder companyBuilder;

	private final ClauseBuilder clauseBuilder;

	private final ExclusionBuilder exclusionBuilder;

	private final DeductibleBuilder deductibleBuilder;

	private final UserBuilder userBuilder;

	@Autowired
	public PetitionBuilder(StakesBuilder stakesBuilder,
			CompanyBuilder companyBuilder,
			ClauseBuilder clauseBuilder,
			ExclusionBuilder exclusionBuilder,
			DeductibleBuilder deductibleBuilder,
			UserBuilder userBuilder) {
		this.stakesBuilder = stakesBuilder;
		this.companyBuilder = companyBuilder;
		this.clauseBuilder = clauseBuilder;
		this.exclusionBuilder = exclusionBuilder;
		this.deductibleBuilder = deductibleBuilder;
		this.userBuilder = userBuilder;
	}

	public Petition buildPetition(PetitionHibernate hibernate) {
		List<Subject> subjects = hibernate.getSubjects().stream()
				.map(hibernateSubject -> new Subject(hibernateSubject.getItem()))
				.collect(Collectors.toList());

		List<ClientCompany> insureds = hibernate.getInsureds().stream()
				.map(companyBuilder::buildClientCompany)
				.collect(Collectors.toList());

		List<Clause> clauses = hibernate.getClauses().stream()
				.map(clauseBuilder::buildClause)
				.collect(Collectors.toList());

		List<Exclusion> exclusions = hibernate.getExclusions().stream()
				.map(exclusionBuilder::buildExclusion)
				.collect(Collectors.toList());

		List<Deductible> deductibles = hibernate.getDeductibles().stream()
				.map(deductibleBuilder::buildDeductible)
				.collect(Collectors.toList());

		return new Petition(
				hibernate.getId(),
				hibernate.getName(),
				hibernate.getState(),
				hibernate.getType(),
				subjects,
				new DateTime(hibernate.getCoverStartDate()),
				new DateTime(hibernate.getCoverEndDate()),
				companyBuilder.buildClientCompany(hibernate.getPolicyHolder()),
				insureds,
				stakesBuilder.buildBrokerStakes(hibernate.getBrokerStakes()),
				clauses,
				exclusions,
				deductibles,
				userBuilder.buildUser(hibernate.getBroker())
		);
	}
}
