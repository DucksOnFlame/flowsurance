package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.address.Address;
import com.flowsurance.persistence.hibernate.AddressHibernate;
import com.flowsurance.rest.dto.address.AddressDTO;
import org.springframework.stereotype.Service;

@Service
public class AddressBuilder {

	public static Address buildAddress(AddressDTO dto) {
		return new Address(
				dto.getId(),
				dto.getStreetAddress(),
				dto.getZipCode(),
				dto.getCity(),
				dto.getState(),
				dto.getCountry()
		);
	}

	public static Address buildAddress(AddressHibernate hibernate) {
		return new Address(
				hibernate.getId(),
				hibernate.getStreetAddress(),
				hibernate.getZipCode(),
				hibernate.getCity(),
				hibernate.getState(),
				hibernate.getCountry()
		);
	}
}
