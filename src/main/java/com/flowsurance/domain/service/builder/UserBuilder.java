package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.user.User;
import com.flowsurance.persistence.hibernate.UserHibernate;
import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBuilder {

	private final CompanyBuilder companyBuilder;

	@Autowired
	public UserBuilder(CompanyBuilder companyBuilder) {
		this.companyBuilder = companyBuilder;
	}

	public User buildUser(UserHibernate hibernate) {
		return new User(
				hibernate.getId(),
				hibernate.getUserName().getUserName(),
				hibernate.getDisplayName(),
				companyBuilder.buildCompany(hibernate.getCompany()),
				hibernate.getEmail(),
				hibernate.getJobTitle()
		);
	}

	public Optional<User> buildUser(Optional<UserHibernate> hibernateOptional) {
		UserHibernate hibernate = hibernateOptional.orNull();
		if (hibernate == null) {
			return Optional.absent();
		}

		return Optional.of(buildUser(hibernate));
	}
}
