package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.suite.InsuranceSuite;
import com.flowsurance.persistence.hibernate.InsuranceSuiteHibernate;
import com.flowsurance.rest.dto.suite.InsuranceSuiteDTO;
import org.springframework.stereotype.Service;

@Service
public class InsuranceSuiteBuilder {

	public InsuranceSuite buildInsuranceSuite(InsuranceSuiteDTO insuranceSuiteDTO) {
		return new InsuranceSuite(
				insuranceSuiteDTO.getId(),
				insuranceSuiteDTO.getName(),
				insuranceSuiteDTO.getType()
		);
	}

	public InsuranceSuite buildInsuranceSuite(InsuranceSuiteHibernate insuranceSuiteHibernate) {
		return new InsuranceSuite(
				insuranceSuiteHibernate.getId(),
				insuranceSuiteHibernate.getName(),
				insuranceSuiteHibernate.getType()
		);
	}
}
