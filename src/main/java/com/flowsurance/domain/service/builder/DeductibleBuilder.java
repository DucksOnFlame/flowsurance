package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import org.springframework.stereotype.Service;

@Service
public class DeductibleBuilder {

	public Deductible buildDeductible(DeductibleHibernate hibernate) {
		return new Deductible(hibernate.getDescription());
	}
}
