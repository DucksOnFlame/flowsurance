package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.domain.model.conditions.offer.OfferedClause;
import com.flowsurance.domain.model.conditions.offer.OfferedDeductible;
import com.flowsurance.domain.model.conditions.offer.OfferedExclusion;
import com.flowsurance.persistence.dao.conditions.ClauseDao;
import com.flowsurance.persistence.dao.conditions.ExclusionDao;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedDeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.offer.OfferedExclusionHibernate;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferBuilder {

	private final PetitionBuilder petitionBuilder;

	private final ClauseBuilder clauseBuilder;

	private final ClauseDao clauseDao;

	private final ExclusionBuilder exclusionBuilder;

	private final ExclusionDao exclusionDao;

	private final UserBuilder userBuilder;

	@Autowired
	public OfferBuilder(PetitionBuilder petitionBuilder,
			ClauseBuilder clauseBuilder,
			ClauseDao clauseDao,
			ExclusionBuilder exclusionBuilder,
			ExclusionDao exclusionDao,
			UserBuilder userBuilder) {
		this.petitionBuilder = petitionBuilder;
		this.clauseBuilder = clauseBuilder;
		this.clauseDao = clauseDao;
		this.exclusionBuilder = exclusionBuilder;
		this.exclusionDao = exclusionDao;
		this.userBuilder = userBuilder;
	}

	public Offer build(OfferHibernate hibernate) {
		return new Offer(
				hibernate.getId(),
				hibernate.getState(),
				petitionBuilder.buildPetition(hibernate.getPetition()),
				buildOfferedClauses(hibernate.getOfferedClauses()),
				buildOfferedExclusions(hibernate.getOfferedExclusions()),
				buildOfferedDeductibles(hibernate.getOfferedDeductibles()),
				hibernate.getPrice(),
				userBuilder.buildUser(hibernate.getInsurer())
		);
	}

	private List<OfferedClause> buildOfferedClauses(List<OfferedClauseHibernate> hibernates) {
		List<ClauseId> clauseIds = Lists.newArrayList();

		for (OfferedClauseHibernate hibernate : hibernates) {
			ClauseId originalClauseId = hibernate.getOriginalClause().orNull();
			if (originalClauseId != null) {
				clauseIds.add(originalClauseId);
			}

			ClauseId changedClauseId = hibernate.getChangedClause().orNull();
			if (changedClauseId != null) {
				clauseIds.add(changedClauseId);
			}
		}

		Map<ClauseId, Clause> clauseMap = clauseDao.getByIds(clauseIds).stream()
				.map(clauseBuilder::buildClause)
				.collect(Collectors.toMap(Clause::getId, clause -> clause));

		List<OfferedClause> offeredClauses = Lists.newArrayList();
		for (OfferedClauseHibernate hibernate : hibernates) {
			offeredClauses.add(new OfferedClause(
					hibernate.getOfferId(),
					hibernate.getOriginalClause().transform(clauseMap::get),
					hibernate.getType(),
					hibernate.getChangedClause().transform(clauseMap::get)
			));
		}

		return offeredClauses;
	}

	private List<OfferedExclusion> buildOfferedExclusions(List<OfferedExclusionHibernate> hibernates) {
		List<ExclusionId> exclusionIds = Lists.newArrayList();

		for (OfferedExclusionHibernate hibernate : hibernates) {
			ExclusionId originalExclusionId = hibernate.getOriginalExclusionId().orNull();
			if (originalExclusionId != null) {
				exclusionIds.add(originalExclusionId);
			}

			ExclusionId changedExclusionId = hibernate.getChangedExclusionId().orNull();
			if (changedExclusionId != null) {
				exclusionIds.add(changedExclusionId);
			}
		}

		Map<ExclusionId, Exclusion> exclusionMap = exclusionDao.getByIds(exclusionIds).stream()
				.map(exclusionBuilder::buildExclusion)
				.collect(Collectors.toMap(Exclusion::getId, exclusion -> exclusion));

		List<OfferedExclusion> offeredExclusions = Lists.newArrayList();
		for (OfferedExclusionHibernate hibernate : hibernates) {
			offeredExclusions.add(new OfferedExclusion(
					hibernate.getOfferId(),
					hibernate.getOriginalExclusionId().transform(exclusionMap::get),
					hibernate.getType(),
					hibernate.getChangedExclusionId().transform(exclusionMap::get)
			));
		}

		return offeredExclusions;
	}

	private List<OfferedDeductible> buildOfferedDeductibles(List<OfferedDeductibleHibernate> hibernates) {
		List<OfferedDeductible> offeredDeductibles = Lists.newArrayList();
		for (OfferedDeductibleHibernate hibernate : hibernates) {
			offeredDeductibles.add(new OfferedDeductible(
					hibernate.getOfferId(),
					hibernate.getOriginalDeductible().transform(originalDeductible -> new Deductible(originalDeductible.getDescription())),
					hibernate.getType(),
					hibernate.getChangedDeductible().transform(changedDeductible -> new Deductible(changedDeductible.getDescription()))
			));
		}

		return offeredDeductibles;
	}
}
