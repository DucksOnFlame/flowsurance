package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.persistence.hibernate.conditions.ExclusionHibernate;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import org.springframework.stereotype.Service;

@Service
public class ExclusionBuilder {

	public Exclusion buildExclusion(ExclusionDTO dto) {
		return new Exclusion(
				dto.getId(),
				dto.getContent()
		);
	}

	public Exclusion buildExclusion(ExclusionHibernate hibernate) {
		return new Exclusion(
				hibernate.getId(),
				hibernate.getContent()
		);
	}
}
