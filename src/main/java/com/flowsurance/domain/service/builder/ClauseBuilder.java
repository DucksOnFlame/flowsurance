package com.flowsurance.domain.service.builder;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Limit;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.persistence.hibernate.conditions.ClauseHibernate;
import com.flowsurance.persistence.hibernate.conditions.DeductibleHibernate;
import com.flowsurance.persistence.hibernate.conditions.LimitHibernate;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;
import com.flowsurance.rest.dto.conditions.LimitDTO;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class ClauseBuilder {

	public Clause buildClause(ClauseDTO dto) {
		return new Clause(
				dto.getId(),
				dto.getTitle(),
				dto.getContent(),
				dto.getLimits().stream()
						.map(this::buildLimit)
						.collect(Collectors.toList()),
				dto.getDeductibles().stream()
						.map(this::buildDeductible)
						.collect(Collectors.toList())
		);
	}

	private Limit buildLimit(LimitDTO dto) {
		return new Limit(
				dto.getDescription(),
				dto.getValuePerOccurrence(),
				dto.getValueInAggregate()
		);
	}

	private Deductible buildDeductible(DeductibleDTO dto) {
		return new Deductible(dto.getDescription());
	}

	public Clause buildClause(ClauseHibernate hibernate) {
		return new Clause(
				hibernate.getId(),
				hibernate.getTitle(),
				hibernate.getContent(),
				hibernate.getLimits().stream()
						.map(this::buildLimit)
						.collect(Collectors.toList()),
				hibernate.getDeductibles().stream()
						.map(this::buildDeductible)
						.collect(Collectors.toList())
		);
	}

	private Limit buildLimit(LimitHibernate hibernate) {
		return new Limit(
				hibernate.getDescription(),
				hibernate.getValuePerOccurrence(),
				hibernate.getValueInAggregate()
		);
	}

	private Deductible buildDeductible(DeductibleHibernate hibernate) {
		return new Deductible(hibernate.getDescription());
	}
}
