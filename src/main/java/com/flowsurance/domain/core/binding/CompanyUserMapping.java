package com.flowsurance.domain.core.binding;

import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.user.UserId;

public class CompanyUserMapping {

	private final UserId userId;

	private final CompanyId companyId;

	public CompanyUserMapping(UserId userId, CompanyId companyId) {
		this.userId = userId;
		this.companyId = companyId;
	}

	public UserId getUserId() {
		return userId;
	}

	public CompanyId getCompanyId() {
		return companyId;
	}
}
