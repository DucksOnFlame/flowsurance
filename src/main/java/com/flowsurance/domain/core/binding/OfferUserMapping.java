package com.flowsurance.domain.core.binding;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.user.UserId;

public class OfferUserMapping {

	private final UserId userId;

	private final OfferId offerId;

	public OfferUserMapping(UserId userId, OfferId offerId) {
		this.userId = userId;
		this.offerId = offerId;
	}

	public UserId getUserId() {
		return userId;
	}

	public OfferId getOfferId() {
		return offerId;
	}
}
