package com.flowsurance.domain.core.binding;

import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.user.UserId;

public class ExclusionUserMapping {

	private final UserId userId;

	private final ExclusionId exclusionId;

	public ExclusionUserMapping(UserId userId, ExclusionId exclusionId) {
		this.userId = userId;
		this.exclusionId = exclusionId;
	}

	public UserId getUserId() {
		return userId;
	}

	public ExclusionId getExclusionId() {
		return exclusionId;
	}
}
