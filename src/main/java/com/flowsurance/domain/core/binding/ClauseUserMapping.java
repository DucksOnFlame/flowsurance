package com.flowsurance.domain.core.binding;

import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.user.UserId;

public class ClauseUserMapping {

	private final ClauseUserMappingId id;

	private final UserId userId;

	private final ClauseId clauseId;

	public ClauseUserMapping(ClauseUserMappingId id, UserId userId, ClauseId clauseId) {
		this.id = id;
		this.userId = userId;
		this.clauseId = clauseId;
	}

	public ClauseUserMappingId getId() {
		return id;
	}

	public UserId getUserId() {
		return userId;
	}

	public ClauseId getClauseId() {
		return clauseId;
	}
}
