package com.flowsurance.domain.core.binding;

import com.flowsurance.util.IdValueObject;

public class ExclusionUserMappingId extends IdValueObject {

	public ExclusionUserMappingId(String id) {
		super(id);
	}
}
