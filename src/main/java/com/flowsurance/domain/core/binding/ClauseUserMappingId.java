package com.flowsurance.domain.core.binding;

import com.flowsurance.util.IdValueObject;

public class ClauseUserMappingId extends IdValueObject {

	public ClauseUserMappingId(String id) {
		super(id);
	}
}
