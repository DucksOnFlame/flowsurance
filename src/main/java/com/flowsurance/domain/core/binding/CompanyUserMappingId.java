package com.flowsurance.domain.core.binding;

import com.flowsurance.util.IdValueObject;

public class CompanyUserMappingId extends IdValueObject {

	public CompanyUserMappingId(String id) {
		super(id);
	}
}
