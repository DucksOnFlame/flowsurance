package com.flowsurance.domain.core.binding;

import com.flowsurance.util.IdValueObject;

public class OfferPetitionMappingId extends IdValueObject {

	public OfferPetitionMappingId(String id) {
		super(id);
	}
}
