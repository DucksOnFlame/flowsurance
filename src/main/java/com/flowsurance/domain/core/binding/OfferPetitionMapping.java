package com.flowsurance.domain.core.binding;

import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.petition.PetitionId;

public class OfferPetitionMapping {

	private final PetitionId petitionId;

	private final OfferId offerId;

	public OfferPetitionMapping(PetitionId petitionId, OfferId offerId) {
		this.petitionId = petitionId;
		this.offerId = offerId;
	}
}
