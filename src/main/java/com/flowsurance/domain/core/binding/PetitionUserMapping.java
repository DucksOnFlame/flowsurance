package com.flowsurance.domain.core.binding;

import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.user.UserId;

public class PetitionUserMapping {

	private final PetitionUserMappingId id;

	private final UserId userId;

	private final PetitionId conditionsId;

	public PetitionUserMapping(PetitionUserMappingId id, UserId userId, PetitionId conditionsId) {
		this.id = id;
		this.userId = userId;
		this.conditionsId = conditionsId;
	}

	public PetitionUserMappingId getId() {
		return id;
	}

	public UserId getUserId() {
		return userId;
	}

	public PetitionId getConditionsId() {
		return conditionsId;
	}
}
