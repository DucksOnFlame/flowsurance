package com.flowsurance.domain.core.binding;

import com.flowsurance.util.IdValueObject;

public class PetitionUserMappingId extends IdValueObject {

	public PetitionUserMappingId(String id) {
		super(id);
	}
}
