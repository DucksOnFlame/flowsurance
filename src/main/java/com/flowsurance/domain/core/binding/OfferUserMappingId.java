package com.flowsurance.domain.core.binding;

import com.flowsurance.util.IdValueObject;

public class OfferUserMappingId extends IdValueObject {

	public OfferUserMappingId(String id) {
		super(id);
	}
}
