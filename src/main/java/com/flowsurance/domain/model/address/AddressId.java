package com.flowsurance.domain.model.address;

import com.flowsurance.util.IdValueObject;

public class AddressId extends IdValueObject {

	public AddressId(String id) {
		super(id);
	}
}
