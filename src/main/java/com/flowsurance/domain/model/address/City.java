package com.flowsurance.domain.model.address;

import com.flowsurance.util.IdValueObject;

public class City extends IdValueObject {

	public City(String id) {
		super(id);
	}
}
