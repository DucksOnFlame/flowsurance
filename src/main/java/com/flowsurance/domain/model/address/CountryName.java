package com.flowsurance.domain.model.address;

import com.flowsurance.util.IdValueObject;

public class CountryName extends IdValueObject {

	public CountryName(String id) {
		super(id);
	}
}
