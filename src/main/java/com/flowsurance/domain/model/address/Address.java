package com.flowsurance.domain.model.address;

import java.util.Objects;

public class Address {

	private final AddressId id;

	private final StreetAddress streetAddress;

	private final ZipCode zipCode;

	private final City city;

	private final State state;

	private final Country country;

	public Address(AddressId id, StreetAddress streetAddress, ZipCode zipCode, City city, State state, Country country) {
		this.id = id;
		this.streetAddress = streetAddress;
		this.zipCode = zipCode;
		this.city = city;
		this.state = state;
		this.country = country;
	}

	public AddressId getId() {
		return id;
	}

	public StreetAddress getStreetAddress() {
		return streetAddress;
	}

	public ZipCode getZipCode() {
		return zipCode;
	}

	public City getCity() {
		return city;
	}

	public State getState() {
		return state;
	}

	public Country getCountry() {
		return country;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Address address = (Address) o;
		return Objects.equals(id, address.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
