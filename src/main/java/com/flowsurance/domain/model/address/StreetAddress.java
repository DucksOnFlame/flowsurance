package com.flowsurance.domain.model.address;

import com.flowsurance.util.IdValueObject;

public class StreetAddress extends IdValueObject {

	public StreetAddress(String id) {
		super(id);
	}
}
