package com.flowsurance.domain.model.address;

import java.io.Serializable;

public class CreateAddressRequest implements Serializable {

	private final String streetAddress;

	private final String zipCode;

	private final String city;

	private final String state;

	private final String countryName;

	public CreateAddressRequest(StreetAddress streetAddress, ZipCode zipCode, City city, State state, Country country) {
		this.streetAddress = streetAddress.get();
		this.zipCode = zipCode.get();
		this.city = city.get();
		this.state = state.get();
		this.countryName = country.getName().get();
	}

	public StreetAddress getStreetAddress() {
		return new StreetAddress(streetAddress);
	}

	public ZipCode getZipCode() {
		return new ZipCode(zipCode);
	}

	public City getCity() {
		return new City(city);
	}

	public State getState() {
		return new State(state);
	}

	public Country getCountry() {
		return Country.valueOf(new CountryName(countryName));
	}
}
