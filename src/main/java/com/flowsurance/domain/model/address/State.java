package com.flowsurance.domain.model.address;

import com.flowsurance.util.IdValueObject;

public class State extends IdValueObject {

	public State(String id) {
		super(id);
	}
}
