package com.flowsurance.domain.model.address;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.neovisionaries.i18n.CountryCode;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Countries {

	private static Map<CountryName, Country> countriesByName;

	private static Map<CountryCode, Country> countriesByCode;

	static {
		countriesByName = Maps.newHashMap();
		countriesByCode = Maps.newHashMap();
		CountryCode[] allCodes = CountryCode.values();

		for (CountryCode code : allCodes) {
			// Don't map non-official CountryCodes
			if (!shouldMap(code)) {
				continue;
			}

			CountryName name = new CountryName(new Locale("", code.toString()).getDisplayCountry());
			Country country = new Country(name, code);
			countriesByName.put(name, country);
			countriesByCode.put(code, country);
		}
	}

	private static boolean shouldMap(CountryCode code) {
		return CountryCode.Assignment.OFFICIALLY_ASSIGNED.equals(code.getAssignment())
				|| CountryCode.Assignment.USER_ASSIGNED.equals(code.getAssignment());
	}

	static Country getCountryByName(CountryName name) {
		Country country = countriesByName.get(name);
		if (country == null) {
			throw new IllegalArgumentException("Could not find Country by name: " + name);
		}

		return country;
	}

	static Country getCountryByCode(CountryCode code) {
		Country country = countriesByCode.get(code);
		if (country == null) {
			throw new IllegalArgumentException("Could not find Country by code: " + code);
		}

		return country;
	}

	public static List<Country> getAll() {
		return Lists.newArrayList(countriesByCode.values());
	}
}
