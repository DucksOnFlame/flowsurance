package com.flowsurance.domain.model.address;

import com.neovisionaries.i18n.CountryCode;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Country {

	private CountryName name;

	private CountryCode code;

	// Constructor should stay package-private
	Country(CountryName name, CountryCode code) {
		this.name = name;
		this.code = code;
	}

	public static Country valueOf(CountryName name) {
		return Countries.getCountryByName(name);
	}

	public static Country valueOf(CountryCode code) {
		return Countries.getCountryByCode(code);
	}

	public CountryName getName() {
		return name;
	}

	public CountryCode getCode() {
		return code;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder(17, 37);
		builder.append(code);
		builder.append(name);
		return builder.toHashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Country that = (Country) o;

		return this.code.equals(that.code);
	}

	@Override
	public String toString() {
		return "Country: code: " + code.name() + ", name: " + name.get();
	}
}
