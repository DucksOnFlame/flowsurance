package com.flowsurance.domain.model.address;

import com.flowsurance.util.IdValueObject;

public class ZipCode extends IdValueObject {

	public ZipCode(String id) {
		super(id);
	}
}