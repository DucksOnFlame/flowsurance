package com.flowsurance.domain.model.user;

import com.flowsurance.util.IdValueObject;

public class UserId extends IdValueObject {

	public UserId(String id) {
		super(id);
	}

	public static UserId orNull(String id) {
		if (id == null) {
			return null;
		}

		return new UserId(id);
	}
}
