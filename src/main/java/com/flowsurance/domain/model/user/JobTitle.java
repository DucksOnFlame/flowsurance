package com.flowsurance.domain.model.user;

import com.flowsurance.util.IdValueObject;

public class JobTitle extends IdValueObject {

	public JobTitle(String id) {
		super(id);
	}
}
