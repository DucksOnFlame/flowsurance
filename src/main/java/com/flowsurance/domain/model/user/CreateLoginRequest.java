package com.flowsurance.domain.model.user;

public class CreateLoginRequest {

	private final UserId userId;

	private final UserPassword password;

	public CreateLoginRequest(UserId userId, UserPassword password) {
		this.userId = userId;
		this.password = password;
	}

	public UserId getUserId() {
		return userId;
	}

	public UserPassword getPassword() {
		return password;
	}
}
