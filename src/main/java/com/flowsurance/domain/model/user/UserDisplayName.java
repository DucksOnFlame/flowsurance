package com.flowsurance.domain.model.user;

import com.flowsurance.util.IdValueObject;

public class UserDisplayName extends IdValueObject {

	public UserDisplayName(String id) {
		super(id);
	}
}
