package com.flowsurance.domain.model.user;

import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.CompanyId;
import java.io.Serializable;

public class CreateUserRequest implements Serializable {

	private final String name;

	private final String surname;

	private final String displayName;

	private final String companyId;

	private final String email;

	private final String jobTitle;

	private final String password;

	public CreateUserRequest(UserName userName,
			UserDisplayName displayName,
			Company company,
			Email email,
			JobTitle jobTitle,
			UserPassword password) {
		this.name = userName.getName();
		this.surname = userName.getSurname();
		this.displayName = displayName.get();
		this.companyId = company.getId().get();
		this.email = email.get();
		this.jobTitle = jobTitle.get();
		this.password = password.get();
	}

	public UserName getUserName() {
		return new UserName(name, surname);
	}

	public UserDisplayName getDisplayName() {
		return new UserDisplayName(displayName);
	}

	public CompanyId getCompanyId() {
		return new CompanyId(companyId);
	}

	public Email getEmail() {
		return new Email(email);
	}

	public JobTitle getJobTitle() {
		return new JobTitle(jobTitle);
	}

	public UserPassword getPassword() {
		return new UserPassword(password);
	}
}
