package com.flowsurance.domain.model.user;

import com.flowsurance.domain.model.entity.Company;

public class User {

	private final UserId id;

	private final UserName userName;

	private final UserDisplayName displayName;

	private final Company company;

	private final Email email;

	private final JobTitle jobTitle;

	public User(UserId id, UserName userName, UserDisplayName displayName, Company company, Email email, JobTitle jobTitle) {
		this.id = id;
		this.userName = userName;
		this.displayName = displayName;
		this.company = company;
		this.email = email;
		this.jobTitle = jobTitle;
	}

	public UserId getId() {
		return id;
	}

	public UserName getUserName() {
		return userName;
	}

	public UserDisplayName getDisplayName() {
		return displayName;
	}

	public Company getCompany() {
		return company;
	}

	public Email getEmail() {
		return email;
	}

	public JobTitle getJobTitle() {
		return jobTitle;
	}
}
