package com.flowsurance.domain.model.user;

import com.flowsurance.util.IdValueObject;

public class LoginId extends IdValueObject {

	public LoginId(String id) {
		super(id);
	}
}
