package com.flowsurance.domain.model.user;

public class UserName {

	private final String name;

	private final String surname;

	public UserName(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
}
