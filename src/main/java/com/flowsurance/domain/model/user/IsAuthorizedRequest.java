package com.flowsurance.domain.model.user;

public class IsAuthorizedRequest {

	private final UserId userId;

	private final UserPassword password;

	public IsAuthorizedRequest(UserId userId, UserPassword password) {
		this.userId = userId;
		this.password = password;
	}

	public UserId getUserId() {
		return userId;
	}

	public UserPassword getPassword() {
		return password;
	}
}
