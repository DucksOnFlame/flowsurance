package com.flowsurance.domain.model.entity;

import com.flowsurance.domain.model.address.Address;
import com.google.common.base.Preconditions;

public class InsurerCompany extends Company {

	private InsurerCompany(CompanyId id, CompanyName name, Address address, CompanyType type, TaxPayerId taxPayerId) {
		super(id, name, address, type, taxPayerId);
	}

	public static InsurerCompany valueOf(Company company) {
		Preconditions.checkArgument(company.getType().equals(CompanyType.INSURER), "Company was not of Insurer type! Company: " + company);
		return new InsurerCompany(
				company.getId(),
				company.getName(),
				company.getAddress(),
				company.getType(),
				company.getTaxPayerId()
		);
	}
}
