package com.flowsurance.domain.model.entity;

import com.flowsurance.util.IdValueObject;

public class CompanyName extends IdValueObject {

	public CompanyName(String id) {
		super(id);
	}
}
