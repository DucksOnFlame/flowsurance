package com.flowsurance.domain.model.entity;

import com.flowsurance.util.IdValueObject;

public class CompanyId extends IdValueObject {

	public CompanyId(String id) {
		super(id);
	}
}
