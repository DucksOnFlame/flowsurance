package com.flowsurance.domain.model.entity;

import com.flowsurance.domain.model.address.Address;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Company {

	private final CompanyId id;

	private final CompanyName name;

	private final Address address;

	private final CompanyType type;

	private final TaxPayerId taxPayerId;

	public Company(CompanyId id, CompanyName name, Address address, CompanyType type, TaxPayerId taxPayerId) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.type = type;
		this.taxPayerId = taxPayerId;
	}

	public CompanyId getId() {
		return id;
	}

	public CompanyName getName() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public CompanyType getType() {
		return type;
	}

	public TaxPayerId getTaxPayerId() {
		return taxPayerId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Company company = (Company) o;

		return new EqualsBuilder()
				.append(id, company.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
