package com.flowsurance.domain.model.entity;

import com.flowsurance.util.IdValueObject;

public class TaxPayerId extends IdValueObject {

	public TaxPayerId(String id) {
		super(id);
	}
}
