package com.flowsurance.domain.model.entity;

import com.flowsurance.domain.model.address.Address;
import com.google.common.base.Preconditions;

public class BrokerCompany extends Company {

	private BrokerCompany(CompanyId id, CompanyName name, Address address, CompanyType type, TaxPayerId taxPayerId) {
		super(id, name, address, type, taxPayerId);
	}

	public static BrokerCompany valueOf(Company company) {
		Preconditions.checkArgument(company.getType().equals(CompanyType.BROKER), "Company was not of Broker type! Company: " + company);
		return new BrokerCompany(
				company.getId(),
				company.getName(),
				company.getAddress(),
				company.getType(),
				company.getTaxPayerId()
		);
	}
}
