package com.flowsurance.domain.model.entity;

import com.flowsurance.domain.model.address.Address;
import com.google.common.base.Preconditions;

public class ClientCompany extends Company {

	private ClientCompany(CompanyId id, CompanyName name, Address address, CompanyType type, TaxPayerId taxPayerId) {
		super(id, name, address, type, taxPayerId);
	}

	public static ClientCompany valueOf(Company company) {
		Preconditions.checkArgument(company.getType().equals(CompanyType.CLIENT), "Company was not of Client type! Company: " + company);
		return new ClientCompany(
				company.getId(),
				company.getName(),
				company.getAddress(),
				company.getType(),
				company.getTaxPayerId()
		);
	}
}
