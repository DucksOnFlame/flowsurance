package com.flowsurance.domain.model.entity;

import com.flowsurance.domain.model.address.City;
import com.flowsurance.domain.model.address.Country;
import com.flowsurance.domain.model.address.CountryName;
import com.flowsurance.domain.model.address.CreateAddressRequest;
import com.flowsurance.domain.model.address.State;
import com.flowsurance.domain.model.address.StreetAddress;
import com.flowsurance.domain.model.address.ZipCode;

public class CreateCompanyRequest {

	private final String name;

	private final String street;

	private final String zipCode;

	private final String city;

	private final String state;

	private final String country;

	private final CompanyType type;

	private final String taxPayerId;

	public CreateCompanyRequest(CompanyName name, StreetAddress street,
			ZipCode zipCode,
			City city,
			State state,
			Country country,
			CompanyType type,
			TaxPayerId taxPayerId) {
		this.name = name.get();
		this.street = street.get();
		this.zipCode = zipCode.get();
		this.city = city.get();
		this.state = state.get();
		this.country = country.getName().get();
		this.type = type;
		this.taxPayerId = taxPayerId.get();
	}

	public CreateAddressRequest getCreateAddressRequest() {
		return new CreateAddressRequest(
				new StreetAddress(street),
				new ZipCode(zipCode),
				new City(city),
				new State(state),
				Country.valueOf(new CountryName(country))
		);
	}

	public CompanyName getName() {
		return new CompanyName(name);
	}

	public CompanyType getType() {
		return type;
	}

	public TaxPayerId getTaxPayerId() {
		return new TaxPayerId(taxPayerId);
	}
}
