package com.flowsurance.domain.model.suite;

public class CreateInsuranceSuiteRequest {

	private String name;

	private InsuranceSuiteType type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public InsuranceSuiteType getType() {
		return type;
	}

	public void setType(InsuranceSuiteType type) {
		this.type = type;
	}
}
