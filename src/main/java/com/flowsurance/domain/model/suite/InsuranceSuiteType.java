package com.flowsurance.domain.model.suite;

public enum InsuranceSuiteType {
	POLICY, OFFER, PETITION
}
