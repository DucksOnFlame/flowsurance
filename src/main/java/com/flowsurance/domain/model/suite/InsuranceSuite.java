package com.flowsurance.domain.model.suite;

import com.flowsurance.domain.model.conditions.petition.Petition;
import com.google.common.collect.Lists;
import java.util.List;

public class InsuranceSuite {

	private InsuranceSuiteId id;

	private String name;

	private List<Petition> petitionList = Lists.newArrayList();

	private InsuranceSuiteType type;

	public InsuranceSuite(InsuranceSuiteId id, String name, InsuranceSuiteType type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public InsuranceSuiteId getId() {
		return id;
	}

	public void setId(InsuranceSuiteId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Petition> getPetitionList() {
		return petitionList;
	}

	public void setPetitionList(List<Petition> petitionList) {
		this.petitionList = petitionList;
	}

	public InsuranceSuiteType getType() {
		return type;
	}

	public void setType(InsuranceSuiteType type) {
		this.type = type;
	}
}
