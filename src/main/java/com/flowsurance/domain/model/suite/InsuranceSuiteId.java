package com.flowsurance.domain.model.suite;

import com.flowsurance.util.IdValueObject;

public class InsuranceSuiteId extends IdValueObject {

	public InsuranceSuiteId(String id) {
		super(id);
	}
}
