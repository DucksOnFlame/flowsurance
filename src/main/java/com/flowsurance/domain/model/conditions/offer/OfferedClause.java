package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.google.common.base.Optional;

public class OfferedClause implements OfferedCondition {

	private final OfferId offerId;

	private final Optional<Clause> originalClause;

	private final OfferedConditionType type;

	private final Optional<Clause> changedClause;

	public OfferedClause(OfferId offerId,
			Optional<Clause> originalClause,
			OfferedConditionType type,
			Optional<Clause> changedClause) {
		this.offerId = offerId;
		this.originalClause = originalClause;
		this.type = type;
		this.changedClause = changedClause;
	}

	public OfferId getOfferId() {
		return offerId;
	}

	public Optional<Clause> getOriginalClause() {
		return originalClause;
	}

	@Override
	public OfferedConditionType getType() {
		return type;
	}

	public Optional<Clause> getChangedClause() {
		return changedClause;
	}
}
