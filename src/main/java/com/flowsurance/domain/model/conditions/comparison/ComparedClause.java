package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.google.common.base.Optional;

public class ComparedClause {

	private final OfferedConditionType type;

	private final Optional<Clause> clause;

	public ComparedClause(OfferedConditionType type, Optional<Clause> clause) {
		this.type = type;
		this.clause = clause;
	}

	public OfferedConditionType getType() {
		return type;
	}

	public Optional<Clause> getClause() {
		return clause;
	}
}
