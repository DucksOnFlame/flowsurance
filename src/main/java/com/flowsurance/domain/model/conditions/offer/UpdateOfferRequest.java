package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.rest.dto.conditions.offer.OfferedClauseDTO;
import com.flowsurance.rest.dto.conditions.offer.OfferedDeductibleDTO;
import com.flowsurance.rest.dto.conditions.offer.OfferedExclusionDTO;
import java.util.List;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;

public class UpdateOfferRequest {

	private String id;

	private List<OfferedClauseDTO> offeredClauses;

	private List<OfferedExclusionDTO> offeredExclusions;

	private List<OfferedDeductibleDTO> offeredDeductibles;

	private double amount;

	private String currencyCode;

	public OfferId getId() {
		return new OfferId(id);
	}

	public List<OfferedClauseDTO> getOfferedClauses() {
		return offeredClauses;
	}

	public List<OfferedExclusionDTO> getOfferedExclusions() {
		return offeredExclusions;
	}

	public List<OfferedDeductibleDTO> getOfferedDeductibles() {
		return offeredDeductibles;
	}

	public BigMoney getPrice() {
		return BigMoney.of(CurrencyUnit.of(currencyCode), amount);
	}
}
