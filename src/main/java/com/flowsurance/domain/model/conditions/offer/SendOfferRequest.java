package com.flowsurance.domain.model.conditions.offer;

public class SendOfferRequest {

	private String offerId;

	public OfferId getOfferId() {
		return new OfferId(offerId);
	}
}
