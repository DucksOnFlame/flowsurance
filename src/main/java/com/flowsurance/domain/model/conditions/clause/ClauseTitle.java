package com.flowsurance.domain.model.conditions.clause;

import com.flowsurance.util.IdValueObject;

public class ClauseTitle extends IdValueObject {

	public ClauseTitle(String id) {
		super(id);
	}
}
