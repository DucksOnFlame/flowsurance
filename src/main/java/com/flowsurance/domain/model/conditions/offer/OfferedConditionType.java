package com.flowsurance.domain.model.conditions.offer;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;

public enum OfferedConditionType {
	ACCEPTED("Zaakceptowane"),
	REJECTED("Odrzucone"),
	CHANGED("Zmienione"),
	NEW("Nowe"),
	TBD("TBD");

	private final String prettyName;

	OfferedConditionType(String prettyName) {
		this.prettyName = prettyName;
	}

	public static OfferedConditionType valueOfPrettyName(String prettyName) {
		for (OfferedConditionType type : values()) {
			if (prettyName.equals(type.getPrettyName())) {
				return type;
			}
		}

		throw new IllegalArgumentException("Could not find enum for prettyName: " + prettyName);
	}

	public static List<String> getAllPrettyNames() {
		return Lists.newArrayList(values()).stream()
				.map(type -> type.prettyName)
				.collect(Collectors.toList());
	}

	public String getPrettyName() {
		return prettyName;
	}
}
