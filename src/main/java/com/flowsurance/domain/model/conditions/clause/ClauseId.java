package com.flowsurance.domain.model.conditions.clause;

import com.flowsurance.util.IdValueObject;

public class ClauseId extends IdValueObject {

	public ClauseId(String id) {
		super(id);
	}
}
