package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.google.common.base.Optional;

public class OfferedExclusion implements OfferedCondition {

	private final OfferId offerId;

	private final Optional<Exclusion> originalExclusion;

	private final OfferedConditionType type;

	private final Optional<Exclusion> changedExclusion;

	public OfferedExclusion(OfferId offerId,
			Optional<Exclusion> originalExclusion,
			OfferedConditionType type,
			Optional<Exclusion> changedExclusion) {
		this.offerId = offerId;
		this.originalExclusion = originalExclusion;
		this.type = type;
		this.changedExclusion = changedExclusion;
	}

	public OfferId getOfferId() {
		return offerId;
	}

	public Optional<Exclusion> getOriginalExclusion() {
		return originalExclusion;
	}

	@Override
	public OfferedConditionType getType() {
		return type;
	}

	public Optional<Exclusion> getChangedExclusion() {
		return changedExclusion;
	}
}
