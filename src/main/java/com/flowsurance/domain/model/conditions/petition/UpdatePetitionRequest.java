package com.flowsurance.domain.model.conditions.petition;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;
import com.flowsurance.rest.dto.conditions.SubjectDTO;
import com.flowsurance.util.lang.DateUtils;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;

public class UpdatePetitionRequest {

	private String id;

	private String name;

	private String type;

	private String state;

	private List<SubjectDTO> subjects;

	private String coverStartDate;

	private String coverEndDate;

	private String policyHolderId;

	private List<String> insuredIds;

	private List<String> clauseIds;

	private List<String> exclusionIds;

	private List<DeductibleDTO> deductibles;

	public UpdatePetitionRequest() {
	}

	public PetitionId getId() {
		return new PetitionId(id);
	}

	public PetitionName getName() {
		return new PetitionName(name);
	}

	public InsuranceConditionsType getType() {
		return InsuranceConditionsType.valueOfPrettyName(type);
	}

	public PetitionState getState() {
		return PetitionState.valueOfPrettyName(state);
	}

	public List<SubjectDTO> getSubjects() {
		return Lists.newArrayList(subjects);
	}

	public DateTime getCoverStartDate() {
		return DateUtils.fromString(coverStartDate);
	}

	public DateTime getCoverEndDate() {
		return DateUtils.fromString(coverEndDate);
	}

	public CompanyId getPolicyHolderId() {
		return new CompanyId(policyHolderId);
	}

	public List<CompanyId> getInsuredIds() {
		return insuredIds.stream().map(CompanyId::new).collect(Collectors.toList());
	}

	public List<ClauseId> getClauseIds() {
		return clauseIds.stream().map(ClauseId::new).collect(Collectors.toList());
	}

	public List<ExclusionId> getExclusionIds() {
		return exclusionIds.stream().map(ExclusionId::new).collect(Collectors.toList());
	}

	public List<Deductible> getDeductibles() {
		return deductibles.stream().map(dto -> new Deductible(dto.getDescription())).collect(Collectors.toList());
	}
}
