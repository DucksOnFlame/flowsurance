package com.flowsurance.domain.model.conditions;

import com.flowsurance.domain.model.entity.BrokerCompany;
import com.google.common.collect.Maps;
import java.util.Map;

public class BrokerStakes extends AbstractStakes<BrokerCompany> {

	public BrokerStakes(Map<BrokerCompany, Float> stakes) {
		super(stakes);
	}

	public static BrokerStakes singleStakes(BrokerCompany brokerCompany) {
		Map<BrokerCompany, Float> stakes = Maps.newHashMap();
		stakes.put(brokerCompany, 1F);
		return new BrokerStakes(stakes);
	}
}
