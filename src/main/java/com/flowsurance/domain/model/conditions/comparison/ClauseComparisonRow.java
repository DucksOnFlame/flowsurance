package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.Map;

public class ClauseComparisonRow {

	private final Clause originalClause;

	private final Map<InsurerCompany, ComparedClause> comparedClauses;

	public ClauseComparisonRow(Clause originalClause,
			Map<InsurerCompany, ComparedClause> comparedClauses) {
		this.originalClause = originalClause;
		this.comparedClauses = comparedClauses;
	}

	public Clause getOriginalClause() {
		return originalClause;
	}

	public Map<InsurerCompany, ComparedClause> getComparedClauses() {
		return Maps.newHashMap(comparedClauses);
	}
}
