package com.flowsurance.domain.model.conditions.petition;

import com.flowsurance.domain.model.conditions.BrokerStakes;
import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.Subject;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.entity.ClientCompany;
import com.flowsurance.domain.model.user.User;
import com.google.common.collect.Lists;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

public class Petition {

	private final PetitionId id;

	private final PetitionName name;

	private final PetitionState state;

	private final InsuranceConditionsType type;

	private final List<Subject> subjects;

	private final DateTime coverStartDate;

	private final DateTime coverEndDate;

	private final ClientCompany policyHolder;

	private final List<ClientCompany> insureds;

	private final BrokerStakes brokerStakes;

	private final List<Clause> clauses;

	private final List<Exclusion> exclusions;

	private final List<Deductible> deductibles;

	private final User broker;

	public Petition(PetitionId id,
			PetitionName name,
			PetitionState state,
			InsuranceConditionsType type,
			List<Subject> subjects,
			DateTime coverStartDate,
			DateTime coverEndDate,
			ClientCompany policyHolder,
			List<ClientCompany> insureds,
			BrokerStakes brokerStakes,
			List<Clause> clauses,
			List<Exclusion> exclusions, List<Deductible> deductibles, User broker) {
		this.id = id;
		this.name = name;
		this.state = state;
		this.type = type;
		this.subjects = subjects;
		this.coverStartDate = coverStartDate;
		this.coverEndDate = coverEndDate;
		this.policyHolder = policyHolder;
		this.insureds = insureds;
		this.brokerStakes = brokerStakes;
		this.clauses = clauses;
		this.exclusions = exclusions;
		this.deductibles = deductibles;
		this.broker = broker;
	}

	public PetitionId getId() {
		return id;
	}

	public PetitionName getName() {
		return name;
	}

	public PetitionState getState() {
		return state;
	}

	public InsuranceConditionsType getType() {
		return type;
	}

	public List<Subject> getSubjects() {
		return Lists.newArrayList(subjects);
	}

	public DateTime getCoverStartDate() {
		return coverStartDate;
	}

	public DateTime getCoverEndDate() {
		return coverEndDate;
	}

	public ClientCompany getPolicyHolder() {
		return policyHolder;
	}

	public List<ClientCompany> getInsureds() {
		return Lists.newArrayList(insureds);
	}

	public BrokerStakes getBrokerStakes() {
		return brokerStakes;
	}

	public List<Clause> getClauses() {
		return Lists.newArrayList(clauses);
	}

	public List<Exclusion> getExclusions() {
		return Lists.newArrayList(exclusions);
	}

	public List<Deductible> getDeductibles() {
		return Lists.newArrayList(deductibles);
	}

	public User getBroker() {
		return broker;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Petition petition = (Petition) o;

		return new EqualsBuilder()
				.append(id, petition.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}
}
