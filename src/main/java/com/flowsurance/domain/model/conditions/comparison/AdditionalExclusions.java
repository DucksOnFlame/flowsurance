package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class AdditionalExclusions {

	private final Map<InsurerCompany, List<Exclusion>> additionalExclusions;

	public AdditionalExclusions(Map<InsurerCompany, List<Exclusion>> additionalExclusions) {
		this.additionalExclusions = additionalExclusions;
	}

	public Map<InsurerCompany, List<Exclusion>> getAdditionalExclusions() {
		return Maps.newHashMap(additionalExclusions);
	}
}
