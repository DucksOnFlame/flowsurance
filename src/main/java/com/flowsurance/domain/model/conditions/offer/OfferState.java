package com.flowsurance.domain.model.conditions.offer;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;

public enum OfferState {
	NEW("Nowe"),
	SENT("Wysłane"),
	ACCEPTED("Zaakceptowana"),
	REJECTED("Odrzucona");

	private final String prettyName;

	OfferState(String prettyName) {
		this.prettyName = prettyName;
	}

	public static OfferState valueOfPrettyName(String prettyName) {
		for (OfferState state : values()) {
			if (prettyName.equals(state.getPrettyName())) {
				return state;
			}
		}

		throw new IllegalArgumentException("Could not find enum for prettyName: " + prettyName);
	}

	public static List<String> getAllPrettyNames() {
		return Lists.newArrayList(values()).stream()
				.map(type -> type.prettyName)
				.collect(Collectors.toList());
	}

	public String getPrettyName() {
		return prettyName;
	}
}
