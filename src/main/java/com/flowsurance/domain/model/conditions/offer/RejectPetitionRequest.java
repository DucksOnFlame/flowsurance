package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.petition.PetitionId;

public class RejectPetitionRequest {

	private String petitionId;

	public PetitionId getOfferId() {
		return new PetitionId(petitionId);
	}
}
