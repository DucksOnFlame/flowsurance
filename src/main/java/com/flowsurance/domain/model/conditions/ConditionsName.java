package com.flowsurance.domain.model.conditions;

import com.flowsurance.util.IdValueObject;

public abstract class ConditionsName extends IdValueObject {

	public ConditionsName(String id) {
		super(id);
	}
}
