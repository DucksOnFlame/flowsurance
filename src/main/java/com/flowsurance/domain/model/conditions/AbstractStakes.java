package com.flowsurance.domain.model.conditions;

import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.CompanyId;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractStakes<COMPANY extends Company> {

	private Map<COMPANY, Float> stakes;

	public AbstractStakes(Map<COMPANY, Float> stakes) {
		validate(stakes);
		this.stakes = stakes;
	}

	private void validate(Map<COMPANY, Float> stakes) {
		Preconditions.checkNotNull(stakes);
		Float sum = 0f;
		for (Float stake : stakes.values()) {
			sum += stake;
		}

		Preconditions.checkArgument(sum.equals(1f), "Sum was not 100%! Sum: " + sum);
	}

	public Float getStakes(COMPANY insurerCompany) {
		return Preconditions.checkNotNull(stakes.get(insurerCompany), "Could not find stakes for: " + insurerCompany);
	}

	public Map<COMPANY, Float> getAllStakes() {
		return Maps.newHashMap(stakes);
	}

	public Map<CompanyId, Float> getAllStakesByIds() {
		return stakes.entrySet().stream()
				.collect(Collectors.toMap(entry -> entry.getKey().getId(), Map.Entry::getValue));
	}

	public Map<String, Float> getSerializableStakes() {
		Map<String, Float> serializable = Maps.newHashMap();
		for (Map.Entry<COMPANY, Float> entry : stakes.entrySet()) {
			serializable.put(entry.getKey().getName().get(), entry.getValue());
		}

		return serializable;
	}
}
