package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.google.common.base.Optional;

public class ComparedExclusion {

	private final OfferedConditionType type;

	private final Optional<Exclusion> exclusion;

	public ComparedExclusion(OfferedConditionType type, Optional<Exclusion> exclusion) {
		this.type = type;
		this.exclusion = exclusion;
	}

	public OfferedConditionType getType() {
		return type;
	}

	public Optional<Exclusion> getExclusion() {
		return exclusion;
	}
}
