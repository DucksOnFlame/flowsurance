package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.Deductible;
import com.google.common.base.Optional;

public class OfferedDeductible implements OfferedCondition {

	private final OfferId offerId;

	private final Optional<Deductible> originalDeductible;

	private final OfferedConditionType type;

	private final Optional<Deductible> changedDeductible;

	public OfferedDeductible(OfferId offerId,
			Optional<Deductible> originalDeductible,
			OfferedConditionType type,
			Optional<Deductible> changedDeductible) {
		this.offerId = offerId;
		this.originalDeductible = originalDeductible;
		this.type = type;
		this.changedDeductible = changedDeductible;
	}

	public OfferId getOfferId() {
		return offerId;
	}

	public Optional<Deductible> getOriginalDeductible() {
		return originalDeductible;
	}

	@Override
	public OfferedConditionType getType() {
		return type;
	}

	public Optional<Deductible> getChangedDeductible() {
		return changedDeductible;
	}
}
