package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.Map;

public class ExclusionComparisonRow {

	private final Exclusion originalExclusion;

	private final Map<InsurerCompany, ComparedExclusion> comparedExclusions;

	public ExclusionComparisonRow(Exclusion originalExclusion,
			Map<InsurerCompany, ComparedExclusion> comparedExclusions) {
		this.originalExclusion = originalExclusion;
		this.comparedExclusions = comparedExclusions;
	}

	public Exclusion getOriginalExclusion() {
		return originalExclusion;
	}

	public Map<InsurerCompany, ComparedExclusion> getComparedExclusions() {
		return Maps.newHashMap(comparedExclusions);
	}
}
