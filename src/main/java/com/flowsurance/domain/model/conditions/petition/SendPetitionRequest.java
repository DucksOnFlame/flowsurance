package com.flowsurance.domain.model.conditions.petition;

import com.flowsurance.domain.model.user.UserId;
import java.util.List;
import java.util.stream.Collectors;

public class SendPetitionRequest {

	private String petitionId;

	private List<String> insurerIds;

	public PetitionId getPetitionId() {
		return new PetitionId(petitionId);
	}

	public List<UserId> getInsurerIds() {
		return insurerIds.stream().map(UserId::new).collect(Collectors.toList());
	}
}
