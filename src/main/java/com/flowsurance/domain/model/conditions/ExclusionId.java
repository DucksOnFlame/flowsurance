package com.flowsurance.domain.model.conditions;

import com.flowsurance.util.IdValueObject;

public class ExclusionId extends IdValueObject {

	public ExclusionId(String id) {
		super(id);
	}
}
