package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class AdditionalClauses {

	private final Map<InsurerCompany, List<Clause>> additionalClauses;

	public AdditionalClauses(Map<InsurerCompany, List<Clause>> additionalClauses) {
		this.additionalClauses = additionalClauses;
	}

	public Map<InsurerCompany, List<Clause>> getAdditionalClauses() {
		return Maps.newHashMap(additionalClauses);
	}
}
