package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.Map;

public class DeductibleComparisonRow {

	private final Deductible originalDeductible;

	private final Map<InsurerCompany, ComparedDeductible> comparedDeductibles;

	public DeductibleComparisonRow(Deductible originalDeductible,
			Map<InsurerCompany, ComparedDeductible> comparedDeductibles) {
		this.originalDeductible = originalDeductible;
		this.comparedDeductibles = comparedDeductibles;
	}

	public Deductible getOriginalDeductible() {
		return originalDeductible;
	}

	public Map<InsurerCompany, ComparedDeductible> getComparedDeductibles() {
		return Maps.newHashMap(comparedDeductibles);
	}
}
