package com.flowsurance.domain.model.conditions.clause;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Limit;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.List;

public class CreateClauseRequest implements Serializable {

	private final String title;

	private final String content;

	private final List<Limit> limits;

	private final List<Deductible> deductibles;

	public CreateClauseRequest(ClauseTitle title,
			String content,
			List<Limit> limits,
			List<Deductible> deductibles) {
		this.title = title.get();
		this.content = content;
		this.limits = limits;
		this.deductibles = deductibles;
	}

	public ClauseTitle getTitle() {
		return new ClauseTitle(title);
	}

	public String getContent() {
		return content;
	}

	public List<Limit> getLimits() {
		if (limits == null) {
			return Lists.newArrayList();
		}

		return Lists.newArrayList(limits);
	}

	public List<Deductible> getDeductibles() {
		if (deductibles == null) {
			return Lists.newArrayList();
		}

		return Lists.newArrayList(deductibles);
	}
}
