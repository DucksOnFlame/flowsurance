package com.flowsurance.domain.model.conditions.petition;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;

public enum PetitionState {
	NEW("Nowe"),
	SENT("Wysłane"),
	COMPARISON("Porównanie"),
	DONE("Zakończone");

	private final String prettyName;

	PetitionState(String prettyName) {
		this.prettyName = prettyName;
	}

	public static PetitionState valueOfPrettyName(String prettyName) {
		for (PetitionState state : values()) {
			if (state.getPrettyName().equals(prettyName)) {
				return state;
			}
		}

		throw new IllegalArgumentException("Could not find enum for prettyName: " + prettyName);
	}

	public static List<String> getAllPrettyNames() {
		return Lists.newArrayList(values()).stream()
				.map(type -> type.prettyName)
				.collect(Collectors.toList());
	}

	public String getPrettyName() {
		return prettyName;
	}
}
