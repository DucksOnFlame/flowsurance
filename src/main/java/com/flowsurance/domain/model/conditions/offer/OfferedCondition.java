package com.flowsurance.domain.model.conditions.offer;

public interface OfferedCondition {

	OfferedConditionType getType();
}
