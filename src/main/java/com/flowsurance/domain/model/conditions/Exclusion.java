package com.flowsurance.domain.model.conditions;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Exclusion {

	private ExclusionId id;

	private String content;

	public Exclusion(ExclusionId id, String content) {
		this.id = id;
		this.content = content;
	}

	public ExclusionId getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Exclusion exclusion = (Exclusion) o;

		return new EqualsBuilder()
				.append(id, exclusion.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("content", content)
				.toString();
	}
}
