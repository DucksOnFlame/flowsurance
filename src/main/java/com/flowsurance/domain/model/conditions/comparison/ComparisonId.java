package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.util.IdValueObject;

public class ComparisonId extends IdValueObject {

	public ComparisonId(String id) {
		super(id);
	}
}
