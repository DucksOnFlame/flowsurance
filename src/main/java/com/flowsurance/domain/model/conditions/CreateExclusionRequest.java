package com.flowsurance.domain.model.conditions;

public class CreateExclusionRequest {

	private String content;

	public CreateExclusionRequest(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
}
