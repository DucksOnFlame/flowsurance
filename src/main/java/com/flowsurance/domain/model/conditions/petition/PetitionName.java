package com.flowsurance.domain.model.conditions.petition;

import com.flowsurance.domain.model.conditions.ConditionsName;

public class PetitionName extends ConditionsName {

	public PetitionName(String id) {
		super(id);
	}
}
