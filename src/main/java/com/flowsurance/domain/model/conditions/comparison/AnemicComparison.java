package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.google.common.collect.Lists;
import java.util.List;

public class AnemicComparison {

	private final ComparisonId id;

	private final Petition petition;

	private final List<Offer> offers;

	public AnemicComparison(ComparisonId id, Petition petition, List<Offer> offers) {
		this.id = id;
		this.petition = petition;
		this.offers = offers;
	}

	public ComparisonId getId() {
		return id;
	}

	public Petition getPetition() {
		return petition;
	}

	public List<Offer> getOffers() {
		return Lists.newArrayList(offers);
	}
}
