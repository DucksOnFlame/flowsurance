package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.petition.PetitionId;

public class MoveToOffersRequest {

	private String petitionId;

	public PetitionId getPetitionId() {
		return new PetitionId(petitionId);
	}
}
