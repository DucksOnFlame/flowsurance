package com.flowsurance.domain.model.conditions;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;

public enum InsuranceConditionsType {
	PROPERTY_DAMAGE("Ubezpieczenie majątkowe"),
	GENERAL_LIABILITY("Ubezpieczenie OC"),
	CARGO("Ubezpieczenie Cargo"),
	CARRIERS_LIABILITY("Ubezpieczenie OC Przewoźnika"),
	DNO("Directors & Officers");

	private final String prettyName;

	InsuranceConditionsType(String prettyName) {
		this.prettyName = prettyName;
	}

	public static InsuranceConditionsType valueOfPrettyName(String prettyName) {
		switch (prettyName) {
			case "Ubezpieczenie majątkowe":
				return PROPERTY_DAMAGE;
			case "Ubezpieczenie OC":
				return GENERAL_LIABILITY;
			case "Ubezpieczenie Cargo":
				return CARGO;
			case "Ubezpieczenie OC Przewoźnika":
				return CARRIERS_LIABILITY;
			case "Directors & Officers":
				return DNO;
			default:
				throw new IllegalArgumentException("Could not find enum for prettyName: " + prettyName);
		}
	}

	public static List<String> getAllPrettyNames() {
		return Lists.newArrayList(values()).stream()
				.map(type -> type.prettyName)
				.collect(Collectors.toList());
	}

	public String getPrettyName() {
		return prettyName;
	}
}
