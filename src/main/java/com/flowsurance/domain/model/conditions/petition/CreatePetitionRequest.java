package com.flowsurance.domain.model.conditions.petition;

import com.flowsurance.domain.model.conditions.BrokerStakes;
import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.util.IdValueObject;
import com.flowsurance.util.lang.DateUtils;
import com.google.common.base.Preconditions;
import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;

public class CreatePetitionRequest {

	private final String name;

	private final String type;

	private final String coverStartDate;

	private final String coverEndDate;

	private final String policyHolderId;

	private final List<String> insuredIds;

	private User broker;

	private BrokerStakes brokerStakes;

	public CreatePetitionRequest(PetitionName name,
			InsuranceConditionsType type,
			DateTime coverStartDate,
			DateTime coverEndDate,
			CompanyId policyHolderId,
			List<CompanyId> insuredIds) {
		this.name = name.get();
		this.type = type.getPrettyName();
		this.coverStartDate = DateUtils.dateTimeToString(coverStartDate);
		this.coverEndDate = DateUtils.dateTimeToString(coverEndDate);
		this.policyHolderId = policyHolderId.get();
		this.insuredIds = insuredIds.stream().map(IdValueObject::get).collect(Collectors.toList());
	}

	public PetitionName getName() {
		return new PetitionName(name);
	}

	public InsuranceConditionsType getType() {
		return InsuranceConditionsType.valueOfPrettyName(type);
	}

	public PetitionState getState() {
		return PetitionState.NEW;
	}

	public DateTime getCoverStartDate() {
		return DateUtils.fromString(coverStartDate);
	}

	public DateTime getCoverEndDate() {
		return DateUtils.fromString(coverEndDate);
	}

	public CompanyId getPolicyHolderId() {
		return new CompanyId(policyHolderId);
	}

	public List<CompanyId> getInsuredIds() {
		return insuredIds.stream()
				.map(CompanyId::new)
				.collect(Collectors.toList());
	}

	public BrokerStakes getBrokerStakes() {
		return brokerStakes;
	}

	public void setBrokerStakes(BrokerStakes brokerStakes) {
		this.brokerStakes = Preconditions.checkNotNull(brokerStakes);
	}

	public User getBroker() {
		return broker;
	}

	public void setBroker(User broker) {
		this.broker = Preconditions.checkNotNull(broker);
	}
}
