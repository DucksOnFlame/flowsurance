package com.flowsurance.domain.model.conditions;

public class Subject {

	private final String item;

	public Subject(String item) {
		this.item = item;
	}

	public String getItem() {
		return item;
	}
}
