package com.flowsurance.domain.model.conditions.offer;

public class OfferNotReadyException extends Exception {

	public OfferNotReadyException() {
		super("Offer of is not ready! Some conditions from the petition have not been addressed!");
	}
}
