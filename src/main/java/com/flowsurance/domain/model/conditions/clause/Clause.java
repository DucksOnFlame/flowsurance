package com.flowsurance.domain.model.conditions.clause;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.Limit;
import com.flowsurance.util.Utilities;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Clause {

	private ClauseId id;

	private ClauseTitle title;

	private String content;

	private List<Limit> limits = new ArrayList<>();

	private List<Deductible> deductibles = new ArrayList<>();

	public Clause(ClauseId id,
			ClauseTitle title,
			String content,
			List<Limit> limits,
			List<Deductible> deductibles) {
		Utilities.checkNoneNull(id, title, content, limits, deductibles);
		this.id = id;
		this.title = title;
		this.content = content;
		this.limits.addAll(limits);
		this.deductibles.addAll(deductibles);
	}

	public ClauseId getId() {
		return id;
	}

	public ClauseTitle getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public List<Limit> getLimits() {
		return limits;
	}

	public List<Deductible> getDeductibles() {
		return deductibles;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Clause clause = (Clause) o;

		return new EqualsBuilder()
				.append(id, clause.id)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(id)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("title", title)
				.append("content", content)
				.append("limits", limits)
				.append("deductibles", deductibles)
				.toString();
	}
}
