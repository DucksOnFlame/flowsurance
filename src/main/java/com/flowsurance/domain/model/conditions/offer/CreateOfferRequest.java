package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.user.UserId;

public class CreateOfferRequest {

	private final String petitionId;

	private UserId insurerUserId;

	public CreateOfferRequest(String petitionId) {
		this.petitionId = petitionId;
	}

	public PetitionId getPetitionId() {
		return new PetitionId(petitionId);
	}

	public UserId getInsurerUserId() {
		return insurerUserId;
	}

	public void setInsurerUserId(UserId insurerUserId) {
		this.insurerUserId = insurerUserId;
	}
}
