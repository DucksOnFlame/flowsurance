package com.flowsurance.domain.model.conditions;

import com.flowsurance.util.Utilities;
import com.google.common.base.Optional;
import org.joda.money.BigMoney;

public class Limit {

	private String description;

	private BigMoney valuePerOccurrence;

	private BigMoney valueInAggregate;

	public Limit(String description, BigMoney valuePerOccurrence, BigMoney valueInAggregate) {
		Utilities.checkNoneNull(description);
		Utilities.checkAtLeastOneNotNull(valuePerOccurrence, valueInAggregate);
		this.description = description;
		this.valuePerOccurrence = valuePerOccurrence;
		this.valueInAggregate = valueInAggregate;
	}

	public String getDescription() {
		return description;
	}

	public Optional<BigMoney> getValuePerOccurrence() {
		return Optional.fromNullable(valuePerOccurrence);
	}

	public Optional<BigMoney> getValueInAggregate() {
		return Optional.fromNullable(valueInAggregate);
	}
}
