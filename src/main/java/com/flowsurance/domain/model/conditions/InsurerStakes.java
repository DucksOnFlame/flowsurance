package com.flowsurance.domain.model.conditions;

import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.Map;

public class InsurerStakes extends AbstractStakes<InsurerCompany> {

	public InsurerStakes(Map<InsurerCompany, Float> stakes) {
		super(stakes);
	}

	public static InsurerStakes singleStakes(InsurerCompany insurerCompany) {
		Map<InsurerCompany, Float> stakes = Maps.newHashMap();
		stakes.put(insurerCompany, 1F);
		return new InsurerStakes(stakes);
	}
}
