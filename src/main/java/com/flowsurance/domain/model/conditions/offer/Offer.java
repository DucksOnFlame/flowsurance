package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.user.User;
import com.google.common.collect.Lists;
import java.util.List;
import org.joda.money.BigMoney;

public class Offer {

	private final OfferId id;

	private final OfferState offerState;

	private final Petition petition;

	private final List<OfferedClause> offeredClauses;

	private final List<OfferedExclusion> offeredExclusions;

	private final List<OfferedDeductible> offeredDeductibles;

	private final BigMoney price;

	private final User insurer;

	public Offer(OfferId id,
			OfferState offerState,
			Petition petition,
			List<OfferedClause> offeredClauses,
			List<OfferedExclusion> offeredExclusions,
			List<OfferedDeductible> offeredDeductibles,
			BigMoney price,
			User insurer) {
		this.id = id;
		this.offerState = offerState;
		this.petition = petition;
		this.offeredClauses = offeredClauses;
		this.offeredExclusions = offeredExclusions;
		this.offeredDeductibles = offeredDeductibles;
		this.price = price;
		this.insurer = insurer;
	}

	public OfferId getId() {
		return id;
	}

	public OfferState getOfferState() {
		return offerState;
	}

	public Petition getPetition() {
		return petition;
	}

	public List<OfferedClause> getOfferedClauses() {
		return Lists.newArrayList(offeredClauses);
	}

	public List<OfferedExclusion> getOfferedExclusions() {
		return Lists.newArrayList(offeredExclusions);
	}

	public List<OfferedDeductible> getOfferedDeductibles() {
		return Lists.newArrayList(offeredDeductibles);
	}

	public List<OfferedCondition> getAllOfferedConditions() {
		List<OfferedCondition> allConditions = Lists.newArrayList();
		allConditions.addAll(offeredClauses);
		allConditions.addAll(offeredExclusions);
		allConditions.addAll(offeredDeductibles);
		return allConditions;
	}

	public BigMoney getPrice() {
		return price;
	}

	public User getInsurer() {
		return insurer;
	}
}
