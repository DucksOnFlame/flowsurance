package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.Subject;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.conditions.petition.PetitionName;
import com.flowsurance.domain.model.entity.ClientCompany;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.domain.model.user.User;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import org.joda.money.BigMoney;
import org.joda.time.DateTime;

public class Comparison {

	private final ComparisonId id;

	private final PetitionId petitionId;

	private final PetitionName name;

	private final InsuranceConditionsType type;

	private final List<Subject> subjects;

	private final DateTime coverStartDate;

	private final DateTime coverEndDate;

	private final ClientCompany policyHolder;

	private final List<ClientCompany> insureds;

	private final User broker;

	private final List<User> underwritters;

	private final List<ClauseComparisonRow> comparedClauses;

	private final List<ExclusionComparisonRow> comparedExclusions;

	private final List<DeductibleComparisonRow> comparedDeductibles;

	private final AdditionalClauses additionalClauses;

	private final AdditionalExclusions additionalExclusions;

	private final AdditionalDeductibles additionalDeductibles;

	private final Map<InsurerCompany, BigMoney> priceMap;

	public Comparison(ComparisonId id,
			Petition petition,
			List<User> underwritters,
			List<ClauseComparisonRow> comparedClauses,
			List<ExclusionComparisonRow> comparedExclusions,
			List<DeductibleComparisonRow> comparedDeductibles,
			AdditionalClauses additionalClauses,
			AdditionalExclusions additionalExclusions,
			AdditionalDeductibles additionalDeductibles,
			Map<InsurerCompany, BigMoney> priceMap) {
		this.id = id;
		this.petitionId = petition.getId();
		this.name = petition.getName();
		this.type = petition.getType();
		this.subjects = petition.getSubjects();
		this.coverStartDate = petition.getCoverStartDate();
		this.coverEndDate = petition.getCoverEndDate();
		this.policyHolder = petition.getPolicyHolder();
		this.insureds = petition.getInsureds();
		this.broker = petition.getBroker();
		this.underwritters = underwritters;
		this.comparedClauses = comparedClauses;
		this.comparedExclusions = comparedExclusions;
		this.comparedDeductibles = comparedDeductibles;
		this.additionalClauses = additionalClauses;
		this.additionalExclusions = additionalExclusions;
		this.additionalDeductibles = additionalDeductibles;
		this.priceMap = priceMap;
	}

	public ComparisonId getId() {
		return id;
	}

	public PetitionId getPetitionId() {
		return petitionId;
	}

	public PetitionName getName() {
		return name;
	}

	public InsuranceConditionsType getType() {
		return type;
	}

	public List<Subject> getSubjects() {
		return Lists.newArrayList(subjects);
	}

	public DateTime getCoverStartDate() {
		return coverStartDate;
	}

	public DateTime getCoverEndDate() {
		return coverEndDate;
	}

	public ClientCompany getPolicyHolder() {
		return policyHolder;
	}

	public List<ClientCompany> getInsureds() {
		return Lists.newArrayList(insureds);
	}

	public User getBroker() {
		return broker;
	}

	public List<User> getUnderwritters() {
		return Lists.newArrayList(underwritters);
	}

	public List<ClauseComparisonRow> getClauseComparisonRows() {
		return Lists.newArrayList(comparedClauses);
	}

	public List<ExclusionComparisonRow> getExclusionComparisonRows() {
		return Lists.newArrayList(comparedExclusions);
	}

	public List<DeductibleComparisonRow> getDeductibleComparisonRows() {
		return Lists.newArrayList(comparedDeductibles);
	}

	public AdditionalClauses getAdditionalClauses() {
		return additionalClauses;
	}

	public AdditionalExclusions getAdditionalExclusions() {
		return additionalExclusions;
	}

	public AdditionalDeductibles getAdditionalDeductibles() {
		return additionalDeductibles;
	}

	public Map<InsurerCompany, BigMoney> getPriceMap() {
		return Maps.newHashMap(priceMap);
	}
}
