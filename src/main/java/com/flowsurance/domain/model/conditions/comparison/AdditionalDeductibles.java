package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class AdditionalDeductibles {

	private final Map<InsurerCompany, List<Deductible>> additionalDeductibles;

	public AdditionalDeductibles(Map<InsurerCompany, List<Deductible>> additionalDeductibles) {
		this.additionalDeductibles = additionalDeductibles;
	}

	public Map<InsurerCompany, List<Deductible>> getAdditionalDeductibles() {
		return Maps.newHashMap(additionalDeductibles);
	}
}
