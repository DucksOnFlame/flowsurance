package com.flowsurance.domain.model.conditions.petition;

import com.flowsurance.util.IdValueObject;

public class PetitionId extends IdValueObject {

	public PetitionId(String id) {
		super(id);
	}
}
