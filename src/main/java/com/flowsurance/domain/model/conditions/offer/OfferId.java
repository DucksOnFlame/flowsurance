package com.flowsurance.domain.model.conditions.offer;

import com.flowsurance.util.IdValueObject;

public class OfferId extends IdValueObject {

	public OfferId(String id) {
		super(id);
	}
}
