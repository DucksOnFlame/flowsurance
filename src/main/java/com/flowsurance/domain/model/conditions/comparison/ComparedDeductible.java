package com.flowsurance.domain.model.conditions.comparison;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.google.common.base.Optional;

public class ComparedDeductible {

	private final OfferedConditionType type;

	private final Optional<Deductible> deductible;

	public ComparedDeductible(OfferedConditionType type, Optional<Deductible> deductible) {
		this.type = type;
		this.deductible = deductible;
	}

	public OfferedConditionType getType() {
		return type;
	}

	public Optional<Deductible> getDeductible() {
		return deductible;
	}
}
