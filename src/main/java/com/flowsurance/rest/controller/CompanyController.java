package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.entity.Company;
import com.flowsurance.domain.model.entity.CompanyId;
import com.flowsurance.domain.model.entity.CreateCompanyRequest;
import com.flowsurance.domain.service.repository.CompanyRepository;
import com.flowsurance.rest.dto.entity.CompanyDTO;
import com.flowsurance.util.rest.DeleteConfirmationDTO;
import com.flowsurance.util.rest.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/company")
public class CompanyController {

	private final CompanyRepository companyRepository;

	@Autowired
	public CompanyController(CompanyRepository companyRepository) {
		this.companyRepository = companyRepository;
	}

	@GetMapping
	public List<CompanyDTO> getAllCompanies() {
		return companyRepository.getAllCompaniesForCurrentUser().stream()
				.map(CompanyDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{companyId}")
	public CompanyDTO getCompany(@PathVariable("companyId") CompanyId companyId) {
		Company company = companyRepository.getCompany(companyId).orNull();
		if (company == null) {
			throw new NotFoundException("Company of id: " + companyId + " not found!");
		}

		return new CompanyDTO(company);
	}

	@PutMapping("/create")
	public CompanyDTO createCompany(@RequestBody CreateCompanyRequest request) {
		return new CompanyDTO(companyRepository.createCompany(request));
	}

	@DeleteMapping("/{companyId}")
	public DeleteConfirmationDTO deleteCompany(@PathVariable("companyId") CompanyId companyId) {
		companyRepository.deleteCompany(companyId);
		return DeleteConfirmationDTO.get();
	}
}
