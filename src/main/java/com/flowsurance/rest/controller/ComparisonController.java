package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.conditions.comparison.Comparison;
import com.flowsurance.domain.model.conditions.comparison.ComparisonId;
import com.flowsurance.domain.service.repository.ComparisonRepository;
import com.flowsurance.rest.dto.conditions.comparison.AnemicComparisonDTO;
import com.flowsurance.rest.dto.conditions.comparison.ComparisonDTO;
import com.flowsurance.util.rest.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comparison")
public class ComparisonController {

	private final ComparisonRepository comparisonRepository;

	@Autowired
	public ComparisonController(ComparisonRepository comparisonRepository) {
		this.comparisonRepository = comparisonRepository;
	}

	@GetMapping
	public List<AnemicComparisonDTO> getUserMappedComparisons() {
		return comparisonRepository.getAnemicComparisonsForCurrentUser().stream()
				.map(AnemicComparisonDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{comparisonId}")
	public ComparisonDTO getById(@PathVariable("comparisonId") ComparisonId comparisonId) {
		Comparison comparison = comparisonRepository.get(comparisonId).orNull();
		if (comparison == null) {
			throw new NotFoundException("Could not find Comparison by id: " + comparisonId);
		}

		return new ComparisonDTO(comparison);
	}
}
