package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.user.CreateLoginRequest;
import com.flowsurance.domain.model.user.CreateUserRequest;
import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.repository.LoginRepository;
import com.flowsurance.domain.service.repository.UserRepository;
import com.flowsurance.rest.dto.user.IsAuthorizedDTO;
import com.flowsurance.rest.dto.user.UserConfigDTO;
import com.flowsurance.rest.dto.user.UserDTO;
import com.flowsurance.util.rest.DeleteConfirmationDTO;
import com.flowsurance.util.rest.NotFoundException;
import com.flowsurance.util.rest.RedirectResponseDTO;
import com.flowsurance.util.rest.UnauthorizedException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

	private final UserRepository userRepository;

	private final LoginRepository loginRepository;

	@Autowired
	public UserController(UserRepository userRepository, LoginRepository loginRepository) {
		this.userRepository = userRepository;
		this.loginRepository = loginRepository;
	}

	@GetMapping
	public List<UserDTO> getAllUsers() {
		return userRepository.getAllUsers().stream()
				.map(UserDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/insurer")
	public List<UserDTO> getAllInsurers() {
		return userRepository.getAllInsurers().stream()
				.map(UserDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{userId}")
	public UserDTO getUser(@PathVariable("userId") UserId userId) {
		User user = userRepository.getUser(userId).orNull();
		if (user == null) {
			throw new NotFoundException("User of id: " + userId + " not found!");
		}

		return new UserDTO(user);
	}

	@GetMapping("/config")
	public UserConfigDTO getCurrentUserConfig() {
		return userRepository.getCurrentUserConfig();
	}

	@PutMapping("/create")
	public UserDTO createUser(@RequestBody CreateUserRequest request) {
		User user = userRepository.createUser(request);
		UserDTO userDTO = new UserDTO(user);
		loginRepository.create(new CreateLoginRequest(user.getId(), request.getPassword()));
		return userDTO;
	}

	@DeleteMapping("/{userId}")
	public DeleteConfirmationDTO deleteUser(@PathVariable("userId") UserId userId) {
		userRepository.deleteUser(userId);
		return DeleteConfirmationDTO.get();
	}

	@PostMapping("/login")
	public RedirectResponseDTO login(@RequestBody IsAuthorizedDTO request) {
		boolean authorized = userRepository.authorize(request.getDisplayName(), request.getPassword());
		if (authorized) {
			return new RedirectResponseDTO("/mainPage.html");
		} else {
			throw new UnauthorizedException("Not authorized!");
		}
	}
}
