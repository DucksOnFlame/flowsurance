package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.clause.CreateClauseRequest;
import com.flowsurance.domain.service.repository.ClauseRepository;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.flowsurance.util.rest.DeleteConfirmationDTO;
import com.flowsurance.util.rest.NotFoundException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clause")
public class ClauseController {

	private final ClauseRepository clauseRepository;

	@Autowired
	public ClauseController(ClauseRepository clauseRepository) {
		this.clauseRepository = clauseRepository;
	}

	@GetMapping
	public List<ClauseDTO> getAllClausesForCurrentUser() {
		return clauseRepository.getAllClausesForCurrentUser().stream()
				.map(ClauseDTO::new)
				.sorted(Comparator.comparing(ClauseDTO::getId))
				.collect(Collectors.toList());
	}

	@GetMapping("/{clauseId}")
	public ClauseDTO getClause(@PathVariable("clauseId") ClauseId clauseId) {
		Clause clause = clauseRepository.getClause(clauseId).orNull();
		if (clause == null) {
			throw new NotFoundException("Clause of id: " + clauseId + " not found!");
		}

		return new ClauseDTO(clause);
	}

	@PutMapping("/create")
	public ClauseDTO createClause(@RequestBody CreateClauseRequest request) {
		return new ClauseDTO(clauseRepository.createClause(request));
	}

	@DeleteMapping("/{clauseId}")
	public DeleteConfirmationDTO deleteSuite(@PathVariable("clauseId") ClauseId clauseId) {
		clauseRepository.deleteClause(clauseId);
		return DeleteConfirmationDTO.get();
	}
}
