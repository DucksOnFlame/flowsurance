package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.conditions.CreateExclusionRequest;
import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.ExclusionId;
import com.flowsurance.domain.service.repository.ExclusionRepository;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.flowsurance.util.rest.DeleteConfirmationDTO;
import com.flowsurance.util.rest.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exclusion")
public class ExclusionController {

	private final ExclusionRepository exclusionRepository;

	@Autowired
	public ExclusionController(ExclusionRepository exclusionRepository) {
		this.exclusionRepository = exclusionRepository;
	}

	@GetMapping
	public List<ExclusionDTO> getAllExclusions() {
		return exclusionRepository.getAllExclusionsForCurrentUser().stream()
				.map(ExclusionDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{exclusionId}")
	public ExclusionDTO getExclusion(@PathVariable("exclusionId") ExclusionId exclusionId) {
		Exclusion exclusion = exclusionRepository.getExclusion(exclusionId).orNull();
		if (exclusion == null) {
			throw new NotFoundException("Exclusion of id: " + exclusionId + " not found!");
		}

		return new ExclusionDTO(exclusion);
	}

	@PutMapping("/create")
	public ExclusionDTO createExclusion(@RequestBody CreateExclusionRequest request) {
		return new ExclusionDTO(exclusionRepository.createExclusion(request));
	}

	@DeleteMapping("/{exclusionId}")
	public DeleteConfirmationDTO deleteExclusion(@PathVariable("exclusionId") ExclusionId exclusionId) {
		exclusionRepository.deleteExclusion(exclusionId);
		return DeleteConfirmationDTO.get();
	}
}
