package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.conditions.offer.CreateOfferRequest;
import com.flowsurance.domain.model.conditions.offer.OfferId;
import com.flowsurance.domain.model.conditions.offer.OfferNotReadyException;
import com.flowsurance.domain.model.conditions.offer.SendOfferRequest;
import com.flowsurance.domain.model.conditions.offer.UpdateOfferRequest;
import com.flowsurance.domain.service.repository.OfferRepository;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.flowsurance.rest.dto.conditions.offer.OfferDTO;
import com.flowsurance.rest.dto.conditions.petition.MoveConfirmationDTO;
import com.flowsurance.util.rest.BadRequestException;
import com.flowsurance.util.rest.NotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/offer")
public class OfferController {

	private final OfferRepository offerRepository;

	@Autowired
	public OfferController(OfferRepository offerRepository) {
		this.offerRepository = offerRepository;
	}

	@GetMapping
	public List<OfferDTO> getUserMappedOffers() {
		return offerRepository.getOffersForCurrentUser().stream()
				.map(OfferDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{offerId}")
	public OfferDTO getUserMappedOffers(@PathVariable("offerId") OfferId offerId) {
		OfferDTO dto = offerRepository.get(offerId).transform(OfferDTO::new).orNull();
		if (dto == null) {
			throw new NotFoundException("Could not find Offer by id: " + offerId);
		}

		return dto;
	}

	@GetMapping("/{offerId}/clause/available")
	public List<ClauseDTO> getAllAvailableClauses(@PathVariable("offerId") OfferId offerId) {
		return offerRepository.getAllAvailableClauses(offerId).stream()
				.map(ClauseDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{offerId}/exclusion/available")
	public List<ExclusionDTO> getAllAvailableExclusions(@PathVariable("offerId") OfferId offerId) {
		return offerRepository.getAllAvailableExclusions(offerId).stream()
				.map(ExclusionDTO::new)
				.collect(Collectors.toList());
	}

	@PutMapping("/create")
	public MoveConfirmationDTO createOfferFromPetition(@RequestBody CreateOfferRequest request) {
		offerRepository.create(request);
		return MoveConfirmationDTO.get();
	}

	@PostMapping("/update")
	public OfferDTO update(@RequestBody UpdateOfferRequest request) {
		return new OfferDTO(offerRepository.update(request));
	}

	@PostMapping("/send")
	public OfferDTO send(@RequestBody SendOfferRequest request) {
		try {
			return new OfferDTO(offerRepository.send(request));
		} catch (OfferNotReadyException e) {
			throw new BadRequestException(e.getMessage());
		}
	}
}
