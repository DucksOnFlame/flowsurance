package com.flowsurance.rest.controller;

import com.flowsurance.domain.model.conditions.InsuranceConditionsType;
import com.flowsurance.domain.model.conditions.petition.CreatePetitionRequest;
import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.domain.model.conditions.petition.PetitionId;
import com.flowsurance.domain.model.conditions.petition.SendPetitionRequest;
import com.flowsurance.domain.model.conditions.petition.UpdatePetitionRequest;
import com.flowsurance.domain.service.repository.PetitionRepository;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.flowsurance.rest.dto.conditions.petition.PetitionDTO;
import com.flowsurance.rest.dto.conditions.petition.SimplePetitionDTO;
import com.flowsurance.util.rest.DeleteConfirmationDTO;
import com.flowsurance.util.rest.NotFoundException;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/petition")
public class PetitionController {

	private final PetitionRepository petitionRepository;

	@Autowired
	public PetitionController(PetitionRepository petitionRepository) {
		this.petitionRepository = petitionRepository;
	}

	@GetMapping
	public List<SimplePetitionDTO> getUserMappedPetitions() {
		return petitionRepository.getPetitionsForCurrentUser().stream()
				.map(SimplePetitionDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{petitionId}")
	public PetitionDTO getPetition(@PathVariable("petitionId") PetitionId id) {
		Petition petition = petitionRepository.getPetition(id).orNull();
		if (petition == null) {
			throw new NotFoundException("Petition of id: " + id + " not found!");
		}

		return new PetitionDTO(petition);
	}

	@PutMapping("/create")
	public SimplePetitionDTO createPetition(@RequestBody CreatePetitionRequest request) {
		return new SimplePetitionDTO(petitionRepository.createPetition(request));
	}

	@DeleteMapping("/{petitionId}")
	public DeleteConfirmationDTO deletePetition(@PathVariable("petitionId") PetitionId id) {
		petitionRepository.delete(id);
		return DeleteConfirmationDTO.get();
	}

	@GetMapping("/type")
	public List<String> getAllAvailableTypes() {
		return Lists.newArrayList(InsuranceConditionsType.getAllPrettyNames());
	}

	@GetMapping("/{petitionId}/clause/available")
	public List<ClauseDTO> getAllAvailableClauses(@PathVariable("petitionId") PetitionId id) {
		return petitionRepository.getAllAvailableClauses(id).stream()
				.map(ClauseDTO::new)
				.collect(Collectors.toList());
	}

	@GetMapping("/{petitionId}/exclusion/available")
	public List<ExclusionDTO> getAllAvailableExclusions(@PathVariable("petitionId") PetitionId id) {
		return petitionRepository.getAllAvailableExclusions(id).stream()
				.map(ExclusionDTO::new)
				.collect(Collectors.toList());
	}

	@PostMapping("/update")
	public PetitionDTO updatePetition(@RequestBody UpdatePetitionRequest request) {
		return new PetitionDTO(petitionRepository.update(request));
	}

	@PostMapping("/send")
	public PetitionDTO sendPetition(@RequestBody SendPetitionRequest request) {
		return new PetitionDTO(petitionRepository.send(request));
	}

}
