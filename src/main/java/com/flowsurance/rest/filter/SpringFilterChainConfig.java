package com.flowsurance.rest.filter;

import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringFilterChainConfig {

	private final String excludedPaths;

	private final UserContextService userContextService;

	private final UserRepository userRepository;

	@Autowired
	public SpringFilterChainConfig(@Value("${excludedPaths}") String excludedPaths,
			UserContextService userContextService,
			UserRepository userRepository) {
		this.excludedPaths = excludedPaths;
		this.userContextService = userContextService;
		this.userRepository = userRepository;
	}

	@Bean
	public FilterRegistrationBean<AuthorizationFilter> authorizationFilter() {
		FilterRegistrationBean<AuthorizationFilter> registrationBean = new FilterRegistrationBean<>();

		registrationBean.setFilter(new AuthorizationFilter(excludedPaths, userContextService, userRepository));
		registrationBean.addUrlPatterns("/*");
		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<LoginFilter> loginFilter() {
		FilterRegistrationBean<LoginFilter> registrationBean = new FilterRegistrationBean<>();

		registrationBean.setFilter(new LoginFilter(userContextService));
		registrationBean.addUrlPatterns("/user/login");
		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<LogoutFilter> logoutFilter() {
		FilterRegistrationBean<LogoutFilter> registrationBean = new FilterRegistrationBean<>();

		registrationBean.setFilter(new LogoutFilter());
		registrationBean.addUrlPatterns("/", "/index.html");
		return registrationBean;
	}
}
