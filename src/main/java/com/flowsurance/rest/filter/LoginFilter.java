package com.flowsurance.rest.filter;

import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.service.UserContextService;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginFilter implements Filter {

	private final UserContextService userContextService;

	@Autowired
	public LoginFilter(UserContextService userContextService) {
		this.userContextService = userContextService;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		try {
			((HttpServletRequest) servletRequest).getSession();
			filterChain.doFilter(servletRequest, servletResponse);
		} finally {
			User user = userContextService.get();
			if (((HttpServletResponse) servletResponse).getStatus() != 401 && user != null) {
				((HttpServletRequest) servletRequest).getSession().setAttribute("userId", user.getId().get());
			}
		}
	}

	@Override
	public void destroy() {

	}
}
