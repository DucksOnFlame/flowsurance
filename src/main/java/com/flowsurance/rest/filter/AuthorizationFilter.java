package com.flowsurance.rest.filter;

import com.flowsurance.domain.model.user.User;
import com.flowsurance.domain.model.user.UserId;
import com.flowsurance.domain.service.UserContextService;
import com.flowsurance.domain.service.repository.UserRepository;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

@Component
public class AuthorizationFilter implements Filter {

	private final String excludedPaths;

	private final UserContextService userContextService;

	private final UserRepository userRepository;

	public AuthorizationFilter(String excludedPaths, UserContextService userContextService, UserRepository userRepository) {
		this.excludedPaths = excludedPaths;
		this.userContextService = userContextService;
		this.userRepository = userRepository;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
		String requestPath = httpRequest.getServletPath();
		boolean isExcluded = getExcludedPaths().stream().anyMatch(requestPath::equals);

		if (isExcluded || requestPath.contains(".js") || requestPath.contains(".png") || requestPath.contains(".css")) {
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		}

		UserId userId = UserId.orNull((String) httpRequest.getSession().getAttribute("userId"));
		if (userId == null) {
			serveUnauthorized(httpRequest, (HttpServletResponse) servletResponse);
			return;
		}

		User user = userRepository.getUser(userId).orNull();
		if (user == null) {
			serveUnauthorized(httpRequest, (HttpServletResponse) servletResponse);
			return;
		}

		userContextService.set(user);
		try {
			filterChain.doFilter(servletRequest, servletResponse);
		} finally {
			userContextService.clear();
		}
	}

	@Override
	public void destroy() {

	}

	private void serveUnauthorized(HttpServletRequest request, HttpServletResponse response) {
		if (request.getServletPath().endsWith(".html")) {
			try {
				response.sendRedirect("/");
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			response.setStatus(401);
		}
	}

	private List<String> getExcludedPaths() {
		return Lists.newArrayList(excludedPaths.split(";"));
	}
}
