package com.flowsurance.rest.dto.address;

import com.flowsurance.domain.model.address.Address;
import com.flowsurance.domain.model.address.AddressId;
import com.flowsurance.domain.model.address.City;
import com.flowsurance.domain.model.address.Country;
import com.flowsurance.domain.model.address.CountryName;
import com.flowsurance.domain.model.address.State;
import com.flowsurance.domain.model.address.StreetAddress;
import com.flowsurance.domain.model.address.ZipCode;

public class AddressDTO {

	private final String id;

	private final String streetAddress;

	private final String zipCode;

	private final String city;

	private final String state;

	private final String countryName;

	public AddressDTO(Address address) {
		this.id = address.getId().get();
		this.streetAddress = address.getStreetAddress().get();
		this.zipCode = address.getZipCode().get();
		this.city = address.getCity().get();
		this.state = address.getState().get();
		this.countryName = address.getCountry().getName().get();
	}

	public AddressId getId() {
		return new AddressId(id);
	}

	public StreetAddress getStreetAddress() {
		return new StreetAddress(streetAddress);
	}

	public ZipCode getZipCode() {
		return new ZipCode(zipCode);
	}

	public City getCity() {
		return new City(city);
	}

	public State getState() {
		return new State(state);
	}

	public Country getCountry() {
		return Country.valueOf(new CountryName(countryName));
	}
}
