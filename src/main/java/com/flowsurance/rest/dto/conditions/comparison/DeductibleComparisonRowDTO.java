package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ComparedDeductible;
import com.flowsurance.domain.model.conditions.comparison.DeductibleComparisonRow;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;
import com.google.common.collect.Maps;
import java.util.Map;

public class DeductibleComparisonRowDTO {

	private final DeductibleDTO originalDeductible;

	private final Map<String, ComparedDeductibleDTO> comparedDeductibles;

	public DeductibleComparisonRowDTO(DeductibleComparisonRow deductibleComparisonRow) {
		this.originalDeductible = new DeductibleDTO(deductibleComparisonRow.getOriginalDeductible());

		Map<String, ComparedDeductibleDTO> comparedDeductibleDTOs = Maps.newHashMap();
		for (Map.Entry<InsurerCompany, ComparedDeductible> entry : deductibleComparisonRow.getComparedDeductibles().entrySet()) {
			comparedDeductibleDTOs.put(entry.getKey().getName().get(), new ComparedDeductibleDTO(entry.getValue()));
		}

		this.comparedDeductibles = comparedDeductibleDTOs;
	}
}
