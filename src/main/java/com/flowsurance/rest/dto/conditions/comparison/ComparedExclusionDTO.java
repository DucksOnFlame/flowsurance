package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ComparedExclusion;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;

public class ComparedExclusionDTO {

	private final String type;

	private final ExclusionDTO newExclusion;

	public ComparedExclusionDTO(ComparedExclusion comparedExclusion) {
		this.type = comparedExclusion.getType().name();
		this.newExclusion = comparedExclusion.getExclusion().transform(ExclusionDTO::new).orNull();
	}
}
