package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.Deductible;
import com.flowsurance.domain.model.conditions.comparison.AdditionalDeductibles;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class AdditionalDeductiblesDTO {

	private List<Map<String, DeductibleDTO>> additionalDeductibleRows;

	public AdditionalDeductiblesDTO(AdditionalDeductibles additionalDeductibles) {
		Map<InsurerCompany, List<Deductible>> allDeductibles = additionalDeductibles.getAdditionalDeductibles();

		int maxDeductibleNumber = 0;
		for (List<Deductible> deductibleList : allDeductibles.values()) {
			int deductibleNumber = deductibleList.size();
			if (deductibleNumber > maxDeductibleNumber) {
				maxDeductibleNumber = deductibleNumber;
			}
		}

		List<Map<String, DeductibleDTO>> additionalDeductibleRows = Lists.newArrayList();
		for (int i = 0; i < maxDeductibleNumber; i++) {
			additionalDeductibleRows.add(Maps.newHashMap());
		}

		for (Map.Entry<InsurerCompany, List<Deductible>> entry : allDeductibles.entrySet()) {
			for (int i = 0; i < entry.getValue().size(); i++) {
				Map<String, DeductibleDTO> insurerDeductibles = additionalDeductibleRows.get(i);
				insurerDeductibles.put(entry.getKey().getName().get(), new DeductibleDTO(entry.getValue().get(i)));
			}
		}

		this.additionalDeductibleRows = additionalDeductibleRows;
	}
}
