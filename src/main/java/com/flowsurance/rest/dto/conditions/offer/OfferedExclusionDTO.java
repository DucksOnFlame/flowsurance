package com.flowsurance.rest.dto.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.domain.model.conditions.offer.OfferedExclusion;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.google.common.base.Optional;

public class OfferedExclusionDTO {

	private String offerId;

	private ExclusionDTO originalExclusion;

	private OfferedConditionType type;

	private ExclusionDTO changedExclusion;

	public OfferedExclusionDTO(OfferedExclusion offeredExclusion) {
		this.offerId = offeredExclusion.getOfferId().get();
		this.originalExclusion = offeredExclusion.getOriginalExclusion().transform(ExclusionDTO::new).orNull();
		this.type = offeredExclusion.getType();
		this.changedExclusion = offeredExclusion.getChangedExclusion().transform(ExclusionDTO::new).orNull();
	}

	public String getOfferId() {
		return offerId;
	}

	public Optional<ExclusionDTO> getOriginalExclusion() {
		return Optional.fromNullable(originalExclusion);
	}

	public OfferedConditionType getType() {
		return type;
	}

	public Optional<ExclusionDTO> getChangedExclusion() {
		return Optional.fromNullable(changedExclusion);
	}
}
