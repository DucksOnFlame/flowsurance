package com.flowsurance.rest.dto.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.OfferedClause;
import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.google.common.base.Optional;

public class OfferedClauseDTO {

	private String offerId;

	private ClauseDTO originalClause;

	private OfferedConditionType type;

	private ClauseDTO changedClause;

	public OfferedClauseDTO(OfferedClause offeredClause) {
		this.offerId = offeredClause.getOfferId().get();
		this.originalClause = offeredClause.getOriginalClause().transform(ClauseDTO::new).orNull();
		this.type = offeredClause.getType();
		this.changedClause = offeredClause.getChangedClause().transform(ClauseDTO::new).orNull();
	}

	public String getOfferId() {
		return offerId;
	}

	public Optional<ClauseDTO> getOriginalClause() {
		return Optional.fromNullable(originalClause);
	}

	public OfferedConditionType getType() {
		return type;
	}

	public Optional<ClauseDTO> getChangedClause() {
		return Optional.fromNullable(changedClause);
	}
}
