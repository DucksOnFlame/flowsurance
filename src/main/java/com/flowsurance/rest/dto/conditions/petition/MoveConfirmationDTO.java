package com.flowsurance.rest.dto.conditions.petition;

public class MoveConfirmationDTO {

	private final static MoveConfirmationDTO instance = new MoveConfirmationDTO();

	private final String message = "Petition moved to offers.";

	private MoveConfirmationDTO() {
	}

	public static MoveConfirmationDTO get() {
		return instance;
	}
}
