package com.flowsurance.rest.dto.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.rest.dto.conditions.petition.PetitionDTO;
import com.flowsurance.rest.dto.user.UserDTO;
import java.util.List;
import java.util.stream.Collectors;

public class OfferDTO {

	private String id;

	private PetitionDTO petition;

	private String state;

	private List<OfferedClauseDTO> offeredClauses;

	private List<OfferedExclusionDTO> offeredExclusions;

	private List<OfferedDeductibleDTO> offeredDeductibles;

	private double amount;

	private String currencyCode;

	private UserDTO insurer;

	public OfferDTO(Offer offer) {
		this.id = offer.getId().get();
		this.petition = new PetitionDTO(offer.getPetition());
		this.state = offer.getOfferState().getPrettyName();
		this.offeredClauses = offer.getOfferedClauses().stream().map(OfferedClauseDTO::new).collect(Collectors.toList());
		this.offeredExclusions = offer.getOfferedExclusions().stream().map(OfferedExclusionDTO::new).collect(Collectors.toList());
		this.offeredDeductibles = offer.getOfferedDeductibles().stream().map(OfferedDeductibleDTO::new).collect(Collectors.toList());
		this.amount = offer.getPrice().getAmount().doubleValue();
		this.currencyCode = offer.getPrice().getCurrencyUnit().getCode();
		this.insurer = new UserDTO(offer.getInsurer());
	}
}
