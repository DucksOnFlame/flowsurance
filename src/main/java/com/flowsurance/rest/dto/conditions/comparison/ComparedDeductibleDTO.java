package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ComparedDeductible;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;

public class ComparedDeductibleDTO {

	private final String type;

	private final DeductibleDTO newDeductible;

	public ComparedDeductibleDTO(ComparedDeductible comparedDeductible) {
		this.type = comparedDeductible.getType().name();
		this.newDeductible = comparedDeductible.getDeductible().transform(DeductibleDTO::new).orNull();
	}
}
