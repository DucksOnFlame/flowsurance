package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.comparison.AdditionalClauses;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class AdditionalClausesDTO {

	private List<Map<String, ClauseDTO>> additionalClauseRows;

	public AdditionalClausesDTO(AdditionalClauses additionalClauses) {
		Map<InsurerCompany, List<Clause>> allClauses = additionalClauses.getAdditionalClauses();

		int maxClauseNumber = 0;
		for (List<Clause> clauseList : allClauses.values()) {
			int clauseNumber = clauseList.size();
			if (clauseNumber > maxClauseNumber) {
				maxClauseNumber = clauseNumber;
			}
		}

		List<Map<String, ClauseDTO>> additionalClauseRows = Lists.newArrayList();
		for (int i = 0; i < maxClauseNumber; i++) {
			additionalClauseRows.add(Maps.newHashMap());
		}

		for (Map.Entry<InsurerCompany, List<Clause>> entry : allClauses.entrySet()) {
			for (int i = 0; i < entry.getValue().size(); i++) {
				Map<String, ClauseDTO> insurerClauses = additionalClauseRows.get(i);
				insurerClauses.put(entry.getKey().getName().get(), new ClauseDTO(entry.getValue().get(i)));
			}
		}

		this.additionalClauseRows = additionalClauseRows;
	}
}
