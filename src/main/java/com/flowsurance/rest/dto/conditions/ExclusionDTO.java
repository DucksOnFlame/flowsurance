package com.flowsurance.rest.dto.conditions;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.ExclusionId;

public class ExclusionDTO {

	private String id;

	private String content;

	public ExclusionDTO(Exclusion exclusion) {
		this.id = exclusion.getId().get();
		this.content = exclusion.getContent();
	}

	public ExclusionId getId() {
		return new ExclusionId(id);
	}

	public String getContent() {
		return content;
	}
}
