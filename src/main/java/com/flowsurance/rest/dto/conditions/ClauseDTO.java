package com.flowsurance.rest.dto.conditions;

import com.flowsurance.domain.model.conditions.Limit;
import com.flowsurance.domain.model.conditions.clause.Clause;
import com.flowsurance.domain.model.conditions.clause.ClauseId;
import com.flowsurance.domain.model.conditions.clause.ClauseTitle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClauseDTO implements Serializable {

	private String id;

	private String title;

	private String content;

	private List<LimitDTO> limits;

	private List<DeductibleDTO> deductibles;

	public ClauseDTO(Clause clause) {
		this.id = clause.getId().get();
		this.title = clause.getTitle().get();
		this.content = clause.getContent();

		this.limits = new ArrayList<>();
		for (Limit limit : clause.getLimits()) {
			limits.add(new LimitDTO(limit));
		}

		this.deductibles = clause.getDeductibles().stream()
				.map(DeductibleDTO::new)
				.collect(Collectors.toList());
	}

	public ClauseId getId() {
		return new ClauseId(id);
	}

	public ClauseTitle getTitle() {
		return new ClauseTitle(title);
	}

	public String getContent() {
		return content;
	}

	public List<LimitDTO> getLimits() {
		return limits;
	}

	public List<DeductibleDTO> getDeductibles() {
		return deductibles;
	}
}
