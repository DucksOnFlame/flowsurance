package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.Exclusion;
import com.flowsurance.domain.model.conditions.comparison.AdditionalExclusions;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;

public class AdditionalExclusionsDTO {

	private List<Map<String, ExclusionDTO>> additionalExclusionRows;

	public AdditionalExclusionsDTO(AdditionalExclusions additionalExclusions) {
		Map<InsurerCompany, List<Exclusion>> allExclusions = additionalExclusions.getAdditionalExclusions();

		int maxExclusionNumber = 0;
		for (List<Exclusion> exclusionList : allExclusions.values()) {
			int exclusionNumber = exclusionList.size();
			if (exclusionNumber > maxExclusionNumber) {
				maxExclusionNumber = exclusionNumber;
			}
		}

		List<Map<String, ExclusionDTO>> additionalExclusionRows = Lists.newArrayList();
		for (int i = 0; i < maxExclusionNumber; i++) {
			additionalExclusionRows.add(Maps.newHashMap());
		}

		for (Map.Entry<InsurerCompany, List<Exclusion>> entry : allExclusions.entrySet()) {
			for (int i = 0; i < entry.getValue().size(); i++) {
				Map<String, ExclusionDTO> insurerExclusions = additionalExclusionRows.get(i);
				insurerExclusions.put(entry.getKey().getName().get(), new ExclusionDTO(entry.getValue().get(i)));
			}
		}

		this.additionalExclusionRows = additionalExclusionRows;
	}
}
