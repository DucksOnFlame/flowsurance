package com.flowsurance.rest.dto.conditions;

import com.google.common.collect.Maps;
import java.util.Map;

public class StakesDTO {

	private Map<String, Float> stakes = Maps.newHashMap();

	public StakesDTO() {
	}

	public StakesDTO(Map<String, Float> stakes) {
		this.stakes = stakes;
	}

	public Map<String, Float> getStakes() {
		return Maps.newHashMap(stakes);
	}
}
