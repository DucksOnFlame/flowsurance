package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.Comparison;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.PriceDTO;
import com.flowsurance.rest.dto.conditions.SubjectDTO;
import com.flowsurance.rest.dto.entity.CompanyDTO;
import com.flowsurance.rest.dto.user.UserDTO;
import com.flowsurance.util.lang.DateUtils;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.joda.money.BigMoney;

public class ComparisonDTO {

	private String id;

	private String petitionId;

	private String name;

	private String type;

	private List<SubjectDTO> subjects;

	private String coverStartDate;

	private String coverEndDate;

	private CompanyDTO policyHolder;

	private List<CompanyDTO> insureds;

	private UserDTO broker;

	private List<String> insurers;

	private Map<String, UserDTO> underwritters;

	private List<ClauseComparisonRowDTO> clauseComparisonRows;

	private List<ExclusionComparisonRowDTO> exclusionComparisonRows;

	private List<DeductibleComparisonRowDTO> deductibleComparisonRows;

	private AdditionalClausesDTO additionalClauses;

	private AdditionalExclusionsDTO additionalExclusions;

	private AdditionalDeductiblesDTO additionalDeductibles;

	private Map<String, PriceDTO> priceMap;
	
	public ComparisonDTO(Comparison comparison) {
		this.id = comparison.getId().get();
		this.petitionId = comparison.getPetitionId().get();
		this.name = comparison.getName().get();
		this.type = comparison.getType().getPrettyName();
		this.subjects = comparison.getSubjects().stream().map(SubjectDTO::new).collect(Collectors.toList());
		this.coverStartDate = DateUtils.dateTimeToString(comparison.getCoverStartDate());
		this.coverEndDate = DateUtils.dateTimeToString(comparison.getCoverEndDate());
		this.policyHolder = new CompanyDTO(comparison.getPolicyHolder());
		this.insureds = comparison.getInsureds().stream().map(CompanyDTO::new).collect(Collectors.toList());
		this.broker = new UserDTO(comparison.getBroker());
		this.underwritters = comparison.getUnderwritters().stream()
				.collect(Collectors.toMap(user -> user.getCompany().getName().get(), UserDTO::new));

		Map<String, PriceDTO> priceMap = Maps.newHashMap();
		for (Map.Entry<InsurerCompany, BigMoney> priceEntry : comparison.getPriceMap().entrySet()) {
			priceMap.put(priceEntry.getKey().getName().get(), new PriceDTO(priceEntry.getValue()));
		}

		this.insurers = comparison.getUnderwritters().stream().map(underwritter -> underwritter.getCompany().getName().get()).collect(Collectors.toList());

		this.clauseComparisonRows = comparison.getClauseComparisonRows().stream().map(ClauseComparisonRowDTO::new).collect(Collectors.toList());
		this.exclusionComparisonRows = comparison.getExclusionComparisonRows().stream().map(ExclusionComparisonRowDTO::new).collect(Collectors.toList());
		this.deductibleComparisonRows = comparison.getDeductibleComparisonRows().stream().map(DeductibleComparisonRowDTO::new).collect(Collectors.toList());

		this.additionalClauses = new AdditionalClausesDTO(comparison.getAdditionalClauses());
		this.additionalExclusions = new AdditionalExclusionsDTO(comparison.getAdditionalExclusions());
		this.additionalDeductibles = new AdditionalDeductiblesDTO(comparison.getAdditionalDeductibles());

		this.priceMap = priceMap;
	}
}
