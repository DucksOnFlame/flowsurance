package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ComparedClause;
import com.flowsurance.rest.dto.conditions.ClauseDTO;

public class ComparedClauseDTO {

	private final String type;

	private final ClauseDTO newClause;

	public ComparedClauseDTO(ComparedClause comparedClause) {
		this.type = comparedClause.getType().name();
		this.newClause = comparedClause.getClause().transform(ClauseDTO::new).orNull();
	}
}
