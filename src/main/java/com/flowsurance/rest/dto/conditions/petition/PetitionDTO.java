package com.flowsurance.rest.dto.conditions.petition;

import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.flowsurance.rest.dto.conditions.SubjectDTO;
import com.flowsurance.rest.dto.entity.CompanyDTO;
import com.flowsurance.rest.dto.user.UserDTO;
import com.flowsurance.util.lang.DateUtils;
import java.util.List;
import java.util.stream.Collectors;

public class PetitionDTO {

	private String id;

	private String name;

	private String type;

	private String state;

	private List<SubjectDTO> subjects;

	private String coverStartDate;

	private String coverEndDate;

	private CompanyDTO policyHolder;

	private List<CompanyDTO> insureds;

	private List<ClauseDTO> clauses;

	private List<ExclusionDTO> exclusions;

	private List<DeductibleDTO> deductibles;

	private UserDTO broker;

	public PetitionDTO(Petition petition) {
		this.id = petition.getId().get();
		this.name = petition.getName().get();
		this.type = petition.getType().getPrettyName();
		this.state = petition.getState().getPrettyName();
		this.subjects = petition.getSubjects().stream().map(SubjectDTO::new).collect(Collectors.toList());
		this.coverStartDate = DateUtils.dateTimeToString(petition.getCoverStartDate());
		this.coverEndDate = DateUtils.dateTimeToString(petition.getCoverEndDate());
		this.policyHolder = new CompanyDTO(petition.getPolicyHolder());
		this.insureds = petition.getInsureds().stream()
				.map(CompanyDTO::new)
				.collect(Collectors.toList());

		this.clauses = petition.getClauses().stream()
				.map(ClauseDTO::new)
				.collect(Collectors.toList());

		this.exclusions = petition.getExclusions().stream()
				.map(ExclusionDTO::new)
				.collect(Collectors.toList());

		this.deductibles = petition.getDeductibles().stream()
				.map(DeductibleDTO::new)
				.collect(Collectors.toList());

		this.broker = new UserDTO(petition.getBroker());
	}
}
