package com.flowsurance.rest.dto.conditions;

import com.flowsurance.domain.model.conditions.Limit;
import java.math.BigDecimal;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;

public class LimitDTO {

	private String description;

	private BigDecimal valuePerOccurrence = null;

	private String occurrenceCurrency = null;

	private BigDecimal valueInAggregate = null;

	private String aggregateCurrency = null;

	public LimitDTO(Limit limit) {
		this.description = limit.getDescription();

		BigMoney valuePerOccurrence = limit.getValuePerOccurrence().orNull();
		if (valuePerOccurrence != null) {
			this.valuePerOccurrence = valuePerOccurrence.getAmount();
			this.occurrenceCurrency = valuePerOccurrence.getCurrencyUnit().getCode();
		}

		BigMoney valueInAggregate = limit.getValueInAggregate().orNull();
		if (valueInAggregate != null) {
			this.valueInAggregate = valueInAggregate.getAmount();
			this.aggregateCurrency = valueInAggregate.getCurrencyUnit().getCode();
		}
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigMoney getValuePerOccurrence() {
		return BigMoney.of(CurrencyUnit.of(occurrenceCurrency), valuePerOccurrence);
	}

	public void setValuePerOccurrence(BigMoney valuePerOccurrence) {
		if (valuePerOccurrence != null) {
			this.valuePerOccurrence = valuePerOccurrence.getAmount();
			this.occurrenceCurrency = valuePerOccurrence.getCurrencyUnit().getCode();
		}
	}

	public BigMoney getValueInAggregate() {
		return BigMoney.of(CurrencyUnit.of(aggregateCurrency), valueInAggregate);
	}

	public void setValueInAggregate(BigMoney valueInAggregate) {
		if (valueInAggregate != null) {
			this.valueInAggregate = valueInAggregate.getAmount();
			this.aggregateCurrency = valueInAggregate.getCurrencyUnit().getCode();
		}
	}
}
