package com.flowsurance.rest.dto.conditions;

import com.flowsurance.domain.model.conditions.Subject;

public class SubjectDTO {

	private String item;

	public SubjectDTO() {
	}

	public SubjectDTO(Subject subject) {
		this.item = subject.getItem();
	}

	public String getItem() {
		return item;
	}
}
