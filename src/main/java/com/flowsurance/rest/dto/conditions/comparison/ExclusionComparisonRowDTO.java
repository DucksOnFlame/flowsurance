package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ComparedExclusion;
import com.flowsurance.domain.model.conditions.comparison.ExclusionComparisonRow;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.ExclusionDTO;
import com.google.common.collect.Maps;
import java.util.Map;

public class ExclusionComparisonRowDTO {

	private final ExclusionDTO originalExclusion;

	private final Map<String, ComparedExclusionDTO> comparedExclusions;

	public ExclusionComparisonRowDTO(ExclusionComparisonRow exclusionComparisonRow) {
		this.originalExclusion = new ExclusionDTO(exclusionComparisonRow.getOriginalExclusion());

		Map<String, ComparedExclusionDTO> comparedExclusionDTOs = Maps.newHashMap();
		for (Map.Entry<InsurerCompany, ComparedExclusion> entry : exclusionComparisonRow.getComparedExclusions().entrySet()) {
			comparedExclusionDTOs.put(entry.getKey().getName().get(), new ComparedExclusionDTO(entry.getValue()));
		}

		this.comparedExclusions = comparedExclusionDTOs;
	}
}
