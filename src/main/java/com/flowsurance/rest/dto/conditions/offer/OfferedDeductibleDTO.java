package com.flowsurance.rest.dto.conditions.offer;

import com.flowsurance.domain.model.conditions.offer.OfferedConditionType;
import com.flowsurance.domain.model.conditions.offer.OfferedDeductible;
import com.flowsurance.rest.dto.conditions.DeductibleDTO;
import com.google.common.base.Optional;

public class OfferedDeductibleDTO {

	private String offerId;

	private DeductibleDTO originalDeductible;

	private OfferedConditionType type;

	private DeductibleDTO changedDeductible;

	public OfferedDeductibleDTO(OfferedDeductible offeredDeductible) {
		this.offerId = offeredDeductible.getOfferId().get();
		this.originalDeductible = offeredDeductible.getOriginalDeductible().transform(DeductibleDTO::new).orNull();
		this.type = offeredDeductible.getType();
		this.changedDeductible = offeredDeductible.getChangedDeductible().transform(DeductibleDTO::new).orNull();
	}

	public String getOfferId() {
		return offerId;
	}

	public Optional<DeductibleDTO> getOriginalDeductible() {
		return Optional.fromNullable(originalDeductible);
	}

	public OfferedConditionType getType() {
		return type;
	}

	public Optional<DeductibleDTO> getChangedDeductible() {
		return Optional.fromNullable(changedDeductible);
	}
}
