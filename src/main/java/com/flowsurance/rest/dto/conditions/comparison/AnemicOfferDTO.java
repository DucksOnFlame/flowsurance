package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.offer.Offer;
import com.flowsurance.rest.dto.conditions.offer.OfferedClauseDTO;
import com.flowsurance.rest.dto.conditions.offer.OfferedDeductibleDTO;
import com.flowsurance.rest.dto.conditions.offer.OfferedExclusionDTO;
import com.flowsurance.rest.dto.user.UserDTO;
import java.util.List;
import java.util.stream.Collectors;

public class AnemicOfferDTO {

	private String id;

	private String state;

	private List<OfferedClauseDTO> offeredClauses;

	private List<OfferedExclusionDTO> offeredExclusions;

	private List<OfferedDeductibleDTO> offeredDeductibles;

	private double amount;

	private String currencyCode;

	private UserDTO insurer;

	public AnemicOfferDTO(Offer offer) {
		this.id = offer.getId().get();
		this.state = offer.getOfferState().getPrettyName();
		this.offeredClauses = offer.getOfferedClauses().stream().map(OfferedClauseDTO::new).collect(Collectors.toList());
		this.offeredExclusions = offer.getOfferedExclusions().stream().map(OfferedExclusionDTO::new).collect(Collectors.toList());
		this.offeredDeductibles = offer.getOfferedDeductibles().stream().map(OfferedDeductibleDTO::new).collect(Collectors.toList());
		this.amount = offer.getPrice().getAmount().doubleValue();
		this.currencyCode = offer.getPrice().getCurrencyUnit().getCode();
		this.insurer = new UserDTO(offer.getInsurer());
	}
}
