package com.flowsurance.rest.dto.conditions.petition;

import com.flowsurance.domain.model.conditions.petition.Petition;
import com.flowsurance.rest.dto.user.UserDTO;
import com.flowsurance.util.lang.DateUtils;

public class SimplePetitionDTO {

	private String id;

	private String name;

	private String state;

	private String type;

	private String policyHolderName;

	private String coverStartDate;

	private String coverEndDate;

	private UserDTO broker;

	public SimplePetitionDTO(Petition petition) {
		this.id = petition.getId().get();
		this.name = petition.getName().get();
		this.state = petition.getState().getPrettyName();
		this.type = petition.getType().getPrettyName();
		this.coverStartDate = DateUtils.dateTimeToString(petition.getCoverStartDate());
		this.coverEndDate = DateUtils.dateTimeToString(petition.getCoverEndDate());
		this.policyHolderName = petition.getPolicyHolder().getName().get();
		this.broker = new UserDTO(petition.getBroker());
	}
}
