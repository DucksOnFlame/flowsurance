package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.AnemicComparison;
import com.flowsurance.rest.dto.conditions.offer.OfferDTO;
import com.flowsurance.rest.dto.conditions.petition.PetitionDTO;
import java.util.List;
import java.util.stream.Collectors;

public class AnemicComparisonDTO {

	private String id;

	private PetitionDTO petition;

	private List<OfferDTO> offers;

	public AnemicComparisonDTO(AnemicComparison anemicComparison) {
		this.id = anemicComparison.getId().get();
		this.petition = new PetitionDTO(anemicComparison.getPetition());
		this.offers = anemicComparison.getOffers().stream().map(OfferDTO::new).collect(Collectors.toList());
	}
}
