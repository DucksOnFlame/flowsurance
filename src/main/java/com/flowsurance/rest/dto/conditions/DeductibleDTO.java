package com.flowsurance.rest.dto.conditions;

import com.flowsurance.domain.model.conditions.Deductible;

public class DeductibleDTO {

	private final String description;

	public DeductibleDTO(Deductible deductible) {
		this.description = deductible.getDescription();
	}

	public String getDescription() {
		return description;
	}
}
