package com.flowsurance.rest.dto.conditions;

import org.joda.money.BigMoney;

public class PriceDTO {

	private double amount;

	private String currency;

	public PriceDTO(BigMoney price) {
		this.amount = price.getAmount().doubleValue();
		this.currency = price.getCurrencyUnit().getCode();
	}
}
