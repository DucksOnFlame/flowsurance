package com.flowsurance.rest.dto.conditions.comparison;

import com.flowsurance.domain.model.conditions.comparison.ClauseComparisonRow;
import com.flowsurance.domain.model.conditions.comparison.ComparedClause;
import com.flowsurance.domain.model.entity.InsurerCompany;
import com.flowsurance.rest.dto.conditions.ClauseDTO;
import com.google.common.collect.Maps;
import java.util.Map;

public class ClauseComparisonRowDTO {

	private final ClauseDTO originalClause;

	private final Map<String, ComparedClauseDTO> comparedClauses;

	public ClauseComparisonRowDTO(ClauseComparisonRow clauseComparisonRow) {
		this.originalClause = new ClauseDTO(clauseComparisonRow.getOriginalClause());

		Map<String, ComparedClauseDTO> comparedClauseDTOs = Maps.newHashMap();
		for (Map.Entry<InsurerCompany, ComparedClause> entry : clauseComparisonRow.getComparedClauses().entrySet()) {
			comparedClauseDTOs.put(entry.getKey().getName().get(), new ComparedClauseDTO(entry.getValue()));
		}

		this.comparedClauses = comparedClauseDTOs;
	}
}
