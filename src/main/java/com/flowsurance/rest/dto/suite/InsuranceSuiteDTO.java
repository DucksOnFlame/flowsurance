package com.flowsurance.rest.dto.suite;

import com.flowsurance.domain.model.suite.InsuranceSuite;
import com.flowsurance.domain.model.suite.InsuranceSuiteId;
import com.flowsurance.domain.model.suite.InsuranceSuiteType;
import com.flowsurance.rest.dto.conditions.petition.PetitionDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InsuranceSuiteDTO implements Serializable {

	private String id;

	private String name;

	private List<PetitionDTO> insuranceConditionsList = new ArrayList<>();

	private InsuranceSuiteType type;

	public InsuranceSuiteDTO(InsuranceSuite insuranceSuite) {
		this.id = insuranceSuite.getId().get();
		this.name = insuranceSuite.getName();
		this.type = insuranceSuite.getType();
	}

	public InsuranceSuiteId getId() {
		return new InsuranceSuiteId(id);
	}

	public String getName() {
		return name;
	}

	public List<PetitionDTO> getInsuranceConditions() {
		return insuranceConditionsList;
	}

	public void addInsuranceConditions(PetitionDTO insuranceConditions) {
		if (insuranceConditions != null) {
			this.insuranceConditionsList.add(insuranceConditions);
		}
	}

	public InsuranceSuiteType getType() {
		return type;
	}
}
