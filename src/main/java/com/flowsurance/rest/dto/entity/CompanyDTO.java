package com.flowsurance.rest.dto.entity;

import com.flowsurance.domain.model.address.Address;
import com.flowsurance.domain.model.entity.Company;
import java.io.Serializable;

public class CompanyDTO implements Serializable {

	private final String id;

	private final String name;

	private final String type;

	private final String street;

	private final String zipCode;

	private final String city;

	private final String state;

	private final String country;

	private final String taxPayerId;

	public CompanyDTO(Company company) {
		this.id = company.getId().get();
		this.name = company.getName().get();
		Address address = company.getAddress();
		this.street = address.getStreetAddress().get();
		this.zipCode = address.getZipCode().get();
		this.city = address.getCity().get();
		this.state = address.getState().get();
		this.country = address.getCountry().getName().get();
		this.type = company.getType().name();
		this.taxPayerId = company.getTaxPayerId().get();
	}
}
