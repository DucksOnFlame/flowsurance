package com.flowsurance.rest.dto.user;

public class UserConfigDTO {

	private boolean isBroker;

	private boolean isInsurer;

	private boolean isAdmin;

	public UserConfigDTO() {
	}

	public UserConfigDTO(boolean isBroker, boolean isInsurer, boolean isAdmin) {
		this.isBroker = isBroker;
		this.isInsurer = isInsurer;
		this.isAdmin = isAdmin;
	}
}
