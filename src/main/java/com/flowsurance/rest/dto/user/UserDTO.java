package com.flowsurance.rest.dto.user;

import com.flowsurance.domain.model.user.User;
import com.flowsurance.rest.dto.entity.CompanyDTO;
import java.io.Serializable;

public class UserDTO implements Serializable {

	private final String id;

	private final String name;

	private final String surname;

	private final String displayName;

	private final CompanyDTO company;

	private final String email;

	private final String jobTitle;

	public UserDTO(User user) {
		this.id = user.getId().get();
		this.name = user.getUserName().getName();
		this.surname = user.getUserName().getSurname();
		this.displayName = user.getDisplayName().get();
		this.company = new CompanyDTO(user.getCompany());
		this.email = user.getEmail().get();
		this.jobTitle = user.getJobTitle().get();
	}
}
