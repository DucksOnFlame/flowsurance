package com.flowsurance.rest.dto.user;

import com.flowsurance.domain.model.user.UserDisplayName;
import com.flowsurance.domain.model.user.UserPassword;

public class IsAuthorizedDTO {

	private String displayName;

	private String password;

	public IsAuthorizedDTO() {
	}

	public UserDisplayName getDisplayName() {
		return new UserDisplayName(displayName);
	}

	public UserPassword getPassword() {
		return new UserPassword(password);
	}
}
